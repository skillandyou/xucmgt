# xucmgt

This package provides web interfaces to XiVOcc system

* CC manager
* CC Agent
* UC Assistant

Electron wrapper for these web apps is located in `electron` folder.

# Development

sbt & npm are required to run the development version and the test suite.

## Tooling

Here is a quick description of the tools used in the javascript application

* `npm` for package management
* `webpack` for building orchestration
* `eslint` for checking code style
* `karma` for testing orchestration (with firefox & chrome headless supports)
* `Jasmine` for unit testing

Each of the tools have specific configuration files:
* package.json
* webpack.config.js
* karma.config.js
* .eslintrc.js (one at the root folder, one in the test folder for specific configuration)

## Workflow

When running the application in development mode using the run command, there is a play run hook that will start webpack to build the javascript application. 

Webpack is responsible of checking the javascript application and will ensure all dependencies are merged inside the application. It will create the following files based on its configuration in the `target/web/public/main/javascripts/dist` folder:

* `agent.js`: the main agent javascript application
* `ccmanager.js`: the main ccmanager javascript application
* `ucassistant.js`: the main web assistant javascript application
* `vendor.js`: contains all the third party dependencies (angular, jquery, lodash,...)
* `manifest.js': contains the bootstrap code for webpack.


## Test

Run the following command to start the test server: `npm it`

It will check the dependencies are available and will run test continuously.

* *Note*: By default source maps have been disabled for increasing test execution speed, for debugging purpose you can uncomment in `karma.config` : ```webpackConfig.devtool='inline-source-map'```

If you want to run only specific tests, you can run one of the commands after running the test server (`npm it`) in a separate console

* `./node_modules/karma/bin/karma run -- --grep="agent edit controller"`
* `karma run -- --grep="agent edit controller"`

The later requires the karma-cli command line tool to be in the path. It can be installed with `npm -i -g karma-cli`

Please note the test are now run inside a browser. In fact, there are two browsers: Chrome & Firefox. It allows to perform test on the rendered version of our application/controllers/directive.

Html templates that lives in the javascript application are automatically loaded using the ng-html2js plugin for karma. Other templates that lives in the Play application needs to be mocked in the `test/karma/bootstrap.js`. This last part requires that you load the following modules in your test when needed : 

``` javascript
  beforeEach(angular.mock.module('karma-backend'));
  beforeEach(angular.mock.module('html-templates'));
```

## Docker Image Build

To be able to build locally the docker image (with the `docker_build.sh` script) you'll need a self signed certificated for electron .exe, to do so:
* Install openssl package for your operating system from [here](https://www.openssl.org/related/binaries.html)
* Generate a private key: `openssl genrsa 2048 > private.pem`
* Generate the self signed certificate: `openssl req -x509 -new -key private.pem -out public.pem`
* Create PFX: `openssl pkcs12 -export -in public.pem -inkey private.pem -out mycert.pfx`
* Move .pfx file to certs folder: `mv mycert.pfx ~/.certs/avencall.pfx`
* Create a secret file containing password of the pfx : `echo mypassswd  >  ~/.certs/secret` (if you don't have secret password just create a blank file)


## ES6 & Webpack nice features

When developing javascript part, you can use ES6 features and will be transpiled to ES5 if needed. Here is a quick list of the nice features of ES6/webpack

### Arrow functions
You can now write function expression using the following syntax
``` javascript
var fn = () => {

};

// Or in case of callbacks
mypromise.then((data) => {
    console.log("Here is the data", data);
});
```

See more: [Arrow functions](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Functions/Arrow_functions)

### Imports
Instead of manually editing your html templates to include files, you can now use the `import` statement to import specific javascript files. 

``` javascript
import _ from 'lodash';

import './mymodule/myfile.js'; // extension is not required, same as './mymodule/myfile'

```

Also they are some aliases for the root folder of our modules:
* xccti
* xchelper
* xclogin

So you can write:
``` javascript
import 'xccti/cti-webpack';
```

See more: [import keyword](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/import)

### classes

See here: [class keyword](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/class)

### export & angular

Modularizing an angular module is pretty easy with the tools we have. You can split each directive,controller,service, template in a separate file. For example, you can write a controller in a js file like this:

``` javascript
export default class MainController {
    constructor() {
        // some code...
    }
}
```

And then register it in your module:

```
import MainController from './maincontroller.js';

angular.module('MyApp', [])
    .controller('MainController', MainController);
```

See more: [export keyword](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/export), [Using angular 1.x with webpack](http://beta.angular-tips.com/blog/2015/06/using-angular-1-dot-x-with-es6-and-webpack/)

### More to come...

## Documentation

    http://xivocc.readthedocs.org

## License


This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.
This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.
You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

See the COPYING and COPYING.LESSER files for details.
