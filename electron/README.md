# Electron Application

# Prerequisites #

* NodeJs and NPM :
  * Recommended : install NodeJs with Snap package : [Snap packages](https://github.com/nodesource/distributions/blob/master/README.md#snap)
  * Basically: `sudo snap install node --classic --channel=12`

* Tools for packaging (only if you want to build the linux app from your laptop)
  * sudo apt-get install --no-install-recommends -y reprepro

## Development

**Take care to use the correct NodeJS version**

Currently: use NodeJS 12
Recommended: install node via snap packages (see above) then you can switch between node/npm version via the commmand : `sudo snap refresh node --channel=10`

First you need to initialize dependencies: `npm install`

* Start a development version: `npm start`, if you need some args you must set them in the scripts/start in the package.json
* Run unit tests: `npm test`
* Run integration tests: `npm run test:e2e`
* Run all tests (unit & integration): `npm run test:all`

## Build

We use our xivo.solutions/electron-builder> docker image to build the Desktop App.

If you need to update the :
- update the xivo.solutions/electron-builder> image
  - update image
  - test
  - then tag it with the XiVO current version (like 2019.13.00)
  - and promote it
- then change the ELECTRON_BUILDER_IMAGE_TAG in xivo.solutions/xucmgt>docker_build.sh

The electron builder is launched to use - on our builder (i.e. jenkins) - the following folders to cache the electron or libs downloaded
- `~/.cache/electron`
- `~/.cache/electron-builder`

Note also that the electron/node_module is mounted in a docker named data volume. See DOCKER_VOLUME_NAME.
Therefore we need to remove this volume after each build.

To build on your machine you need to laucnh the `docker_build.sh` script.

### Auto-update

## Install & Update

### Windows 

Install from a running xucmgt instance by opening the following url: http://xucmgt-host:port/install/win64 where `xucmgt-host` is the host name or ip address of the xuc and `port` is the port it's available on.

### Debian

Add the following line to your /etc/apt/sources.list file :
```
deb http://xucmgt-host:port/updates/debian jessie contrib
```
where `xucmgt-host` is the host name or ip address of the xuc and `port` is the port it's available on.

### Update

Update mechanism is automatic. On debian, the update rely on the apt update process. On windows, when the application starts, it should check if a new version is available and offer to update if one is available.

## Options

Currently only two startup switches are supported:
-d - enable debug tools
--ignore-certificate-errors - activate internal chrome option allowing use with invalid (autosigned) certificates

## Useful technical design informations

### Inter-process communication

Currently, we use Electron IPC to communicate inbetween the different process. Here is a mapping of this whole
system :

![ipc map](./inter_process_communication.png)