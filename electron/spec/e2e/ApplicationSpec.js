require("ts/renderer/ApplicationAngular");
const Application = require('spectron').Application;
const electronPath = require('electron');
const fs = require('fs');
const ini = require('ini');

let getIniFileTemplate = () => {
  return {
    APP_PROTOCOL: '',

    APP_DOMAIN: '',

    APP_INTERFACE: '',

    APP_STARTUP: '',

    APP_CLOSE: ''
  };
};

describe("desktop application", function () {
  let app;

  jasmine.DEFAULT_TIMEOUT_INTERVAL = 10000;

  beforeAll(function () {
    app = new Application({
      path: electronPath,
      args: ['.']
    });

    return app.start();
  });

  afterAll(function () {
    if (app && app.isRunning()) {
      return app.stop();
    }
  });

  it('opens the window', () => app.client.waitUntilWindowLoaded());

  it('loads index page', () => app.client.waitUntilWindowLoaded()
    .then(() => app.webContents.getURL())
    .then(url => expect(url.slice(-2)).toBe('#/')));

  /*it('create a valid update url and check for new version available', (done) => {
    app.client.waitUntilWindowLoaded()
      .then(() => {
        app.client.electron.ipcRenderer.send('setUpdateUrl', 'localhost', false);
        console.log(app.client.electron.remote.autoUpdater.getFeedURL());
        expect(app.client.electron.remote.autoUpdater.getFeedURL()).toBe('localhost/updates/win64');
        done();
      });
  });

  it('creates a config folder and a new ini file if it does not exist', async () => {
    let iniFileTemplate = getIniFileTemplate();
    let newConfigPath = app.getPath('appData') + '/xivo-desktop-assistant/application/config';
    let newConfigFile = newConfigPath + '/xivoconfig.ini';
    if (fs.existsSync('./xivoconfig.ini')) {
      fs.unlinkSync('./xivoconfig.ini');
    }
    if (fs.existsSync('./config/xivoconfig.ini')) {
      fs.unlinkSync('./config/xivoconfig.ini');
    }
    await app.restart();

    expect(fs.existsSync('./xivoconfig.ini')).toBe(false);
    expect(fs.existsSync('./config/xivoconfig.ini')).toBe(false);
    expect(fs.existsSync(newConfigFile)).toBe(true);
    expect(ini.parse(fs.readFileSync(newConfigFile).toString('utf-8'))).toEqual(iniFileTemplate);
  });

  it('moves old ini file to new config folder and deletes the old one', async () => {
    let newConfigPath = app.getPath('appData') + '/xivo-desktop-assistant/application/config';
    let newConfigFile = newConfigPath + '/xivoconfig.ini';
    let iniFile = getIniFileTemplate();
    iniFile.APP_PROTOCOL = 'MY_PROTOCOL';
    iniFile.APP_DOMAIN = 'MY_DOMAIN';
    fs.unlinkSync('./config/xivoconfig.ini');

    fs.appendFileSync('./xivoconfig.ini', JSON.stringify(iniFile));
    await app.restart();
    app.client.waitUntilWindowLoaded();
    expect(JSON.parse(fs.readFileSync(newConfigFile).toString())).toEqual(iniFile);
    expect(fs.existsSync('./xivoconfig.ini')).toBe(false);
  });*/

});
 
