"use strict";

describe("DesktopSettings service", function () {
  var _realLocalStorage;

  beforeAll(() => {
    _realLocalStorage = localStorage;
  });

  beforeEach(() => {
    ngModule("desktopApp");
    var items = {};
    localStorage = {
      getItem: function (k) { return items[k]; },
      setItem: function (k, v) { items[k] = v; },
      clear: function () { console.log(items.length = 0); }
    }
  });

  afterAll(() => {
    localStorage = _realLocalStorage;
  });

  it('should get ucassistant as interface default value', inject(function (_desktopSettings_) {
    expect(_desktopSettings_.getInterface()).toEqual(_desktopSettings_.INTERFACE_UCASSISTANT);
  }));

  it('should get the localstorage interface key value when set to ucassistant', inject(function (_desktopSettings_) {
    localStorage.setItem(_desktopSettings_.KEY_APP_INTERFACE, _desktopSettings_.INTERFACE_UCASSISTANT);
    expect(_desktopSettings_.getInterface()).toEqual(_desktopSettings_.INTERFACE_UCASSISTANT);
  }));

  it('should get the localstorage interface key value when set to ccagent', inject(function (_desktopSettings_) {
    localStorage.setItem(_desktopSettings_.KEY_APP_INTERFACE, _desktopSettings_.INTERFACE_CCAGENT);
    expect(_desktopSettings_.getInterface()).toEqual(_desktopSettings_.INTERFACE_CCAGENT);
  }));

  it('should get ccagent as interface if it is set in server', inject(function (_desktopSettings_) {
    localStorage.setItem(_desktopSettings_.KEY_APP_SERVER, "http://www.azerty.com/ccagent");
    expect(_desktopSettings_.getInterface()).toEqual(_desktopSettings_.INTERFACE_CCAGENT);
  }));

  it('should get ucassistant as interface if it is set in server and interface key is empty', inject(function (_desktopSettings_) {
    localStorage.setItem(_desktopSettings_.KEY_APP_SERVER, "http://www.azerty.com/ucassistant");
    expect(_desktopSettings_.getInterface()).toEqual(_desktopSettings_.INTERFACE_UCASSISTANT);
  }));

  it('should get ucassistant as interface if no interface is set in server and interface key is empty', inject(function (_desktopSettings_) {
    localStorage.setItem(_desktopSettings_.KEY_APP_SERVER, "http://www.azerty.com");
    expect(_desktopSettings_.getInterface()).toEqual(_desktopSettings_.INTERFACE_UCASSISTANT);
  }));

  it('should ignore server interface and get ucassistant interface key if it is set', inject(function (_desktopSettings_) {
    localStorage.setItem(_desktopSettings_.KEY_APP_SERVER, "http://www.azerty.com/ccagent");
    localStorage.setItem(_desktopSettings_.KEY_APP_INTERFACE, _desktopSettings_.INTERFACE_UCASSISTANT);
    expect(_desktopSettings_.getInterface()).toEqual(_desktopSettings_.INTERFACE_UCASSISTANT);
  }));

  it('should ignore server interface and get ccagent interface key if it is set', inject(function (_desktopSettings_) {
    localStorage.setItem(_desktopSettings_.KEY_APP_SERVER, "http://www.azerty.com/ucassistant");
    localStorage.setItem(_desktopSettings_.KEY_APP_INTERFACE, _desktopSettings_.INTERFACE_CCAGENT);
    expect(_desktopSettings_.getInterface()).toEqual(_desktopSettings_.INTERFACE_CCAGENT);
  }));

  it('should extract the last part of the server address', inject(function (_desktopSettings_) {
    let server1 = "http://www.azerty.com/ucassistant";
    expect(_desktopSettings_.getInterfaceFromServerAddress(server1)).toEqual('ucassistant');

    let server2 = "http://www.azerty.com/ccagent";
    expect(_desktopSettings_.getInterfaceFromServerAddress(server2)).toEqual('ccagent');

    let server3 = "http://www.azerty.com/";
    expect(_desktopSettings_.getInterfaceFromServerAddress(server3)).toEqual(undefined);

    let server4 = "http://www.azerty.com";
    expect(_desktopSettings_.getInterfaceFromServerAddress(server4)).toEqual(undefined);

    let server5 = "http://www.azerty.com/uiop/ccagent";
    expect(_desktopSettings_.getInterfaceFromServerAddress(server5)).toEqual('ccagent');

    let server6 = "http://www.azerty.com/uiop/";
    expect(_desktopSettings_.getInterfaceFromServerAddress(server6)).toEqual(undefined);

    let server7 = "http://www.azerty.com/uiop/qsdfg";
    expect(_desktopSettings_.getInterfaceFromServerAddress(server7)).toEqual(undefined);
  }));

  it('should get the server update url', inject(function (_desktopSettings_) {
    let server1 = "http://www.azerty.com/ucassistant";
    expect(_desktopSettings_.getServerAddressWithoutInterface(server1)).toEqual('http://www.azerty.com');

    let server2 = "http://www.azerty.com/ccagent";
    expect(_desktopSettings_.getServerAddressWithoutInterface(server2)).toEqual('http://www.azerty.com');

    let server3 = "http://www.azerty.com/";
    expect(_desktopSettings_.getServerAddressWithoutInterface(server3)).toEqual('http://www.azerty.com');

    let server4 = "http://www.azerty.com";
    expect(_desktopSettings_.getServerAddressWithoutInterface(server4)).toEqual('http://www.azerty.com');

    let server5 = "http://www.azerty.com/uiop/ccagent";
    expect(_desktopSettings_.getServerAddressWithoutInterface(server5)).toEqual('http://www.azerty.com/uiop');

    let server6 = "http://www.azerty.com/uiop/";
    expect(_desktopSettings_.getServerAddressWithoutInterface(server6)).toEqual('http://www.azerty.com/uiop');

    let server7 = "http://www.azerty.com/uiop/qsdfg";
    expect(_desktopSettings_.getServerAddressWithoutInterface(server7)).toEqual('http://www.azerty.com/uiop/qsdfg');

  }));

  it('should take off the protocol from the server address', inject(function (_desktopSettings_) {
    let server1 = "http://www.azerty.com/ucassistant";

    expect(_desktopSettings_.getServerAddressWithoutProtocol(server1)).toEqual('www.azerty.com/ucassistant');

    let server2 = "www.azerty.com/ucassistant";

    expect(_desktopSettings_.getServerAddressWithoutProtocol(server2)).toEqual('www.azerty.com/ucassistant');

    let server3 = "https://www.azerty.com";

    expect(_desktopSettings_.getServerAddressWithoutProtocol(server3)).toEqual('www.azerty.com');

    let server4 = "myhttpserver";

    expect(_desktopSettings_.getServerAddressWithoutProtocol(server4)).toEqual('myhttpserver');

    let server5 = "myhttp://serverhttps://";

    expect(_desktopSettings_.getServerAddressWithoutProtocol(server5)).toEqual('myhttp://serverhttps://');

  }));

  it('should return the protocol from the server address', inject(function (_desktopSettings_) {
    let server1 = "http://www.azerty.com/ucassistant";

    expect(_desktopSettings_.getServerAddressProtocol(server1)).toEqual('http');

    let server2 = "www.azerty.com/ucassistant";

    expect(_desktopSettings_.getServerAddressProtocol(server2)).toEqual('https');

    let server3 = "https://www.azerty.com";

    expect(_desktopSettings_.getServerAddressProtocol(server3)).toEqual('https');

  }));

  it('should return the server address without protocol or interface', inject(function (_desktopSettings_) {
    let server1 = "http://www.azerty.com/ucassistant";

    expect(_desktopSettings_.getServerAddressOnly(server1)).toEqual('www.azerty.com');

    let server2 = "www.azerty.com/ucassistant";

    expect(_desktopSettings_.getServerAddressOnly(server2)).toEqual('www.azerty.com');

    let server3 = "https://www.azerty.com";

    expect(_desktopSettings_.getServerAddressOnly(server3)).toEqual('www.azerty.com');

  }));

});