"use strict";

require("ts/renderer/ApplicationAngular");

describe("SettingsController", function() {

  beforeEach(() => {
    ngModule("desktopApp");
  });
  
  var $controller;
  beforeEach(inject(function(_$controller_, $injector, _updateService_) {
    $controller = _$controller_;
    spyOn(_updateService_, 'setUpdateShortcut');
    spyOn(_updateService_, 'setUpdateCloseTray');
    spyOn(_updateService_, 'setUpdateStartUp');
  }));
  
  it("can be started", inject(function() {
    var $scope = {};
    var ctrl = $controller("SettingsController", {$scope:$scope});

    expect(ctrl).toBeDefined();
  }));

  it("load default url", inject(function(_desktopSettings_, _$state_) {
    var $scope = {};
    spyOn(_desktopSettings_, "get").and.callFake((key) => {
      switch(key) {
      case 'APP_PROTOCOL': return 'http';
      case 'APP_DOMAIN': return 'go.to.somewhere';
      }
      return null;
    });
    var ctrl = $controller("SettingsController", {$scope:$scope, desktopSettings: _desktopSettings_});
    expect($scope.serverAddress).toEqual('go.to.somewhere');
    expect($scope.protocol).toEqual('http');
  }));

  it("save remote url in desktopSettings", inject(function(_desktopSettings_, _$state_, _updateService_) {
    var $scope = {};
    spyOn(_desktopSettings_, "get").and.callFake((key) => {
      switch(key) {
      case 'APP_PROTOCOL': return 'http';
      case 'APP_DOMAIN': return 'go.to.somewhere';
      case 'APP_INTERFACE': return 'ccagent';
      }
      return null;
    });
    spyOn(_desktopSettings_, "set");
    spyOn(_updateService_, 'setUpdateUrl');
    var ctrl = $controller("SettingsController", {$scope:$scope, desktopSettings: _desktopSettings_, updateService: _updateService_});
    $scope.protocol = "http";
    $scope.serverAddress = "go.elsewhere";
    $scope.appInterface = "ucassistant";
    $scope.save();
    expect(_desktopSettings_.set).toHaveBeenCalledWith(_desktopSettings_.KEY_APP_PROTOCOL, "http");
    expect(_desktopSettings_.set).toHaveBeenCalledWith(_desktopSettings_.KEY_APP_SERVER, "go.elsewhere");
    expect(_desktopSettings_.set).toHaveBeenCalledWith(_desktopSettings_.KEY_APP_INTERFACE, "ucassistant");
  }));

  it("set update url", inject(function(_desktopSettings_, _$state_, _updateService_) {
    var $scope = {};
    spyOn(_desktopSettings_, "get").and.callFake((key) => {
      switch(key) {
      case 'APP_PROTOCOL': return 'http';
      case 'APP_DOMAIN': return 'go.to.somewhere';
      case 'APP_INTERFACE': return 'ccagent';
      }
      return null;
    });
    spyOn(_updateService_, 'setUpdateUrl');
    var ctrl = $controller("SettingsController", {$scope:$scope, desktopSettings: _desktopSettings_, updateService: _updateService_});
    $scope.protocol = "http";
    $scope.serverAddress = "go.elsewhere";
    $scope.appInterface = "ucassistant";
    $scope.save();
    expect(_updateService_.setUpdateUrl).toHaveBeenCalledWith('http://go.elsewhere', true);
  }));

  it("builds list of available keys with appropriate labels", inject(function(_desktopSettings_, _$state_, _updateService_) {
    var $scope = {};
    var ctrl = $controller("SettingsController", {$scope:$scope, desktopSettings: _desktopSettings_, updateService: _updateService_});
    expect($scope.keyList).toContain({id :'A', label: 'A'});
    expect($scope.keyList).toContain({id :'Up', label: 'Haut'});
    expect($scope.keyList).toContain({id :'CmdOrCtrl', label: 'Cmd Ou Ctrl +'});
    spyOn(_updateService_, 'setUpdateUrl');
    spyOn(_desktopSettings_, "set");
  }));

  it("creates default shortcut if no shortcut defined", inject(function(_desktopSettings_, _$state_) {
    var $scope = {};
    spyOn(_desktopSettings_, "get").and.callFake((key) => {
      switch(key) {
        case 'APP_SHORTCUT': return null;
      }
      return null;
    });
    var ctrl = $controller("SettingsController", {$scope:$scope, desktopSettings: _desktopSettings_});
    expect($scope.shortcut.current[0].id).toEqual('CmdOrCtrl');
    expect($scope.shortcut.current[1].id).toEqual('Space');
    expect($scope.shortcut.current[0].label).toEqual('Cmd Ou Ctrl +');
    expect($scope.shortcut.current[1].label).toEqual('Espace');
  }));

  it("load already defined shortcut", inject(function(_desktopSettings_, _$state_) {
    var $scope = {};
    spyOn(_desktopSettings_, "get").and.callFake((key) => {
      switch(key) {
        case 'APP_SHORTCUT': return "Ctrl+X";
      }
      return null;
    });
    var ctrl = $controller("SettingsController", {$scope:$scope, desktopSettings: _desktopSettings_});
    expect($scope.shortcut.current[0].id).toEqual('Ctrl');
    expect($scope.shortcut.current[1].id).toEqual('X');
    expect($scope.shortcut.current[0].label).toEqual('Ctrl +');
    expect($scope.shortcut.current[1].label).toEqual('X');
  }));

  it("disable shortcut if saved to null string", inject(function(_desktopSettings_, _$state_) {
    var $scope = {};
    spyOn(_desktopSettings_, "get").and.callFake((key) => {
      switch(key) {
        case 'APP_SHORTCUT': return "null";
      }
      return null;
    });
    var ctrl = $controller("SettingsController", {$scope:$scope, desktopSettings: _desktopSettings_});
    expect($scope.shortcut.current).toEqual(null);
  }));

  it("set shortcut key combination if valid", inject(function(_desktopSettings_, _$state_, _updateService_) {
    var $scope = {};
    spyOn(_desktopSettings_, "get").and.callFake((key) => {
      switch(key) {
        case 'APP_SHORTCUT': return "Ctrl+Shift+Y";
      }
      return null;
    });
    var ctrl = $controller("SettingsController", {$scope:$scope, desktopSettings: _desktopSettings_, updateService: _updateService_});
    spyOn(_updateService_, 'setUpdateUrl');
    spyOn(_desktopSettings_, "set");
    expect($scope.shortcut.current[0].id).toEqual('Ctrl');
    expect($scope.shortcut.current[1].id).toEqual('Shift');
    expect($scope.shortcut.current[2].id).toEqual('Y');
    expect($scope.shortcut.current[0].label).toEqual('Ctrl +');
    expect($scope.shortcut.current[1].label).toEqual('Maj +');
    expect($scope.shortcut.current[2].label).toEqual('Y');
    $scope.save();
    expect(_desktopSettings_.set.calls.allArgs()).toEqual(
      [
        [ "APP_SHORTCUT", "Ctrl+Shift+Y" ],
        [ "APP_STARTUP", false ],
        [ "APP_CLOSE", false ],
        [ "APP_PROTOCOL", "https" ],
        ["APP_DOMAIN", undefined],
        ["APP_INTERFACE", 'ucassistant']
      ]
    );
  }));

  it("should not retrieve shortcut key combination if invalid in localstorage", inject(function(_desktopSettings_, _$state_, _updateService_) {
    var $scope = {};
    spyOn(_desktopSettings_, "get").and.callFake((key) => {
      switch(key) {
        case 'APP_SHORTCUT': return "XXX";
      }
      return null;
    });
    var ctrl = $controller("SettingsController", {$scope:$scope, desktopSettings: _desktopSettings_, updateService: _updateService_});
    spyOn(_updateService_, 'setUpdateUrl');
    spyOn(_desktopSettings_, "set")
    $scope.save();
    expect(_desktopSettings_.set.calls.allArgs()).toEqual(
      [
        [ "APP_SHORTCUT", "null" ],
        [ "APP_STARTUP", false ],
        [ "APP_CLOSE", false ],
        [ "APP_PROTOCOL", "https" ],
        ["APP_DOMAIN", undefined],
        ["APP_INTERFACE", 'ucassistant']
      ]
    );
  }));

  it("save startup in desktopSettings", inject(function(_desktopSettings_, _$state_, _updateService_) {
    var $scope = {};
    spyOn(_desktopSettings_, "get").and.callFake((key) => {
      switch(key) {
      case 'APP_STARTUP': return 'false';
      }
      return null;
    });
    spyOn(_updateService_, 'setUpdateUrl');
    spyOn(_desktopSettings_, "set");
    var ctrl = $controller("SettingsController", {$scope:$scope, desktopSettings: _desktopSettings_, updateService: _updateService_});
    $scope.startup = true;
    $scope.save();
    expect(_desktopSettings_.set.calls.allArgs()).toEqual(
        [
          [ "APP_SHORTCUT", "CmdOrCtrl+Space" ],
          [ "APP_STARTUP", true ],
          [ "APP_CLOSE", false ],
          [ "APP_PROTOCOL", "https"],
        ["APP_DOMAIN", undefined],
          ["APP_INTERFACE", 'ucassistant']
        ]
      );
  }));

  it("save close on tray in desktopSettings", inject(function(_desktopSettings_, _$state_, _updateService_) {
    var $scope = {};
    spyOn(_desktopSettings_, "get").and.callFake((key) => {
      switch(key) {
      case 'APP_CLOSE': return 'false';
      }
      return null;
    });
    spyOn(_updateService_, 'setUpdateUrl');
    spyOn(_desktopSettings_, "set");
    var ctrl = $controller("SettingsController", {$scope:$scope, desktopSettings: _desktopSettings_, updateService: _updateService_});
    $scope.close = true;
    $scope.save();
    expect(_desktopSettings_.set.calls.allArgs()).toEqual(
        [
          [ "APP_SHORTCUT", "CmdOrCtrl+Space" ],
          [ "APP_STARTUP", false ],
          [ "APP_CLOSE", true ],
          [ "APP_PROTOCOL", "https"],
        ["APP_DOMAIN", undefined],
          ["APP_INTERFACE", 'ucassistant']
        ]
      );
  }));

 

});