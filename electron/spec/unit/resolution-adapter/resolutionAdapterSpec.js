"use strict";
const ResolutionAdapter = require('ts/main/ApplicationResolutionManager').ResolutionAdapter;

describe('Resolution adapter tests', function () {
  const win = { on: () => { } };
  const resolutionAdapter = new ResolutionAdapter(win);

  it('should calculate a ratio based on a width and a height', function () {
    let ratio = resolutionAdapter.calculateRatio(400, 800);
    expect(ratio).toBe(0.5);
  });

  it('should calculate zoom correctly', function () {
    let zoom = resolutionAdapter.calculateZoom(400, 800);
    expect(zoom).toBe(0.5);
  });

  it('should calculate the width based on the height and the ratio', function () {
    let ratio = 0.5;
    let width = resolutionAdapter.scaleWidth(400, ratio);
    expect(width).toBe(200);
  });

});
