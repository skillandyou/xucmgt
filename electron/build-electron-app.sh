#!/bin/bash
set -ex

cleanup_npm_build() {
    # Cleanup to be done:
    # - when script ends
    # - AND when script fails

    # dist must be cleaned, but afterwards...
    # so we need to give all rights (because container is run as root)
    # do it only if dist dir exists
    [ -d dist ] && chmod -R 777 dist
    [ -d build/application ] && chmod -R 777 build/application
}

main() {
    # Trap EXIT
    # - will trap clean exits
    # - and exit because of failure (because of the set -e above)
    trap cleanup_npm_build EXIT

    if [ -n "${TARGET_VERSION_SEMVER}" ]; then
        set +e
        npm --no-git-tag-version -f version  "${TARGET_VERSION_SEMVER}" >/dev/null 2>&1
        set -e
    fi

    npm install

    # Launch tests with virtual X environment
    # xvfb-run npm run test:all
    npm test

    # Build Windows Exe (must be first as it cleans the dist dir)
    npm run dist-win64

    # Build Debian Package
    npm run dist-linux64
}

main "${@}"
