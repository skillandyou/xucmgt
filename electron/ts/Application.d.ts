declare namespace XiVO {

    interface Config {
        [key: string]: string;
        APP_PROTOCOL: string;
        APP_DOMAIN: string;
        APP_INTERFACE: string;
        APP_STARTUP: string;
        APP_CLOSE: string;
    }

    interface OptionsDefinition {
        [key: string]: string | boolean | BooleanConstructor | StringConstructor | undefined;
        name: string;
        alias: string;
        type: BooleanConstructor | StringConstructor;
        defaultValue?: string | boolean | undefined;
    }

    interface Options {
        [key: string]: boolean | string | undefined;
    }

    interface Message {
        [key: string]: string;
    }

    interface PositionRectangle {
        [key: string]: number
    }

}