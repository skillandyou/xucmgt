"use strict";

window.$ = window.jQuery = require('jquery');
require('bootstrap');
require('angular');
require('angular-translate');
require('angular-ui-router');
require('ui-select');
require('module').globalPaths.push(__dirname);
const ipcRenderer = window.require('electron').ipcRenderer; // to be able to mock it (see angular-helper)
const isAccelerator = require("electron-is-accelerator");
const _ = require('lodash');
const Shortcuts = require('ts/helpers/ApplicationShortcuts');
const { webFrame } = require('electron');

var i18n = require('ts/helpers/translate');
var shortcuts = new Shortcuts();

var desktopApp = angular.module('desktopApp', ['pascalprecht.translate', 'ui.router','ui.select'])
    .config(function($translateProvider) {
      $translateProvider.useMissingTranslationHandler('i18nProxy');
      // force locale to be set before $translate is instantiated
      i18n.setLocale(ipcRenderer.sendSync('i18n','locale'));
    })
    .config(function($stateProvider, $urlRouterProvider) {
      $stateProvider
        .state('main', {
          url: '/',
          templateUrl: 'view/main.tpl.html',
          controller: 'MainController'
        })
        .state('main.settings', {
          templateUrl: 'view/main.settings.tpl.html',
          controller: 'SettingsController'
        })
        .state('main.updateavailable', {
          templateUrl: 'view/main.updateavailable.tpl.html',
          controller: 'UpdateAvailableController'
        })
        .state('loadingerror', {
          url: '/loadingerror',
          templateUrl: 'view/loadingerror.tpl.html',
          controller: 'LoadingErrorController'
        });

      $urlRouterProvider.otherwise('/');
    }).run(function($rootScope) {
      $rootScope.title = i18n.__('APP_NAME');
    });


desktopApp.factory('i18nProxy', function () {
  return function (translationId, $uses, interpolateParams) {
    return i18n.__(translationId, interpolateParams);
  };
});

desktopApp.factory('desktopSettings', function () {

  const _KEY_APP_PROTOCOL = 'APP_PROTOCOL';
  const _KEY_APP_SERVER = 'APP_DOMAIN';
  const _KEY_APP_INTERFACE = 'APP_INTERFACE';
  const _KEY_APP_SHORTCUT = 'APP_SHORTCUT';
  const _KEY_APP_CLOSE = 'APP_CLOSE';
  const _KEY_APP_STARTUP = 'APP_STARTUP';
  const _INTERFACE_CCAGENT = "ccagent";
  const _INTERFACE_UCASSISTANT = "ucassistant";
  const _DEFAULT_SHORTCUT = "CmdOrCtrl+Space";

  const _get = function(key) {
    return localStorage.getItem(key);
  };

  const _set = function(key, value) {
    return localStorage.setItem(key, value);
  };

  const _getServerAddressOnly = (serverAddress) => {
    return _getServerAddressWithoutInterface(_getServerAddressWithoutProtocol(serverAddress));
  };

  const _getServerAddressWithoutInterface = (serverAddress) => {
    if (!serverAddress) {
      return undefined;
    }
    let interfaceInServerPath = serverAddress
      .split('/')
      .pop();
    if (interfaceInServerPath == _INTERFACE_CCAGENT || interfaceInServerPath == _INTERFACE_UCASSISTANT) {
      serverAddress = serverAddress
        .split('/')
        .slice(0, -1)
        .join('/');
    }
    return serverAddress.endsWith('/') ? serverAddress.slice(0, -1) : serverAddress;
  };

  const _getServerAddressWithoutProtocol = (serverAddress) => {
    if (!serverAddress) {
      return undefined;
    }
    if (serverAddress.startsWith('http://') || serverAddress.startsWith('https://')) {
      serverAddress = serverAddress.split('//').slice(1).join('/');
    }
    return serverAddress;
  };

  const _getServerAddressProtocol = (serverAddress) => {
    if (serverAddress) {
      if (serverAddress.startsWith('http://')) {
        return 'http';
      }
    }
    return 'https';
  };

  const _getInterfaceFromServerAddress = (serverAddress) => {
    if (serverAddress) {
      let serverUrl = new URL(serverAddress);
      let appInterface = serverUrl.pathname
        .split('/')
        .pop();
      if (appInterface == _INTERFACE_CCAGENT || appInterface == _INTERFACE_UCASSISTANT) {
        return appInterface;
      }
    }
    return undefined;
  };

  const _getInterface = () => {
    let appInterface = _get(_KEY_APP_INTERFACE);
    let serverAddress = _get(_KEY_APP_SERVER);
    let serverProtocol = _get(_KEY_APP_PROTOCOL);
    if (appInterface) {
      return appInterface;
    } else if (serverAddress) {
      if (serverProtocol) {
        serverAddress = serverProtocol + '://' + serverAddress;
      } else {
        serverAddress = 'https://' + serverAddress;
      }
      appInterface = _getInterfaceFromServerAddress(serverAddress);
      return !appInterface ? _INTERFACE_UCASSISTANT : appInterface;
    }
    return _INTERFACE_UCASSISTANT;
  };

  return {
    KEY_APP_PROTOCOL: _KEY_APP_PROTOCOL,
    KEY_APP_SERVER: _KEY_APP_SERVER,
    KEY_APP_INTERFACE: _KEY_APP_INTERFACE,
    KEY_APP_SHORTCUT: _KEY_APP_SHORTCUT,
    KEY_APP_CLOSE: _KEY_APP_CLOSE,
    KEY_APP_STARTUP: _KEY_APP_STARTUP,
    DEFAULT_SHORTCUT: _DEFAULT_SHORTCUT,
    INTERFACE_CCAGENT: _INTERFACE_CCAGENT,
    INTERFACE_UCASSISTANT: _INTERFACE_UCASSISTANT,
    getServerAddressOnly: _getServerAddressOnly,
    getInterfaceFromServerAddress: _getInterfaceFromServerAddress,
    getServerAddressWithoutInterface: _getServerAddressWithoutInterface,
    getServerAddressWithoutProtocol: _getServerAddressWithoutProtocol,
    getServerAddressProtocol: _getServerAddressProtocol,
    getInterface: _getInterface,
    get: _get,
    set: _set
  };
});

desktopApp.factory('updateService',  () => {
  var error = false;

  const _updateToken = (scope, token) => {
    scope.url += '?token=' + token;
  };

  const _setUpdateUrl = (url, reload) => {
    ipcRenderer.send('setUpdateUrl', url, reload);
  };

  const _setUpdateShortcut = (keyCombination) => {
    ipcRenderer.send('setUpdateGlobalShortcut', keyCombination != 'null' ? keyCombination : null);
  };

  const _setUpdateCloseTray = (isTray) => {
    ipcRenderer.send('setUpdateCloseTray', isTray);
  };

  const _setUpdateStartUp = (isBoot) => {
    ipcRenderer.send('setUpdateStartUp', isBoot);
  };

  const _throwUrlError = (url) => {
    ipcRenderer.send('urlError', url);
  };

  const _setError = () => {
    error = true;
  };

  const _hasError = () => {
    return error;
  };

  const _clearError = () => {
    error = false;
  };

  const _getUpdateShortcut = (shortcut) => {
    return _.join(_.map(shortcut, (key) => { return key.id; }), '+');
  };

  const _displayShortcut = (shortcut) => {
    return _.join(_.map(shortcut, (key) => { return key.label; }), ' ');
  };

  const _buildKeyList = () => {
    var localizedModifiers = _.map(shortcuts.LOCALIZED_MODIFIER_KEYS, (value) => {
      return { id: value, label: i18n.__('SETTINGS_SHORTCUT_KEY_'+value) + " +" };
    });
    var localizedKeys = _.map(shortcuts.LOCALIZED_KEYS, (value) => {
      return { id: value, label: i18n.__('SETTINGS_SHORTCUT_KEY_'+value) };
    });
    var commonKeys = _.map(shortcuts.COMMON_KEYS, (value) => {
      return { id: value, label: value };
    });

    return _.concat(localizedModifiers, localizedKeys, commonKeys);
  };

  const _quitAndInstall = () => {
    ipcRenderer.send('quitAndInstall');
  };
  
  return {
    updateToken: _updateToken,
    setUpdateUrl: _setUpdateUrl,
    setUpdateShortcut: _setUpdateShortcut,
    setUpdateCloseTray: _setUpdateCloseTray,
    setUpdateStartUp: _setUpdateStartUp,
    getUpdateShortcut: _getUpdateShortcut,
    displayShortcut: _displayShortcut,
    buildKeyList: _buildKeyList,
    quitAndInstall: _quitAndInstall,
    throwUrlError: _throwUrlError,
    hasError: _hasError,
    setError: _setError,
    clearError: _clearError
  };
});

desktopApp.directive('validShortcut', function (updateService) {
  return {
    require: 'ngModel',
    link: function ($scope, elm, attrs, ctrl) {
      $scope.$watch(attrs.ngModel, function(viewValue) {
        ctrl.$setValidity('format', _.isEmpty(_.trim(viewValue)) || isAccelerator(updateService.getUpdateShortcut(viewValue)));
      });
    }
  };
});

desktopApp.controller('MainController', function MainController($scope, desktopSettings, $state, updateService, $log) {
  const webview = document.querySelector('webview');
  let protocol = desktopSettings.get(desktopSettings.KEY_APP_PROTOCOL);
  let serverAddress = desktopSettings.get(desktopSettings.KEY_APP_SERVER);
  let appInterface = desktopSettings.get(desktopSettings.KEY_APP_INTERFACE);

  const protocolOrServerAddressIsUnset = (protocol, serverAddress) => {
    return protocol == null || serverAddress == null;
  };

  const updateAppInterface = () => {
    let newAppInterface = desktopSettings.getInterface();
    desktopSettings.set(desktopSettings.KEY_APP_INTERFACE, newAppInterface);
    return newAppInterface;
  };

  const updateServerAddress = (serverAddress) => {
    let serverAddressOnly = desktopSettings.getServerAddressOnly(serverAddress);
    desktopSettings.set(desktopSettings.KEY_APP_SERVER, serverAddressOnly);
    return serverAddressOnly;
  };

  const getApplicationUrl = (protocol, serverAddress) => {
    return protocol + '://' + serverAddress + '/' + appInterface;
  };

  $scope.setKeysAndUpdateUrl = (protocol, serverAddress) => {
    if (appInterface == undefined) {
      appInterface = updateAppInterface(serverAddress);
    }
    $scope.domain = updateServerAddress(serverAddress);
    $scope.url = getApplicationUrl(protocol, $scope.domain);
    updateService.setUpdateUrl(protocol + '://' + $scope.domain, false);
  };

  $scope.throwWebviewError = () => {
    $log.error('Error failed to load webview - ' + webview.src);
    updateService.setError();
    if ($state.current.name != 'main.settings') {
      updateService.throwUrlError(webview.src);
    }
  };

  $scope.successWebviewLoad = () => {
    $log.info('Webview is loaded');
    updateService.clearError();
  };

  webview.addEventListener('did-fail-load', () => {
    $scope.throwWebviewError();
  });

  webview.addEventListener('load-commit', () => {
    $scope.successWebviewLoad();
  });

  ipcRenderer.on('sref' , (event , location) => {
    $state.go(location);
  });

  ipcRenderer.on('msg' , (event , data) => {
    webview.executeJavaScript('window.postMessage('+ data +', "/")');
  });

  ipcRenderer.on('HiDPI', (event, data) => {
    let zoomFactor = (1 / data);
    webFrame.setZoomFactor(parseFloat(zoomFactor));
  });

  ipcRenderer.on('token', (event, data) => {
    updateService.updateToken($scope, data);
  });

  let close = (desktopSettings.get(desktopSettings.KEY_APP_CLOSE) == 'true');
  updateService.setUpdateCloseTray(close);

  if (protocolOrServerAddressIsUnset(protocol, serverAddress)) {
    $state.go('main.settings');
  } else {
    $scope.setKeysAndUpdateUrl(protocol, serverAddress);
  }
  
  let shortcut = desktopSettings.get(desktopSettings.KEY_APP_SHORTCUT) || desktopSettings.DEFAULT_SHORTCUT;
  updateService.setUpdateShortcut(shortcut);

  $scope.showWebApp = () => {
    return $state.current.name === 'main';
  };
});

desktopApp.controller('SettingsController', function SettingsController($scope, desktopSettings, $state, updateService) {
  $scope.protocol = desktopSettings.get(desktopSettings.KEY_APP_PROTOCOL) || desktopSettings.getServerAddressProtocol(desktopSettings.get(desktopSettings.KEY_APP_SERVER));
  $scope.serverAddress = desktopSettings.getServerAddressOnly(desktopSettings.get(desktopSettings.KEY_APP_SERVER));
  $scope.appInterface = desktopSettings.get(desktopSettings.KEY_APP_INTERFACE) || desktopSettings.getInterface(desktopSettings.KEY_APP_SERVER);
  $scope.close = (desktopSettings.get(desktopSettings.KEY_APP_CLOSE) == 'true');
  $scope.startup = (desktopSettings.get(desktopSettings.KEY_APP_STARTUP) == 'true');
  $scope.keyList = updateService.buildKeyList();
  $scope.shortcut = {};

  let shortcut = desktopSettings.get(desktopSettings.KEY_APP_SHORTCUT) || desktopSettings.DEFAULT_SHORTCUT;
  if (shortcut == 'null' || !isAccelerator(shortcut)){
    $scope.shortcut.current = null;
  }
  else {
    $scope.shortcut.current = _.map(_.split(shortcut,'+'),
      function(key) { return _.find($scope.keyList,
        function(item){
          return key == item.id;
        });
      }
    );
  }

  const _getCurrentInterface = function () {
    if ($scope.appInterface) {
      return $scope.appInterface;
    }
    return desktopSettings.getInterface() || 'ucassistant';
  };

  $scope.save = function() {
    let shortcut = updateService.getUpdateShortcut($scope.shortcut.current);
    if (_.isEmpty(_.trim(shortcut))){
      desktopSettings.set(desktopSettings.KEY_APP_SHORTCUT, 'null');
    }
    if (isAccelerator(shortcut)) {
      desktopSettings.set(desktopSettings.KEY_APP_SHORTCUT, shortcut);
      updateService.setUpdateShortcut(shortcut);
    }
    desktopSettings.set(desktopSettings.KEY_APP_STARTUP, $scope.startup);
    updateService.setUpdateStartUp($scope.startup);
    desktopSettings.set(desktopSettings.KEY_APP_CLOSE, $scope.close);
    updateService.setUpdateCloseTray($scope.close);
    desktopSettings.set(desktopSettings.KEY_APP_PROTOCOL, $scope.protocol);
    $scope.appInterface = _getCurrentInterface();
    $scope.serverAddress = desktopSettings.getServerAddressWithoutInterface($scope.serverAddress);
    desktopSettings.set(desktopSettings.KEY_APP_SERVER, $scope.serverAddress ? $scope.serverAddress : undefined);
    desktopSettings.set(desktopSettings.KEY_APP_INTERFACE, $scope.appInterface ? $scope.appInterface : undefined);
    let url = $scope.protocol + '://' + $scope.serverAddress;
    if ($scope.url != url || updateService.hasError()) {
      updateService.setUpdateUrl(url, true);
    }
    $state.go('main');
  };

  $scope.displayShortcutError = function() {
    return updateService.displayShortcut($scope.shortcut.current) + ' ' + i18n.__('SETTINGS_SHORTCUT_WRONG_FORMAT');
  };

  $scope.groupKey = function(key) {
    return (shortcuts.LOCALIZED_MODIFIER_KEYS.indexOf(key.id) > -1) ? i18n.__('SETTINGS_SHORTCUT_MODIFIERS') : i18n.__('SETTINGS_SHORTCUT_KEYS');
  };

});

desktopApp.controller('LoadingErrorController', function SettingsController($scope, desktopSettings, $state, $timeout) {
  var _retryTimeout = 60;
  $scope.retry = { delay: _retryTimeout };
  
  var _retryCountdown = function() {
    $scope.retry.delay--;
    if($scope.retry.delay <= 0 && $state.current.name !== 'main.settings') {
      $state.go('main');
    } else {
      $timeout(_retryCountdown, 1000);
    }
  };

  ipcRenderer.on('sref' , (event , location) => {
    $state.go(location);
  });

  ipcRenderer.on('HiDPI', (event, data) => {
    let zoomFactor = (1 / data);
    webFrame.setZoomFactor(parseFloat(zoomFactor));
  });

  $scope.remoteUrl = desktopSettings.get(desktopSettings.KEY_APP_PROTOCOL) + '://' + desktopSettings.get(desktopSettings.KEY_APP_SERVER);
  $timeout(_retryCountdown, 1000);
});

desktopApp.controller('UpdateAvailableController', function UpdateAvailableController($scope, updateService) {
  $scope.quitAndInstall = function() { updateService.quitAndInstall(); };
});

desktopApp.filter('trusted', ['$sce', function ($sce) {
  return function(url) {
    return $sce.trustAsResourceUrl(url);
  };
}]);
