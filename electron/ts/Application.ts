/// <reference path="Application.d.ts" />
export { }

(function () { if (require('electron-squirrel-startup')) return })()
  
const { app, BrowserWindow, globalShortcut, Menu, Tray, autoUpdater, nativeImage } = require('electron')
const path = require('path')
const url = require('url')
const isDev = require('electron-is-dev')
const os = require('os')
const _ = require('lodash')
const Registry = require('rage-edit').Registry
const windowStateKeeper = require('electron-window-state')
const { localStorage } = require('electron-browser-storage')
const fs = require('fs')
const log = require('electron-log')
const i18n = require('./helpers/translate')
const commandLineArgs = require('command-line-args')

import { ResolutionAdapter } from './main/ApplicationResolutionManager'
import EventsListener from './main/ApplicationEventsListener'
import ApplicationMenu from './main/ApplicationMenu'
import ConfigManager from './main/ApplicationConfigManager'
import PositionMgt from './main/ApplicationPositionDocking'

const optionDefinitions: Array<XiVO.OptionsDefinition> = [
  {
    name: 'debug',
    alias: 'd',
    type: Boolean,
    defaultValue: true
  },
  {
    name: 'ignore-certificate-errors',
    alias: 'i',
    type: Boolean,
    defaultValue: true
  },
  {
    name: 'token',
    alias: 't',
    type: String
  }
]

const acceptedProtocols: Array<string> = ["tel", "callto"]
const appName: string = app.getName()
const logsPath = app.getPath('appData') + '/xivo-desktop-assistant/application/logs'

let launchOptions: XiVO.Options = {
  debug: false,
  ignoreCertificateErrors: false
}

let closingParameters: XiVO.Options = {
  closeInTray: false,
  quit: false,
  confirm: undefined
}

if (!fs.existsSync(logsPath)) {
  fs.mkdirSync(logsPath, { recursive: true })
}

log.transports.file.file = logsPath + '/application.log'
log.variables.appversion = app.getVersion();
log.transports.file.format = '[{h}:{i}:{s}] [{appversion}] [{level}] {text}'
log.transports.file.maxSize = 10485760 // 10 MiB

function setArgs(): void {
  try {
    launchOptions = commandLineArgs(optionDefinitions, {'argv': process.argv})
    log.info('Starting with options: ', launchOptions, ' on :', process.argv)
    if (launchOptions["ignore-certificate-errors"]) {
      app.commandLine.appendSwitch('ignore-certificate-errors', 'true')
    }
  }
  catch (err) {
    log.error('Unable to parse arguments, ignoring all.')
    log.error('Details: ', err)
  }

  if (typeof(process.env.CUSTOM_USER_DATA) !== "undefined") {
    app.setPath('userData', process.env.CUSTOM_USER_DATA)
  }
}

function getApplicationUrl(view: string): string {
  return url.format({
    pathname: path.join(__dirname, '/../index.html'),
    protocol: 'file:',
    slashes: true,
    hash: view
  })
}

function initApplicationConfigFile(): void {
  const iniFileAsync = ConfigManager.getXivoIniFile()
  iniFileAsync
    .then((config: XiVO.Config) => {
      for (let setting in config) {
        let value = config[setting]
        if (typeof value == 'boolean' || value) {
          if (typeof value == 'string') {
            value = value.toLowerCase()
          }
          localStorage.setItem(setting, value)
        }
      }
    }).catch((err: Error) => {
      log.error(err)
    })
}

// Keep a global reference of the window object, if you don't, the window will
// be closed automatically when the JavaScript object is garbage collected.
let win: Electron.BrowserWindow
let tray: Electron.Tray
let positionMgt: any
let resolutionAdapter: any
let eventsListener: any

function setTrayIcon(icon: string): void {
  let trayImage = path.join(__dirname, '/../img/xivo_headset_'+icon)
  trayImage += (os.platform() === 'win32') ? '.ico' : '.png'
  if (tray) {
    tray.setImage(nativeImage.createFromPath(trayImage))
  } else {
    tray = new Tray(nativeImage.createFromPath(trayImage))
  }
}

function createWindow(): void {

  const firstInstance = app.requestSingleInstanceLock()
  if (!firstInstance) {
    log.info("Closing app: app is already running.")
    globalShortcut.unregisterAll()
    app.quit()
  } else {

    i18n.setLocale(app.getLocale())
    initApplicationConfigFile()

    // Restore size and position from previous session
    // or get default size for first time launch
    let mainWindowState = windowStateKeeper({
      defaultWidth: ConfigManager.ELECTRON_DEFAULT_WIDTH,
      defaultHeight: ConfigManager.ELECTRON_DEFAULT_HEIGHT,
      path: app.getPath('userData'),
      maximize: false,
      fullScreen: false
    })

    // Create a default browser window.
    win = new BrowserWindow({
      'x': mainWindowState.x,
      'y': mainWindowState.y,
      'width': mainWindowState.width,
      'height': mainWindowState.height,
      frame: false,
      backgroundColor: '#eeeeee',
      show: false,
      webPreferences: {
        webviewTag: true,
        nodeIntegration: true
      }
    })

    // Listen to window resizing event and save it in user personal folder
    mainWindowState.manage(win)
    const templateMenu = new ApplicationMenu(win, closingParameters)

    setTrayIcon('default')
    tray.setToolTip(i18n.__('APP_NAME'))
    tray.setContextMenu(templateMenu.contextMenu)

    // and load the index.html of the app.
    win.loadURL(getApplicationUrl('/'))

    let appMenu = null
    if (isDev || launchOptions.debug) {
      appMenu = Menu.buildFromTemplate(_.concat(templateMenu.defaultMenu, templateMenu.debugMenu, templateMenu.aboutMenu))
    } else {
      appMenu = Menu.buildFromTemplate(_.concat(templateMenu.defaultMenu, templateMenu.aboutMenu))
    }

    win.setResizable(false)
    win.setMaximizable(false)

    positionMgt = new PositionMgt(win)
    resolutionAdapter = new ResolutionAdapter(win)
    resolutionAdapter.setDefaultAppSize(ConfigManager.ELECTRON_DEFAULT_HEIGHT, ConfigManager.ELECTRON_DEFAULT_WIDTH)
    eventsListener = new EventsListener(win, resolutionAdapter, launchOptions, positionMgt, appMenu, tray, closingParameters)
  }
}

// register protocols
if(process.platform === 'win32') {
  (async () => {
    await Registry.set('HKCU\\Software\\'+appName+'\\Capabilities', 'ApplicationName', appName)
    await Registry.set('HKCU\\Software\\'+appName+'\\Capabilities', 'ApplicationDescription', appName)

    for (let p of acceptedProtocols) {
      await Registry.set('HKCU\\Software\\'+appName+'\\Capabilities\\URLAssociations', p, appName+'.'+p)
      await Registry.set('HKCU\\Software\\Classes\\'+appName+'.'+p+'\\DefaultIcon', '', process.execPath)
      await Registry.set('HKCU\\Software\\Classes\\'+appName+'.'+p+'\\shell\\open\\command', '', `"${process.execPath}" "%1"`)
    }

    await Registry.set('HKCU\\Software\\RegisteredApplications', appName, 'Software\\'+appName+'\\Capabilities')
  })()
} else {
  acceptedProtocols.forEach((p) => {
    app.setAsDefaultProtocolClient(p)
  })
}

// fix tray win32 notifications
app.setAppUserModelId('com.squirrel.' + appName + '.' + appName)

// Set electron arguments according to the cli args
setArgs()

// Disable scale factor related mathematics mechanics
app.commandLine.appendSwitch('force-device-scale-factor', '1')

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.on('ready', createWindow)

autoUpdater.addListener("update-available", function () {
  log.info("update-available")
})

autoUpdater.addListener("update-downloaded", function () {
  log.info("update-downloaded")
  resolutionAdapter.resize()
  win.webContents.send('sref', 'main.updateavailable')
})

autoUpdater.addListener('error', function (error: Error) {
  log.error("update-error: " + error)
})

autoUpdater.addListener("checking-for-update", function () {
  log.info("update-checking")
})

autoUpdater.addListener("update-not-available", function () {
  log.info("update-not-available")
})