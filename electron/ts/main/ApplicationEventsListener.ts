export { }
const { ipcMain, autoUpdater, clipboard, app, Tray, nativeImage, shell, globalShortcut } = require('electron')
const path = require('path')
const url = require('url')
const AutoLaunch = require('auto-launch')
const _ = require('lodash')
const os = require('os')
const log = require('electron-log')
import MenuTools from '../helpers/ApplicationMenuTools'
import ConfigManager from './ApplicationConfigManager'

export default class EventsListener extends MenuTools {

  private readonly fullscreenEvent: string = 'FULLSCREEN_EVENT'
  private readonly phoneEvent: string = 'PHONE_EVENT'
  private readonly acceptedProtocols: Array<string> = ["tel", "callto"]
  public webview: any
  private readonly protocolSep: string = ":"
  public launch: any = new AutoLaunch({
    name: app.getName()
  })

  constructor(
    public win: Electron.BrowserWindow,
    public resolutionAdapter: any,
    private readonly launchOptions: XiVO.Options,
    public positionMgt: any,
    public menu: any,
    public tray: Electron.Tray,
    public closeParams: XiVO.Options
  ) {

    super(win, closeParams)

    this.win.webContents.on('context-menu', (_, params: Electron.ContextMenuParams): void => {
      this.menu.popup(this.win, { 'x': params.x, 'y': params.y })
    })

    this.tray.on('click', (): void => {
      this.win.isVisible() ? this.win.hide() : this.win.show()
    })

    this.win.once('ready-to-show', (): void => {
      this.win.show()
    })

    // Quit when all windows are closed.
    app.on('window-all-closed', (): void => {
      this.closeApp()
    })

    app.on('web-contents-created', (_, contents: Electron.webContents): void => {
      if (contents.getType() === 'webview') {
        this.webview = contents
        this.webview.on('new-window', function (e: Event, url: string) {
          shell.openExternal(url)
          e.preventDefault()
        })
      }
    })

    app.on('second-instance', (_, commandLine: Array<string>): void => {
      this.setFocus(this.win)
      if (commandLine.length > 1) {
        log.info('received URL to open', commandLine)
        this.forwardMessage(this.win, this.extractMessageFromURL([...commandLine].pop()), this.phoneEvent)
      }
    })

    ipcMain.on('setUpdateUrl', (_, url: string, reload: boolean): void => {
      let CustomFeedURLOptions: Electron.FeedURLOptions = { "url": url + '/updates/win' + (os.arch() === 'x64' ? '64' : '32')}
      log.info("setUpdateUrl with reload", CustomFeedURLOptions.url, reload)
      autoUpdater.setFeedURL(CustomFeedURLOptions)
      autoUpdater.checkForUpdates()
      this.sendTokenToRenderer(this.launchOptions.token)
      if (reload) {
        this.win.loadURL(this.getApplicationUrl('/'))
      }
    })

    ipcMain.on('urlError', (_, arg: any): void => {
      log.error("Fail to load URL", arg)
      this.resolutionAdapter.setDefaultAppSize(ConfigManager.ELECTRON_DEFAULT_HEIGHT, ConfigManager.ELECTRON_DEFAULT_WIDTH)
      this.win.loadURL(this.getApplicationUrl('/loadingerror'))
      this.setTrayIcon('logout')
    })

    ipcMain.on('setUpdateGlobalShortcut', (_, arg: any): void => {
      globalShortcut.unregisterAll()
      if (arg != null) {
        this.setGlobalShortcut(this.win, arg)
      }
    })

    ipcMain.on('setUpdateCloseTray', (_, arg: any): void => {
      closeParams.closeInTray = arg
    })

    ipcMain.on('setUpdateStartUp', (_, arg: any): void => {
      this.launch.isEnabled()
        .then((isEnabled: boolean): void => {
          if (arg !== isEnabled) {
            (arg) ? this.launch.enable() : this.launch.disable()
          }
        })
        .catch(function (err: Error): void {
          log.error(err)
        })
    })

    ipcMain.on('updateBrowserWindowSize', (_, arg: any): void => {
      if (arg.h) {
        arg.h += ConfigManager.ELECTRON_TITLE_BAR_HEIGHT
      }
      this.resolutionAdapter.setDefaultAppSize(arg.h, arg.w)
      let align = this.positionMgt.getAlign()
      this.positionMgt.setAlign(align)
      this.positionMgt.repositioning()

      this.win.setMinimizable(!arg.mini)
      this.win.setAlwaysOnTop(arg.mini)
    })

    ipcMain.on('i18n', (event: Electron.IpcMainEvent): void => {
      event.returnValue = app.getLocale()
    })

    
    ipcMain.on('setConfirmQuit', (_, arg: any): void => {
      closeParams.confirm = !arg.reset ? arg : null
    })

    ipcMain.on('forceQuit', (): void => {
      this.forceQuit()
    })

    ipcMain.on('close', (event: Electron.Event): void => {
      this.closeApp(event)
    })

    ipcMain.on('minimize', (): void => {
      this.win.minimize()
    })

    ipcMain.on('config', (): void => {
      this.win.webContents.send('sref', 'main.settings')
    })

    ipcMain.on('focusBrowserWindow', (): void => {
      this.setFocus(this.win)
    })

    ipcMain.on('trayIcon', (_, arg: any): void => {
      this.setTrayIcon(arg)
    })

    ipcMain.on('runExecutable', (_, path: string, args: any): void => {
      this.runExecutable(path, args)
    })

    ipcMain.on('allowFullscreen', (): void => {
      this.allowFullscreen(this.win)
    })

    ipcMain.on('disableFullscreen', (): void => {
      this.disableFullscreen(this.win)
    })

    ipcMain.on('quitAndInstall', (): void => {
      closeParams.quit = true
      autoUpdater.quitAndInstall()
    })

  }

  getApplicationUrl(view: string): string {
    return url.format({
      pathname: path.join(__dirname, '/../../index.html'),
      protocol: 'file:',
      slashes: true,
      hash: view
    })
  }

  extractMessageFromURL(data: string | undefined): string {
    if (_.find(this.acceptedProtocols, (p: string): string => {
      return _.startsWith(data, p + this.protocolSep)
    })){
      return _.split(data, this.protocolSep, 2)[1]
    } else return ""
  }

  sendTokenToRenderer(token: string | boolean | undefined): void {
    if (token) {
      this.win.webContents.send('token', token)
    }
  }

  setTrayIcon(icon: string): void {
    let trayImage = path.join(__dirname, '/../../img/xivo_headset_' + icon)
    trayImage += (os.platform() === 'win32') ? '.ico' : '.png'
    if (this.tray) {
      this.tray.setImage(nativeImage.createFromPath(trayImage))
    } else {
      this.tray = new Tray(nativeImage.createFromPath(trayImage))
    }
  }

  setGlobalShortcut(browserWindow: Electron.BrowserWindow, keyCombination: Electron.Accelerator): void {
    globalShortcut.register(keyCombination, () => {
      let msg = clipboard.readText('selection')
      if (!msg) msg = 'none'
      log.info('received global key with content', msg)
      this.forwardMessage(browserWindow, msg, this.phoneEvent)
    })
  }

  allowFullscreen(browserWindow: Electron.BrowserWindow): void {
    if (browserWindow) {
      browserWindow.setResizable(true)
      browserWindow.setMaximizable(true)
      browserWindow.setFullScreenable(true)
      this.forwardMessage(browserWindow, 'fullscreen', this.fullscreenEvent)
    }
  }

  disableFullscreen(browserWindow: Electron.BrowserWindow): void {
    if (browserWindow) {
      browserWindow.setResizable(false)
      browserWindow.setMaximizable(false)
      browserWindow.setFullScreenable(false)
    }
  }

  runExecutable(path: string, args: any): void {
    log.info('Running executable ', path, ' with args ', args)
    const process = require('child_process')
    process.spawn(path, args || [])
  }

}