export { }
const globalAny: any = global
const { ipcRenderer } = require('electron')


globalAny.setElectronConfig = (data: any): void => {

  if (data.size) {
    ipcRenderer.send('updateBrowserWindowSize', data.size)
  }
  if (data.focus) {
    ipcRenderer.send('focusBrowserWindow')
  }
  if (data.confirmQuit) {
    ipcRenderer.send('setConfirmQuit', data.confirmQuit)
  }
  if (data.forceQuit) {
    ipcRenderer.send('forceQuit')
  }
  if (data.trayIcon) {
    ipcRenderer.send('trayIcon', data.trayIcon)
  }
  if (data.runExecutable) {
    ipcRenderer.send('runExecutable', data.runExecutable, data.executableArgs)
  }
  if (data.allowFullscreen) {
    ipcRenderer.send('allowFullscreen')
  }
  if (data.disableFullscreen) {
    ipcRenderer.send('disableFullscreen')
  }
}