export { }
const i18n = require('../helpers/translate')
const path = require('path')
const { Menu } = require('electron')
const openAboutWindow = require('about-window').default
import MenuTools from '../helpers/ApplicationMenuTools'

export default class ApplicationMenu extends MenuTools {

  public debugMenu: Array<object>
  public defaultMenu: Array<object>
  public aboutMenu: Array<object>
  public contextMenu: Electron.Menu

  constructor(public win: Electron.BrowserWindow, closeParams: XiVO.Options) {

    super(win, closeParams)
    var self = this

    this.debugMenu = [
      {
        label: i18n.__('MENU_DEBUG'),
        submenu: [
          {
            label: i18n.__('MENU_DEBUG_RELOAD'),
            accelerator: 'CmdOrCtrl+R',
            click() {
              self.win.reload()
            }
          },
          {
            label: i18n.__('MENU_DEBUG_DEVTOOL'),
            click() {
              win.webContents.toggleDevTools()
            }
          },
          {
            label: i18n.__('MENU_DEBUG_DEVTOOL_WEBVIEW'),
            click() {
              if (win.webContents) {
                win.webContents.toggleDevTools()
              }
            }
          }
        ]
      }
    ]

    this.defaultMenu = [
      {
        label: i18n.__('MENU_NAVIGATION'),
        submenu: [
          {
            label: i18n.__('MENU_NAVIGATION_APPLICATION'),
            click() {
              win.webContents.send('sref', 'main')
            }
          },
          {
            label: i18n.__('MENU_NAVIGATION_SETTINGS'),
            click() {
              win.webContents.send('sref', 'main.settings')
            }
          },
          {
            type: 'separator'
          },
          {
            label: i18n.__('MENU_NAVIGATION_QUIT'),
            click() {
              self.closeApp()
            }
          }
        ]
      }
    ]

    this.aboutMenu = [{
      label: i18n.__('MENU_ABOUT'),
      submenu: [{
        label: i18n.__('MENU_NAVIGATION_ABOUT'),
        click() {
          openAboutWindow({
            icon_path: path.join(__dirname, '/../../img/xivo_headset_default.png'),
            copyright: i18n.__('LICENSING'),
            win_options: {
              autoHideMenuBar: true,
              webPreferences: {
                nodeIntegration: true,
                webSecurity: false,
                sandbox: false
              }
            }
          })
        }
      }]
    }]

    this.contextMenu = Menu.buildFromTemplate([
      {
        label: i18n.__('MENU_NAVIGATION_SHOW'),
        click() {
          self.setFocus(win)
        }
      },
      {
        label: i18n.__('MENU_NAVIGATION_SETTINGS'),
        click() {
          win.webContents.send('sref', 'main.settings')
          self.setFocus(win)
        }
      },
      {
        type: 'separator'
      },
      {
        label: i18n.__('MENU_NAVIGATION_QUIT'),
        click() {
          self.closeApp()
        }
      }])

  }
};