export { }
const os = require('os')
const { app } = require('electron')
const configFolderPath: string = app.getPath('appData') + '/xivo-desktop-assistant/application/config'
const configFilePath: string = configFolderPath + '/xivoconfig.ini'
const iniParam: Array<string> = ['APP_PROTOCOL', 'APP_DOMAIN', 'APP_INTERFACE', 'APP_STARTUP', 'APP_CLOSE']
const iniComment: Array<string> = ['Defines if the connection is secured or not : HTTPS or HTTP', 'Defines the server used by the application : IP or URL', 'Defines the interface to open by default : UCASSISTANT or CCAGENT', 'Defines if the UC assistant opens on operating system startup : true or false', 'Defines if the UC assistant will be minimized in the taskbar instead of closed : true or false']
const fs = require('fs')
const ini = require('ini')
const log = require('electron-log')

import Migration from '../helpers/ApplicationMigration'

export default class ConfigManager {

  public static readonly ELECTRON_DEFAULT_WIDTH: number = 362
  public static readonly ELECTRON_DEFAULT_HEIGHT: number = 824
  public static readonly ELECTRON_TITLE_BAR_HEIGHT: number = 24

  public static getXivoIniFile(): Promise<any> {

  let createDefaultConfigFile = (path: string, resolve: (value?: any) => void, reject: (err: Error) => void) => {
    fs.appendFile(path, iniParam.map((e, i) => ';' + iniComment[i] + '\n' + e + "=").join('\n\n'), function (err: Error) {
      return err ? reject(err) : resolve()
    })
  }

  return new Promise((resolveReturn, rejectReturn): void => {

    let folderIsReady = () => {
      return new Promise((resolve: (value?: any) => void): void => {
        if (!fs.existsSync(configFolderPath)) {
          fs.mkdirSync(configFolderPath, { recursive: true })
        }
        resolve()
      }).catch((err: Error): Error => {
        log.error(err)
        return err
      })
    }

    let configIsReady = () => {
      return new Promise((resolve, reject): void => {
        if (!fs.existsSync(configFilePath)) {
          createDefaultConfigFile(configFilePath, resolve, reject)
        } else {
          resolve()
        }
      }).catch((err: Error) => {
        log.error(err)
        return err
      })
    }

    folderIsReady().then(() => {
      Migration.migrate().then(() => {
        configIsReady().then(() => {
          fs.readFile(configFilePath, function (err: Error, data: JSON) {
            if (err) rejectReturn('Ini file cannot be read (' + err + ')')
            log.info('Ini file has been succesfully loaded')
            resolveReturn(ini.parse(data.toString()))
          })
        })
      })
    })
  })
  }
}