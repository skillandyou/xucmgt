export { }
const electron = require('electron')
let defaultAppHeight: number
let defaultAppWidth: number
let newAppHeight: number
let ratio: number
let zoom: number

export class ResolutionAdapter {

  constructor(public win: Electron.BrowserWindow) {

    //This binds resize on the move event
    this.win.on('move', this.resize.bind(this))
  }

  //This function will calculate the zoom we must use to scale the webview
  calculateZoom = (defaultAppWidth: number, newWidth: number): number => {
    return parseFloat((defaultAppWidth / newWidth).toFixed(2))
  }

  //This function will calculate the ratio based on the width and height of the window
  calculateRatio = (appWidth: number, appHeight: number): number => {
    return parseFloat((appWidth / appHeight).toFixed(4))
  }

  //This function adapt the width of the app compared to it's new height
  //to keep the aspect ratio
  scaleWidth = (newAppHeight: number, ratio: number): number => {
    let newAppWidth
    newAppWidth = Math.round(newAppHeight * ratio)

    return newAppWidth
  }

  //This function is the entry point to scale electron based on DPI / screen resolution
  resize(): void {
    if (!this.win.isMinimized()) {
      let size = this.calculateSize()
      this.applySize(size)
    }
  }
 
  //This function calculate the size of the app
  calculateSize(): { height: number, width: number} {

    //retrieve current size and position of the app window
    let appWindow = this.win.getBounds()

    //retrieve current screen where the app window is displayed
    let currentScreen = electron.screen.getDisplayMatching(appWindow)
    
    let newHeight = this.calculateHeight(appWindow, currentScreen)
    let newWidth = this.scaleWidth(newAppHeight, ratio)
    zoom = this.calculateZoom(defaultAppWidth, newWidth)
    this.sendZoomToWebview(zoom)
    return { height: newHeight, width: newWidth }
  }

  //This functions apply the custom size to electron window
  applySize(size: {width: number, height: number}): void {
    this.win.setContentSize(size.width, size.height)
  }

  //This function calculate the height of the app compared to the size of the screen
  calculateHeight(appSize: { width: number, height: number }, currentScreen: Electron.Display): number {
    let customMargin
    customMargin = currentScreen.size.height / 4.2
    let heightDifference = appSize.height - currentScreen.size.height
    heightDifference += customMargin
    newAppHeight = Math.round(appSize.height - heightDifference)
    return newAppHeight
  }

  //This function sends the zoom factor to the webview to scale it
  sendZoomToWebview(zoom: number): void {
    this.win.webContents.on('did-finish-load', () => {
      this.win.webContents.send('HiDPI', zoom)
    })
    this.win.webContents.send('HiDPI', zoom)
  }

  //This function set the default resolution of the app on interface change
  setDefaultAppSize(appHeight: number, appWidth: number): void {
    ratio = this.calculateRatio(appWidth, appHeight)
    defaultAppHeight = appHeight
    defaultAppWidth = appWidth
    this.applySize({ width: defaultAppWidth, height: defaultAppHeight })

    this.resize()
  }
  
}