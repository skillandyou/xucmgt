export { }
module.exports = class KeyBindings {

  public readonly COMMON_KEYS: Array<string> = [
    '0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
    'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z',
    'F1', 'F2', 'F3', 'F4', 'F5', 'F6', 'F7', 'F8', 'F9', 'F10', 'F11', 'F12',
    '!', '$', '%', '^', '&', '*', '(', ')', ':', '<', '_', '>', '?', '{', '|', '}', '"', ';', '=', ',', '-', '.', '/', '~', '@', '#',
  ]

  public readonly LOCALIZED_KEYS: Array<string> = [
    'Space', 'Tab', 'Backspace', 'Delete', 'Insert', 'Enter', 'Up', 'Down', 'Left', 'Right', 'Home', 'End', 'PageUp', 'PageDown', 'Esc'
  ]

  public readonly LOCALIZED_MODIFIER_KEYS: Array<string> = [
    'Cmd', 'Ctrl', 'CmdOrCtrl', 'Alt', 'Option', 'AltGr', 'Shift', 'Super'
  ]

}