export { }
export default class Message {

  constructor(private readonly type: string, private readonly value: string) {}

  public toJSON(): { type: string, value: string } {
    return {
      type: this.type,
      value: this.value
    }
  }
}
