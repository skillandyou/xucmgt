import sbt._

class CustomLogger extends  ProcessLogger {
    val buf = new StringBuffer
    val cl = ConsoleLogger()
    def buffer[T](f: ⇒ T): T = f
    def error(s: ⇒ String): Unit = {
      buf.append(s).append('\n')
      cl.error(s)
    }
    def info(s: ⇒ String): Unit = {
      buf.append(s).append('\n')
      cl.info(s)
    }
  }
