import sbt._
import play.sbt.Play.autoImport._

object Version {
  val htmlcompressor = "1.5.2"
  val scalatestplay = "3.1.2"
}


object Library {
  val htmlcompressor  = "com.googlecode.htmlcompressor"  % "htmlcompressor"       % Version.htmlcompressor
  val playjson        = "com.typesafe.play"             %% "play-json"            % "2.6.10"
  val scalatestplay   = "org.scalatestplus.play"        %% "scalatestplus-play"   % Version.scalatestplay
}

object Dependencies {

  import Library._

  val scalaVersion = "2.12.7"

  val resolutionRepos = Seq(
    "Local Maven Repository" at "file:///" + Path.userHome.absolutePath + "/.m2/repository",
    "theatr.us" at "http://repo.theatr.us",
    "Typesafe repository" at "http://repo.typesafe.com/typesafe/releases/"
  )

  val runDep: Seq[ModuleID] = run(
    htmlcompressor,
    playjson,
    guice,
    ehcache
  )

  val testDep: Seq[ModuleID] = test(
    scalatestplay
  )

  def run       (deps: ModuleID*): Seq[ModuleID] = deps
  def test      (deps: ModuleID*): Seq[ModuleID] = deps map (_ % "test")

}
