#!/usr/bin/env bash
set -ex

clean_electron_build_leftovers() {
    local win_dest_dir="${1}"; shift
    local lin_dest_dir="${1}"; shift
    local docker_volume_name="${1}"; shift

    rm -rf "${win_dest_dir}"
    rm -rf "${lin_dest_dir}"
    rm -rf electron/dist
    rm -rf electron/build/application

    docker volume rm -f "${docker_volume_name}"
}

build_electron_application() {
    local electron_builder_image="${1}"; shift
    local electron_builder_image_tag="${1}"; shift
    local docker_volume_name="${1}"; shift

    # Building Electron Application
    docker run --rm -i \
        --env ELECTRON_CACHE="/root/.cache/electron" \
        --env ELECTRON_BUILDER_CACHE="/root/.cache/electron-builder" \
        -v "${PWD}/electron":/project \
        -v "${docker_volume_name}":/project/node_modules \
        -v ~/.cache/electron:/root/.cache/electron \
        -v ~/.cache/electron-builder:/root/.cache/electron-builder \
        -e TARGET_VERSION_SEMVER="${TARGET_VERSION_SEMVER}" \
        "${electron_builder_image}":"${electron_builder_image_tag}" \
        "/project/build-electron-app.sh"
}

sign_windows_exe() {
    local win_build_dir="${1}"; shift

    local win_sign_image="bbassett/osslsigncode"
    local win_sign_image_tag="1.7.1"

    local dapp_cert_path="${HOME}/.certs"
    local dapp_cert_name="avencall.pfx"
    local dapp_cert_secret
    dapp_cert_secret=$(cat ~/.certs/secret)

    local dapp_exe_filepath
    dapp_exe_filepath=$(ls -1 "${win_build_dir}"/xivo-desktop-assistant*.exe)
    local dapp_exe_filename="${dapp_exe_filepath##*/}" # extract filename from path

    # Codesign windows electron .exe file
    if [ -f  "${dapp_cert_path}/${dapp_cert_name}" ]; then
        # Sign exe file
        docker run --rm \
            -v "${PWD}/${win_build_dir}":/src \
            -v "${dapp_cert_path}":/certs \
            "${win_sign_image}":"${win_sign_image_tag}" \
            sign -pkcs12 "/certs/${dapp_cert_name}" -pass "${dapp_cert_secret}" \
            -n "XiVO Desktop Assistant" -i https://xivo.solutions/ \
            -in "/src/${dapp_exe_filename}" -out "/src/${dapp_exe_filename}-signed"
    else
        echo "Signing certificate not found."
        echo "Aborting build."
        exit 1
    fi

    if [ -f "${dapp_exe_filepath}-signed" ]; then
        mv "${dapp_exe_filepath}-signed" "${dapp_exe_filepath}"
    else
        echo "Signed executable was not found. Check that certificate is there or still valid."
        echo "Aborting build."
        exit 1
    fi
}

publish_windows_exe() {
    local win_build_dir="${1}"; shift
    local win_dest_dir="${1}"; shift

    mkdir -p "${win_dest_dir}"

    # Copy Win Exe from build dir to final dir
    cp -a "${win_build_dir}"/* "${win_dest_dir}"
}

publish_linux_pkg() {
    local lin_build_dir="${1}"; shift
    local lin_dest_dir="${1}"; shift

    # Prepare Debian repo structure
    mkdir -p "${lin_dest_dir}"/{conf,incoming}

    cat > "${lin_dest_dir}"/conf/distributions << EOF
Origin: Xivo.solutions
Label: Xivo.solutions
Suite: stable
Codename: jessie
Architectures: amd64 i386
Components: contrib
Description: XivoCC local repo
EOF

    cp -a "${lin_build_dir}"/*.deb "${lin_dest_dir}"/incoming
    pushd "${lin_dest_dir}"
    reprepro -Vb . includedeb jessie incoming/*.deb
    popd

    # Clean incoming dir (for space usage sake in docker image)
    rm "${lin_dest_dir}"/incoming/*.deb
}


main() {
    # Image and Tag for electron-builder
    ELECTRON_BUILDER_IMAGE="xivoxc/electron-builder"
    ELECTRON_BUILDER_IMAGE_TAG="2019.12.latest"
    # Docker volume name
    DOCKER_VOLUME_NAME="xucmgt-${TARGET_VERSION_SEMVER}-node-modules"
    # Build dirs
    WIN_BUILD_DIR="electron/dist/squirrel-windows"
    LIN_BUILD_DIR="electron/dist"
    # Shipping dirs
    WIN_DEST_DIR="updates/win64"
    LIN_DEST_DIR="updates/debian"

    clean_electron_build_leftovers "${WIN_DEST_DIR}" "${LIN_DEST_DIR}" "${DOCKER_VOLUME_NAME}"

    build_electron_application "${ELECTRON_BUILDER_IMAGE}" "${ELECTRON_BUILDER_IMAGE_TAG}" "${DOCKER_VOLUME_NAME}"

    # Sign Windows Desktop Appl
    sign_windows_exe "${WIN_BUILD_DIR}"

    # Publish Windows Desktop App to final destination
    publish_windows_exe "${WIN_BUILD_DIR}" "${WIN_DEST_DIR}"

    # Publish Linux Desktop App to final destination
    publish_linux_pkg "${LIN_BUILD_DIR}" "${LIN_DEST_DIR}"

    # And ... finally build the xucmgt docker image !
    sbt clean test docker:publishLocal
}

main "${@}"
