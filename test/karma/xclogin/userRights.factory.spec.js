'use strict';

describe('Service: User rights', function () {

  var userRights;
  var xucLink;
  var $q;
  var $rootScope;


  beforeEach(angular.mock.module('html-templates'));
  beforeEach(angular.mock.module('karma-backend'));
  beforeEach(angular.mock.module('xcCti'));
  beforeEach(angular.mock.module('xcLogin'));

  beforeEach(angular.mock.inject(function(_userRights_, _XucLink_, _$q_, _$rootScope_) {
    userRights = _userRights_;
    xucLink = _XucLink_;
    $q = _$q_;
    $rootScope = _$rootScope_;
  }));

  it('checks right to ccManager', function() {
    expect(userRights.allowCCManager('Admin')).toEqual(true);
    expect(userRights.allowCCManager('Supervisor')).toEqual(true);
    expect(userRights.allowCCManager('NoRightForUser')).toEqual(false);
  });

  it ('disables the agent rights for dissuasion when the profile is teacher', function(done) {    
    xucLink.getProfileRightAsync = jasmine.createSpy().and.callFake(() => {
      let result = $q.defer();
      result.resolve({
        profile: "Teacher",
        rights: []
      });
      return result.promise;
    });

    $rootScope.$digest();
    userRights.getDissuasionRight().then(function(right) {
      expect(right).toBeFalsy();  
      done();
    });
  });

  it ('verifies if the agent has the rights for dissuasion when the profile is supervisor', function(done) {
    xucLink.getProfileRightAsync = jasmine.createSpy().and.callFake(() => {
      let result = $q.defer();
      result.resolve({
        profile: "Supervisor",
        rights: ["recording", "dissuasion"]
      });
      return result.promise;
    });
    
    $rootScope.$digest();
    userRights.getDissuasionRight().then(function(right) {
      expect(right).toBeTruthy();  
      done();
    });
  });

  it ('verifies if the agent DOESN\'T HAVE dissuasion rights when the profile is supervisor', function(done) {
    xucLink.getProfileRightAsync = jasmine.createSpy().and.callFake(() => {
      let result = $q.defer();
      result.resolve({
        profile: "Supervisor",
        rights: ["recording"]
      });
      return result.promise;
    });
    
    $rootScope.$digest();
    userRights.getDissuasionRight().then(function(right) {
      expect(right).toBeFalsy();  
      done();
    });
  });

  it ('accords to the agent the rights for dissuasion when the profile is administrator', function(done) {
    xucLink.getProfileRightAsync = jasmine.createSpy().and.callFake(() => {
      let result = $q.defer();
      result.resolve({
        profile: "Admin",
        rights: ["recording"]
      });
      return result.promise;
    });

    $rootScope.$digest();
    userRights.getDissuasionRight().then(function(right) {
      expect(right).toBeTruthy();  
      done();
    });
  });
});