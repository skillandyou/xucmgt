
describe('loginForm directive', function() {
  var XucLink = {
    login: jasmine.createSpy('login'),
    loginWithCredentials: jasmine.createSpy('loginWithCredentials'),
    loginWithStoredCredentials: jasmine.createSpy('loginWithStoredCredentials'),
    getStoredCredentials : jasmine.createSpy('getStoredCredentials'),
    getCasCredentials: jasmine.createSpy('getCasCredentials'),
    logoutFromCas: jasmine.createSpy('logoutFromCas'),
    setHostAndPort : jasmine.createSpy('setHostAndPort'),
    setRedirectToHomeUrl: jasmine.createSpy('setRedirectToHomeUrl'),
    parseUrlParameters: jasmine.createSpy('setRedirectToHomeUrl')
  };

  var locationValue = {
    origin: 'test',
    protocol: 'http:',
    href: 'http://myxucmgt/',
    search: ''
  };
  var windowProvider = {
    $get: function() {
      return {
        location: locationValue,
        navigator: {languages: 'en_EN'}};
    }
  };

  var defaultAutoLoginTimeout = 5;
  var maxAutoLoginTimeout = 60;

  beforeEach(angular.mock.module('karma-backend'));

  beforeEach(function() {
    jasmine.addMatchers({
      toEqualData : customMatchers.toEqualData
    });

    angular.mock.module(function($provide) {
      $provide.provider('$window', windowProvider);
    });
   
    angular.mock.module('xcLogin', function ($translateProvider, $provide) {
      $translateProvider.preferredLanguage('en');
    });

    angular.mock.module('xcCti', function ($provide, _$compileProvider_) {
      $provide.decorator("XucLink", function() {return XucLink;});
      _$compileProvider_.debugInfoEnabled(true);
    });
  });
  
  it('should check stored credentials and attempt autologin', angular.mock.inject(function($rootScope, $compile, $httpBackend, $q) {
    var scope = $rootScope.$new();
    scope.hostAndPort = "xuc:9876";
    scope.onLogin = jasmine.createSpy("onLogin");
    
    var credentials = {login: "jbond", token: "aaaa-bbbb-cccc-dddd-1234", phoneNumber: "1001"};
    XucLink.getStoredCredentials.and.returnValue(credentials);
    XucLink.loginWithStoredCredentials.and.returnValue($q.when(credentials));
    
    var loginForm = $compile('<login-form on-login="onLogin()" host-and-port="hostAndPort" />')(scope);
    $httpBackend.expectGET("assets/javascripts/xclogin/directives/loginForm.html").respond("<div>my template</div>");
    
    $rootScope.$digest();
    $httpBackend.flush();
    
    expect(loginForm).toBeDefined();
    expect(loginForm.controller).toBeDefined();
    expect(XucLink.getStoredCredentials).toHaveBeenCalled();
    expect(XucLink.loginWithStoredCredentials).toHaveBeenCalled();

  }));

  it('should attempt autologin with CAS when configured to', angular.mock.inject(function($rootScope, $compile, $httpBackend, $q) {
    var scope = $rootScope.$new();
    scope.hostAndPort = "xuc:9876";
    scope.casServerUrl = "http://cas.org/cas";
    scope.onLogin = jasmine.createSpy("onLogin");
    XucLink.getStoredCredentials.and.returnValue(null);
    var credentials = {login: "jbond", token: "aaaa-bbbb-cccc-dddd-1234", phoneNumber: "1001"};
    XucLink.parseUrlParameters.and.returnValue({});
    XucLink.getCasCredentials.and.returnValue($q.when(credentials));
    
    var loginForm = $compile('<login-form on-login="onLogin()" host-and-port="hostAndPort" cas-server-url="casServerUrl"/>')(scope);

    $httpBackend.expectGET("assets/javascripts/xclogin/directives/loginForm.html").respond("<div>my template</div>");
    
    $rootScope.$digest();
    $httpBackend.flush();
    
    expect(loginForm).toBeDefined();
    expect(loginForm.controller).toBeDefined();
    expect(XucLink.getCasCredentials).toHaveBeenCalledWith('http://cas.org/cas', '');
    
  }));

  it('should forward last error to CAS server', angular.mock.inject(function($rootScope, $compile, $httpBackend, $q) {
    var scope = $rootScope.$new();
    scope.hostAndPort = "xuc:9876";
    scope.casServerUrl = "http://cas.org/cas";
    scope.customError = 'Logout';  
    scope.onLogin = jasmine.createSpy("onLogin");
    XucLink.getStoredCredentials.and.returnValue(null);
    var credentials = {login: "jbond", token: "aaaa-bbbb-cccc-dddd-1234", phoneNumber: "1001"};
    XucLink.parseUrlParameters.and.returnValue({});
    XucLink.getCasCredentials.and.returnValue($q.when(credentials));
    
    var loginForm = $compile('<login-form on-login="onLogin()" host-and-port="hostAndPort" error-code="customError" cas-server-url="casServerUrl"/>')(scope);

    $httpBackend.expectGET("assets/javascripts/xclogin/directives/loginForm.html").respond("<div>my template</div>");
    
    $rootScope.$digest();
    $httpBackend.flush();
    
    expect(loginForm).toBeDefined();
    expect(loginForm.controller).toBeDefined();
    expect(XucLink.getCasCredentials).toHaveBeenCalledWith('http://cas.org/cas', '?lastError=Logout');
    
  }));

  it('should logout from CAS server when configured to', angular.mock.inject(function($rootScope, $compile, $httpBackend, $q) {
    var scope = $rootScope.$new();
    scope.hostAndPort = "xuc:9876";
    scope.casServerUrl = "http://cas.org/cas";
    scope.customError = 'Logout';
    scope.casLogoutEnable = true;
    scope.onLogin = jasmine.createSpy("onLogin");
    XucLink.getStoredCredentials.and.returnValue(null);
    var credentials = {login: "jbond", token: "aaaa-bbbb-cccc-dddd-1234", phoneNumber: "1001"};
    XucLink.parseUrlParameters.and.returnValue({lastError: 'Logout'});

    var loginForm = $compile('<login-form on-login="onLogin()" host-and-port="hostAndPort" error-code="customError" cas-server-url="casServerUrl" cas-logout-enable="casLogoutEnable"/>')(scope);

    $httpBackend.expectGET("assets/javascripts/xclogin/directives/loginForm.html").respond("<div>my template</div>");

    $rootScope.$digest();
    $httpBackend.flush();

    expect(loginForm).toBeDefined();
    expect(loginForm.controller).toBeDefined();
    expect(XucLink.logoutFromCas).toHaveBeenCalledWith('http://cas.org/cas');

  }));

  it('should fallback to manual logging if CAS failed', angular.mock.inject(function($rootScope, $compile, $httpBackend, $q) {
    var scope = $rootScope.$new();
    scope.hostAndPort = "xuc:9876";
    scope.casServerUrl = "http://cas.org/cas";
    scope.onLogin = jasmine.createSpy("onLogin");
    XucLink.getStoredCredentials.and.returnValue(null);
    var error = {"error":"UserNotFound","message":"User not found"};
    XucLink.parseUrlParameters.and.returnValue({});
    XucLink.getCasCredentials.and.callFake(function() { return $q.reject(error);});
    
    var loginForm = $compile('<login-form on-login="onLogin()" host-and-port="hostAndPort" cas-server-url="casServerUrl"/>')(scope);

    $httpBackend.expectGET("assets/javascripts/xclogin/directives/loginForm.html").respond("<div>my template</div>");
    
    $rootScope.$digest();
    $httpBackend.flush();
    
    expect(loginForm).toBeDefined();
    expect(loginForm.controller).toBeDefined();
    expect(XucLink.getCasCredentials).toHaveBeenCalledWith('http://cas.org/cas', '');
    $rootScope.$digest();
    var isolateScope = loginForm.isolateScope();
    expect(isolateScope.autoLogin).toEqual(false);
    expect(isolateScope.error).toEqual(error);
    expect(isolateScope.requireCredentials).toEqual(true);
  }));
  
  it('should callback onLogin when logging in', angular.mock.inject(function($rootScope, $compile, $httpBackend, $q) {
    var scope = $rootScope.$new();
    scope.hostAndPort = "xuc:9876";
    scope.onLogin = jasmine.createSpy("onLogin");
    
    var credentials = {login: "jbond", token: "aaaa-bbbb-cccc-dddd-1234", phoneNumber: "1001"};
    XucLink.getStoredCredentials.and.returnValue(credentials);
    XucLink.loginWithStoredCredentials.and.returnValue($q.when(credentials));
    
    var loginForm = $compile('<login-form on-login="onLogin()" host-and-port="hostAndPort" />')(scope);

    $httpBackend.expectGET("assets/javascripts/xclogin/directives/loginForm.html").respond("<div>my template</div>");
    
    $rootScope.$digest();
    $httpBackend.flush();

    $rootScope.$broadcast("ctiLoggedOn");
    expect(scope.onLogin).toHaveBeenCalled();

  }));

  it('should attempt to log again when no response from server', angular.mock.inject(function($rootScope, $compile, $httpBackend, $q, $timeout) {
    var scope = $rootScope.$new();
    var pass = 1;
    scope.hostAndPort = "xuc:9876";
    scope.onLogin = jasmine.createSpy("onLogin");
    
    var credentials = {login: "jbond", token: "aaaa-bbbb-cccc-dddd-1234", phoneNumber: "1001"};
    XucLink.getStoredCredentials.and.returnValue(credentials);
    XucLink.loginWithStoredCredentials.and.callFake(function() {
      if(pass === 1) {
        pass++;
        return $q.reject({error: 'NoResponse'});
      } else {
        return $q.when(credentials);
      }
    });
    
    var loginForm = $compile('<login-form on-login="onLogin()" host-and-port="hostAndPort"/>')(scope);

    $httpBackend.expectGET("assets/javascripts/xclogin/directives/loginForm.html").respond("<div>my template</div>");
    
    $rootScope.$digest();
    $httpBackend.flush();
    
    expect(loginForm).toBeDefined();
    expect(loginForm.controller).toBeDefined();
    expect(XucLink.getStoredCredentials).toHaveBeenCalled();
    expect(XucLink.loginWithStoredCredentials).toHaveBeenCalled();

    XucLink.loginWithStoredCredentials.calls.reset();

    // Force 'defaultAutoLoginTimeout' seconds expiration
    for(var i=0; i<defaultAutoLoginTimeout; i++) {
      $timeout.flush();
      $rootScope.$digest();
    }
    $timeout.verifyNoPendingTasks();
    expect(XucLink.loginWithStoredCredentials).toHaveBeenCalled();
  }));

  it('should attempt to log again doubling wait time with a max of ' + maxAutoLoginTimeout + ' secs', angular.mock.inject(function($rootScope, $compile, $httpBackend, $q, $timeout) {
    var scope = $rootScope.$new();
    var pass = 1;
    scope.hostAndPort = "xuc:9876";
    scope.onLogin = jasmine.createSpy("onLogin");
    
    var credentials = {login: "jbond", token: "aaaa-bbbb-cccc-dddd-1234", phoneNumber: "1001"};
    XucLink.getStoredCredentials.and.returnValue(credentials);
    XucLink.loginWithStoredCredentials.and.callFake(function() {
      return $q.reject({error: 'NoResponse'});
    });
    
    var loginForm = $compile('<login-form on-login="onLogin()" host-and-port="hostAndPort"/>')(scope);

    $httpBackend.expectGET("assets/javascripts/xclogin/directives/loginForm.html").respond("<div>my template</div>");
    
    $rootScope.$digest();
    $httpBackend.flush();
    
    expect(loginForm).toBeDefined();
    expect(loginForm.controller).toBeDefined();
    expect(XucLink.getStoredCredentials).toHaveBeenCalled();
    expect(XucLink.loginWithStoredCredentials).toHaveBeenCalled();

    var loginTimeout = defaultAutoLoginTimeout;
    while(loginTimeout < 60) {
      XucLink.loginWithStoredCredentials.calls.reset();
      // Force expiration
      for(var i=0; i<loginTimeout; i++) {
        $timeout.flush();
        $rootScope.$digest();
      }
      expect(XucLink.loginWithStoredCredentials).toHaveBeenCalled();
      loginTimeout = loginTimeout * 2;
      if(loginTimeout > maxAutoLoginTimeout) {
        loginTimeout = maxAutoLoginTimeout;
      }
    }

    
  }));

  it('should NOT attempt to log again when server answer with invalid session', angular.mock.inject(function($rootScope, $compile, $httpBackend, $q, $timeout) {
    var scope = $rootScope.$new();
    var pass = 1;
    scope.hostAndPort = "xuc:9876";
    scope.onLogin = jasmine.createSpy("onLogin");
    
    var credentials = {login: "jbond", token: "aaaa-bbbb-cccc-dddd-1234", phoneNumber: "1001"};
    XucLink.getStoredCredentials.and.returnValue(credentials);
    XucLink.loginWithStoredCredentials.and.callFake(function() {
      if(pass === 1) {
        pass++;
        return $q.reject({error: 'InvalidSession'});
      } else {
        return $q.when(credentials);
      }
    });
    
    var loginForm = $compile('<login-form on-login="onLogin()" host-and-port="hostAndPort"/>')(scope);

    $httpBackend.expectGET("assets/javascripts/xclogin/directives/loginForm.html").respond("<div>my template</div>");
    
    $rootScope.$digest();
    $httpBackend.flush();
    
    expect(loginForm).toBeDefined();
    expect(loginForm.controller).toBeDefined();
    expect(XucLink.getStoredCredentials).toHaveBeenCalled();
    expect(XucLink.loginWithStoredCredentials).toHaveBeenCalled();

    XucLink.loginWithStoredCredentials.calls.reset();

    $timeout.flush();

    $timeout.verifyNoPendingTasks();
    expect(XucLink.loginWithStoredCredentials).not.toHaveBeenCalled();
  }));


  it('should retrieve a token when I put one in the URL', angular.mock.inject(function ($rootScope, $compile, $httpBackend) {
    var scope = $rootScope.$new();

    var loginForm = $compile('<login-form on-login="onLogin()" host-and-port="hostAndPort" cas-server-url="casServerUrl"/>')(scope);
    $httpBackend.expectGET("assets/javascripts/xclogin/directives/loginForm.html").respond("<div>my template</div>");

    $rootScope.$digest();
    $httpBackend.flush();
    
    var isolateScope = loginForm.isolateScope();

    const link1 = 'http://www.whatsthisbug.com/category1?token=NTgwMENENDFF.2RDJDMkEyNzYyRk.Y3MA';
    let token1 = isolateScope.getToken(link1);
    expect(token1).toEqual('NTgwMENENDFF.2RDJDMkEyNzYyRk.Y3MA');
    
    const link2 = 'http://www.whatsthisbug.com/category1?token=NTgwM;E=NENDFF.2RDJDMkE=yNz:YyRk.Y3MA';
    let token2 = isolateScope.getToken(link2);
    expect(token2).toEqual('NTgwM;E=NENDFF.2RDJDMkE=yNz:YyRk.Y3MA');
    
    const link3 = 'http://www.whatsthisbug.com/category1?id=3&token=NTgwM;E=NENDFF.2RDJDMkE=yNz:YyRk.Y3MA';
    let token3 = isolateScope.getToken(link3);
    expect(token3).toEqual('NTgwM;E=NENDFF.2RDJDMkE=yNz:YyRk.Y3MA');

    const link4 = 'http://www.whatsthisbug.com/?token=NTgwM;E=NENDFF.2RDJDMkE=yNz:YyRk.Y3MA#!/ditto';
    let token4 = isolateScope.getToken(link4);
    expect(token4).toEqual('NTgwM;E=NENDFF.2RDJDMkE=yNz:YyRk.Y3MA');

    const link5 = '';
    let token5 = isolateScope.getToken(link5);
    expect(token5).not.toBeDefined();

    const link6 = 'http://www.whatsthisbug.com/';
    let token6 = isolateScope.getToken(link6);
    expect(token6).not.toBeDefined();

    const link7 = undefined;
    let token7 = isolateScope.getToken(link7);
    expect(token7).not.toBeDefined();
  }));
});
