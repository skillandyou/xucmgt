describe('Content customer tab controller', function() {
  var $rootScope;
  var $scope;
  var ctrl;
  var xucUtils;
  var xucPhoneState;
  var ctiProxy;

  beforeEach(angular.mock.module('html-templates'));
  beforeEach(angular.mock.module('karma-backend'));
  beforeEach(angular.mock.module('Agent'));
  beforeEach(angular.mock.module('xcHelper'));
  beforeEach(angular.mock.module('xcCti'));

  beforeEach(function() {
    jasmine.addMatchers({
      toEqualData : customMatchers.toEqualData
    });
  });

  beforeEach(angular.mock.inject(function(_$rootScope_, $controller, _$translate_ ,_XucUtils_, _XucPhoneState_, _CtiProxy_) {
    $rootScope =_$rootScope_;
    $scope = $rootScope.$new();
    xucUtils = _XucUtils_;
    xucPhoneState = _XucPhoneState_;
    ctiProxy = _CtiProxy_;

    ctrl = $controller('ContentHistoryController', {
      '$scope' :      $scope,
      '$translate':  _$translate_
    });
  }));

  it('can instanciate controller', function(){
    expect(ctrl).not.toBeUndefined();
  });

  it('return correct history img src path', function (){
    var baseUrl = '/assets/images/ccagent/history/call_status_';
    var ext = '.svg';
    expect(ctrl.getCallStatus('emitted')).toEqual(baseUrl + 'outgoing' + ext);
    expect(ctrl.getCallStatus('missed')).toEqual(baseUrl + 'missed' + ext);
    expect(ctrl.getCallStatus('answered')).toEqual(baseUrl + 'incoming' + ext);
    expect(ctrl.getCallStatus('exit_with_key')).toEqual(baseUrl + 'missed' + ext);
  });



  it('dial when no call in progress', function(){
    spyOn(xucUtils,'normalizePhoneNb').and.returnValue('5646546');
    spyOn(xucPhoneState, 'getCalls').and.returnValue([]);
    spyOn(ctiProxy,'dial');

    ctrl.dial('5646546');

    expect(ctiProxy.dial).toHaveBeenCalled();

  });

  it('attendedTransfer when call in progress', function(){
    spyOn(xucUtils,'normalizePhoneNb').and.returnValue('5646546');
    spyOn(xucPhoneState, 'getCalls').and.returnValue(['one']);
    spyOn(ctiProxy,'attendedTransfer');

    ctrl.dial('5646546');

    expect(ctiProxy.attendedTransfer).toHaveBeenCalled();

  });

});
