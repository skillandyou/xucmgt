import 'xccti/cti-webpack';
import 'xccti/services/XucDirectory.service';

describe('Content search tab controller', function() {
  var $rootScope;
  var $scope;
  var $translate;
  var $controller;
  var XucDirectory;
  var focus;

  var searchContact = {"status":4,"entry":["EXTERNE2","568778","","",false,""],"contact_id":"24","source":"internal","favorite":false,"hover":true};

  beforeEach(angular.mock.module('html-templates'));
  beforeEach(angular.mock.module('karma-backend'));
  beforeEach(angular.mock.module('Agent'));
  beforeEach(angular.mock.module('xcCti'));

  beforeEach(angular.mock.inject(function(_$rootScope_, _$controller_, _$translate_, _XucDirectory_, _focus_) {
    $rootScope = _$rootScope_;
    $scope = $rootScope.$new();
    XucDirectory = _XucDirectory_;
    $translate = _$translate_;
    $controller = _$controller_;
    focus = _focus_;
  }));

  function createController() {
    return $controller('ContentSearchController', {
      '$scope': $scope,
      '$translate': $translate,
      'XucDirectory': XucDirectory,
      'focus' : focus
    });
  }

  it('can instanciate controller', function() {
    var ctrl = createController();
    expect(ctrl).not.toBeUndefined();
  });

  it('should display results when notified', function() {
    spyOn(XucDirectory, 'getSearchResult').and.returnValue([searchContact]);
    var ctrl = createController();
    ctrl.onSearchResult();
    $scope.$digest();
    expect($scope.searchResult).toEqual([
      {"status":4,"entry":["EXTERNE2","568778","","",false,""],"contact_id":"24","source":"internal","favorite":false,"hover":false}
    ]);
  });

  it('should get correct label, background color and text color for a user status', function() {
    var ctrl = createController();
    expect(ctrl.getPhoneStateLabel(searchContact)).toEqual('USER_STATUS_4');
    expect(ctrl.getPhoneStateTextColor(searchContact)).toEqual('user-status-text4');
    expect(ctrl.getPhoneStateBackColor(searchContact)).toEqual('user-status4');
  });

  it('should test if a contact number is callable', function() {
    var ctrl = createController();
    expect(ctrl.isCallable(searchContact)).toBe(true);
  });

});
