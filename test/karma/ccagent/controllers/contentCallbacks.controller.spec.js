import 'xccti/cti-webpack';
import 'xccti/services/XucCallback.service';
import 'xccti/services/XucUser.service';
import 'ccagent/services/agentCallbackHelper.service';


describe('Content callbacks tab controller', function() {
  var $rootScope;
  var $scope;
  var $state;
  var xucCallback;
  var ctrl;
  var $q;
  var xucUser;
  var agentCallbackHelper;

  var cb1;
  var cb2;
  var cb3;

  beforeEach(angular.mock.module('karma-backend'));
  beforeEach(angular.mock.module('html-templates'));
  beforeEach(angular.mock.module('Agent'));

  beforeEach(angular.mock.inject(function(_$rootScope_, $controller, _agentCallbackHelper_, _XucCallback_, _$q_, _XucUser_, _$state_) {
    $rootScope = _$rootScope_;
    $scope = $rootScope.$new();
    $q = _$q_;
    $state = _$state_;
    xucUser = _XucUser_;
    xucCallback = _XucCallback_;
    agentCallbackHelper = _agentCallbackHelper_;

    cb1 = CallbackRequestBuilder('1000').withQueue({id: 1}).build();
    cb2 = CallbackRequestBuilder('1001').withQueue({id: 1}).build();
    cb3 = CallbackRequestBuilder('1002').withQueue({id: 1}).withAgentId(333).build();

    ctrl = $controller('ContentCallbacksController', {
      '$scope': $scope,
      '$q': $q,
      '$state': $state,
      'xucUser': xucUser,
      'xucCallback': xucCallback,
      'agentCallbackHelper': agentCallbackHelper
    });

  }));

  it('can instantiate controller', function() {
    expect(ctrl).not.toBeUndefined();
  });

  it('display available callback in list', function() {
    var callbacks = $q.defer();
    callbacks.resolve([cb1,cb2]);

    spyOn(agentCallbackHelper, "loadAllCallbacks").and.callFake(() => {
      return callbacks.promise;
    });
    spyOn(agentCallbackHelper, "getCallbacks").and.callFake(() => {
      return [cb1,cb2];
    });


    ctrl.loadAllCallbacks();
    $rootScope.$digest();

    expect($scope.callbacks).toEqual([cb1, cb2]);
  });

  it('display details of pending callback if already took one', function() {
    var callbacks = $q.defer();
    var pendingCallback = $q.defer();
    var agent = $q.defer();

    callbacks.resolve([cb1,cb2]);
    pendingCallback.resolve({"list": [cb3], "total":1});
    agent.resolve({"agentId": 333});

    spyOn(agentCallbackHelper, "loadAllCallbacks").and.callFake(() => {
      return callbacks.promise;
    });
    spyOn(agentCallbackHelper, "getCallbacks").and.callFake(() => {
      return [cb1,cb2];
    });
    spyOn(xucUser, "getUserAsync").and.callFake(() => {
      return agent.promise;
    });
    spyOn(agentCallbackHelper, "getPendingCallbacks").and.callFake(() => {
      return pendingCallback.promise;
    });
    spyOn(ctrl, "setActiveMenu");
    ctrl.initCallbacks();
    $rootScope.$digest();

    expect($scope.callbacks).toEqual([cb1, cb2]);
    expect($scope.activeCallback).toEqual(cb3);
    expect(ctrl.setActiveMenu).toHaveBeenCalledWith('details.id');
  });

});
