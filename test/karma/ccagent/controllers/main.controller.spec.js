import 'ccagent/app';

describe("main controller", function() {

  var $controller;
  var $rootScope;
  var $scope;
  var $q;
  var CtiProxy;
  var XucDirectory;
  var XucQueueTotal;
  var remoteConfiguration;
  var enableExternalViewDefer;

  beforeEach(angular.mock.module('html-templates'));
  beforeEach(angular.mock.module('karma-backend'));
  beforeEach(angular.mock.module('Agent'));

  beforeEach(angular.mock.inject(function (_$controller_, _$rootScope_, _$q_, _CtiProxy_, _XucDirectory_, _XucQueueTotal_, _remoteConfiguration_){
    $controller = _$controller_;
    $rootScope = _$rootScope_;
    $scope = $rootScope.$new();
    $q = _$q_;
    CtiProxy = _CtiProxy_;
    XucDirectory = _XucDirectory_;
    XucQueueTotal = _XucQueueTotal_;
    remoteConfiguration = _remoteConfiguration_;

    enableExternalViewDefer = $q.defer();
    spyOn(remoteConfiguration, 'get').and.returnValue(enableExternalViewDefer.promise);
  }));

  it("can be instantiated", function() {
    var ctrl = $controller('MainController', {$scope: $scope});
    expect(ctrl).toBeDefined();
  });

  it("should toggle window on click", angular.mock.inject(function($state) {
    var _focus = jasmine.createSpy('focus');
    var $scope = $rootScope.$new();
    var ctrl = $controller('MainController', {$scope: $scope, $state: $state, focus: _focus});

    var tResult = $q.defer();
    tResult.resolve({name: 'content.test'});
    spyOn($state,'go').and.callFake(() => {
      return tResult.promise;
    });
    ctrl.show('test');
    $rootScope.$digest();
    expect($state.go).toHaveBeenCalledWith('content.test');
    expect(_focus).not.toHaveBeenCalled();
  }));

  it("should call focus on search element when going to search state", angular.mock.inject(function($state) {
    var _focus = jasmine.createSpy('focus');
    var $scope = $rootScope.$new();
    var ctrl = $controller('MainController', {$scope: $scope, $state: $state, focus: _focus});

    var tResult = $q.defer();
    tResult.resolve({name: 'content.search'});
    spyOn($state,'go').and.callFake(() => {
      return tResult.promise;
    });
    ctrl.show('search');
    $rootScope.$digest();
    expect($state.go).toHaveBeenCalledWith('content.search');
    expect(_focus).toHaveBeenCalledWith('search');
  }));

  it("should redirect to login page when link to xuc is closed", angular.mock.inject(function($state) {
    var $scope = $rootScope.$new();
    var ctrl = $controller('MainController', {$scope: $scope, $state: $state});
    expect(ctrl).toBeDefined();
    spyOn($state,'go');
    $rootScope.$broadcast('linkDisConnected');
    $rootScope.$digest();
    expect($state.go).toHaveBeenCalledWith('login', {'error': 'LinkClosed'});
  }));

  it('should trigger search if search icon is clicked', function(){
    var ctrl = $controller('MainController', {$scope: $scope});
    spyOn(CtiProxy,'dial');
    $scope.searchValue = "1000";

    ctrl.searchOrDial();
    expect(CtiProxy.dial).toHaveBeenCalledWith("1000", {}, false);
  });

  it('should trigger search or call if enter is pressed', function(){
    var ctrl = $controller('MainController', {$scope: $scope});
    $scope.searchValue = "1000";
    spyOn(CtiProxy,'dial');

    ctrl.onKeyPress("13");
    expect(CtiProxy.dial).toHaveBeenCalledWith("1000", {}, false);
  });

  it('should do nothing if another key is pressed', function(){
    var ctrl = $controller('MainController', {$scope: $scope});
    $scope.searchValue = "1000";
    spyOn(CtiProxy,'dial');

    ctrl.onKeyPress("10");
    expect(CtiProxy.dial.calls.any()).toEqual(false);
  });

  it('should search if not a phone number', function(){
    var ctrl = $controller('MainController', {$scope: $scope});
    $scope.searchValue = "user";
    spyOn(XucDirectory,'directoryLookup');

    ctrl.searchOrDial();
    expect(XucDirectory.directoryLookup).toHaveBeenCalledWith("user");
  });

  it('should ask for waitingCalls to be displayed as a badge in activity icon', function(){
    var ctrl = $controller('MainController', {$scope: $scope});
    spyOn(XucQueueTotal,'getCalcStats').and.returnValue({sum:{WaitingCalls:5}});

    expect(ctrl.getWaitingCalls()).toBe(5);
    expect($scope.waitingCalls).toBe(5);
  });

  it('clear input when user click on x', () => {
    var ctrl = $controller('MainController', {$scope: $scope});
    ctrl.emptyInput();
    expect($scope.searchValue).toBe("");
  });

  it("displays the external view button if set in server config", angular.mock.inject(() => {
    enableExternalViewDefer.resolve(true);
    let ctrl = $controller("MainController", { $scope: $scope });
    $rootScope.$digest();
    expect(ctrl.showExternalViewButton).toBe(true);
  }));

  it("does not displays the external view button if set in server config", angular.mock.inject(() => {
    enableExternalViewDefer.resolve(false);
    let ctrl = $controller("MainController", { $scope: $scope });
    $rootScope.$digest();
    expect(ctrl.showExternalViewButton).toBe(false);
  }));

});
