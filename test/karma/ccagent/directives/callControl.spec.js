import angular from 'angular';
import 'ccagent/app';


describe('callControl directive', () => {

  var $compile;
  var $rootScope;
  var scope;
  var remoteConfiguration;
  var CtiProxy;

  beforeEach(angular.mock.module('karma-backend'));
  beforeEach(angular.mock.module('html-templates'));
  beforeEach(angular.mock.module('xcCti'));
  beforeEach(angular.mock.module('Agent'));

  beforeEach(angular.mock.inject((_$compile_, _$rootScope_, _remoteConfiguration_, _$q_, _CtiProxy_) => {
    $compile = _$compile_;
    $rootScope = _$rootScope_;
    remoteConfiguration = _remoteConfiguration_;
    CtiProxy = _CtiProxy_;

    spyOn(remoteConfiguration, 'getBoolean').and.returnValue(_$q_.resolve(true));
  }));

  it('should hangup if action is triggered with hangup parameter', function () {
    scope = $rootScope.$new();
    $compile('<call-control></call-control>')(scope);
    $rootScope.$digest();
    spyOn(CtiProxy, 'hangup');
    spyOn(CtiProxy, 'answer');
    spyOn(scope, 'isRinging').and.returnValue(true);
    scope.triggerAction('answer');
    expect(CtiProxy.hangup).not.toHaveBeenCalled();
    expect(CtiProxy.answer).toHaveBeenCalled();
  });

  it('should answer if action is triggered with answer parameter', function () {
    scope = $rootScope.$new();
    $compile('<call-control></call-control>')(scope);
    $rootScope.$digest();
    spyOn(CtiProxy, 'hangup');
    spyOn(CtiProxy, 'answer');
    spyOn(scope, 'isEstablished').and.returnValue(true);
    scope.triggerAction('hangup');
    expect(CtiProxy.hangup).toHaveBeenCalled();
    expect(CtiProxy.answer).not.toHaveBeenCalled();
  });

});