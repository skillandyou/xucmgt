import angular from 'angular';
import _ from 'lodash';
import 'ccagent/app';

describe('callLine directive', () => {
  var $compile;
  var $rootScope;
  var XucQueue;
  var XucPhoneState;
  var $q;
  var CtiProxy;

  beforeEach(angular.mock.module('karma-backend'));
  beforeEach(angular.mock.module('html-templates'));
  beforeEach(angular.mock.module('xcCti'));
  beforeEach(angular.mock.module('Agent'));

  beforeEach(angular.mock.inject((_$compile_, _$rootScope_, _XucQueue_, _$q_, _XucPhoneState_, _CtiProxy_) =>{
    $compile = _$compile_;
    $rootScope = _$rootScope_;
    XucQueue = _XucQueue_;
    XucPhoneState = _XucPhoneState_;
    $q = _$q_;
    CtiProxy = _CtiProxy_;
  }));

  function checkActionsForState(state, expectedActions, injectedScope, showRecordingControls) {
    var scope = $rootScope.$new();

    scope.canRecord = false;
    if(typeof(showRecordingControls) !== 'undefined') {
      scope.showRecordingControls = showRecordingControls;
    } else {
      scope.showRecordingControls = true;
    }
    
    scope.myCall = {
      otherDN: '1001',
      otherDName: 'James Bond',
      queueName: 'cars',
      startTime: Date.now(),
      state: state,
      direction: 'incoming'
    };
    
    var element = $compile('<call-line call="myCall" show-recording-controls="showRecordingControls"></call-line>')(scope);
    
    $rootScope.$digest();
    var isolatedScope = element.isolateScope();

    if(typeof(injectedScope) !== "undefined") {
      _.merge(isolatedScope, injectedScope);
    }
    
    var actions = _.map(isolatedScope.getCallActions(), (item) => {
      return item.label;
    });
    
    _.forEach(expectedActions, function(item) {
      expect(actions).toContain(item);
    });

    expect(actions.length).toBe(expectedActions.length);

  }
  
  it('should display hold and hangup actions when not-recorded call is established', () => {
    spyOn(XucPhoneState, 'getCalls').and.returnValue([{}]);
    checkActionsForState('Established', ['hold', 'hangup']);
  });

  it('should display hold, hangup, stopRecording actions when recording call is established', () => {
    spyOn(XucPhoneState, 'getCalls').and.returnValue([{}]);
    checkActionsForState('Established', ['hold', 'hangup', 'stoprecording'], { canRecord: true, recording: true });
  });

  it('should display hold, hangup, startRecording actions when recording-capable call is established', () => {
    spyOn(XucPhoneState, 'getCalls').and.returnValue([{}]);
    checkActionsForState('Established', ['hold', 'hangup', 'startrecording'], { canRecord: true, recording: false });
  });

  it('should NOT display stopRecording action when recording call is established but action is disabled', () => {
    spyOn(XucPhoneState, 'getCalls').and.returnValue([{}]);
    checkActionsForState('Established', ['hold', 'hangup'], { canRecord: true, recording: true }, false);
  });

  it('should NOT display startRecording action when recording-capable call is established but action is disabled', () => {
    spyOn(XucPhoneState, 'getCalls').and.returnValue([{}]);
    checkActionsForState('Established', ['hold', 'hangup'], { canRecord: true, recording: false }, false);
  });

  it('should display hangup action when call is dialing', () => {
    checkActionsForState('Dialing', ['hangup']);
  });

  it('should display answer and hangup action when call is ringing', () => {
    checkActionsForState('Ringing', ['answer', 'hangup']);
  });

  it('should display unhold action when one call is on hold', () => {
    var calls = [
      {
        state: 'OnHold'
      }];
    spyOn(XucPhoneState, 'getCalls').and.returnValue(calls);
    checkActionsForState('OnHold', ['unhold']);
  });

  it('should not display hold action if multiple calls', () => {
    var calls = [
      {
        state: 'Established'
      },
      {
        state: 'Dialing'
      }];
    spyOn(XucPhoneState, 'getCalls').and.returnValue(calls);
    checkActionsForState('Established', ['hangup']);
  });

  it('should display DTMF keyboard action when webrtc', () => {
    spyOn(CtiProxy, 'isUsingWebRtc').and.returnValue(true);
    spyOn(XucPhoneState, 'getCalls').and.returnValue([{}]);
    checkActionsForState('Established', ['hold', 'hangup', 'dtmfkeypad']);
  });

  it('should display attended transfer on hold when there are multiple calls', () => {
    var calls = [
      {
        state: 'Established'
      },
      {
        state: 'OnHold'
      }];
    spyOn(XucPhoneState, 'getCalls').and.returnValue(calls);
    checkActionsForState('OnHold', ['transfer']);
  });

  it('should display conference on hold when there are multiple calls', () => {
    var calls = [
      {
        state: 'Established'
      },
      {
        state: 'OnHold'
      }];
    spyOn(CtiProxy, 'isConferenceCapable').and.returnValue(true);
    spyOn(XucPhoneState, 'getCalls').and.returnValue(calls);
    checkActionsForState('OnHold', ['conference', 'transfer']);
  });

  it('should not propose answer and hangup when ringing standard lines', () => {
    var calls = [
      {
        state: 'Ringing'
      }];
    spyOn(XucPhoneState, 'getCalls').and.returnValue(calls);
    checkActionsForState('Ringing', ['answer', 'hangup']);
  });

  it('should not propose answer nor hangup when ringing for custom lines', () => {
    var calls = [
      {
        state: 'Ringing'
      }];
    spyOn(CtiProxy, 'isCustomLine').and.returnValue(true);
    spyOn(XucPhoneState, 'getCalls').and.returnValue(calls);
    checkActionsForState('Ringing', []);
  });

  it('should not propose hold nor conference when established for custom line', () => {
    var calls = [
      {
        state: 'Established'
      }];
    spyOn(CtiProxy, 'isCustomLine').and.returnValue(true);
    spyOn(XucPhoneState, 'getCalls').and.returnValue(calls);
    checkActionsForState('Established', ['hangup']);
  });

  it('should not propose unhold nor conference nor hangup when onHold for custom line', () => {
    var calls = [
      {
        state: 'OnHold'
      }];
    spyOn(CtiProxy, 'isCustomLine').and.returnValue(true);
    spyOn(XucPhoneState, 'getCalls').and.returnValue(calls);
    checkActionsForState('OnHold', []);
  });


  it('should display queue real name', () => {
    var scope = $rootScope.$new();
    spyOn(XucQueue, 'getQueueByNameAsync').and.returnValue($q.resolve({name: 'cars', displayName: 'My Cars' }));
    

    scope.canRecord = false;
    
    scope.myCall = {
      otherDN: '1001',
      otherDName: 'James Bond',
      queueName: 'cars',
      startTime: Date.now(),
      state: 'Established',
      direction: 'incoming',
      acd: true
    };
    
    var element = $compile('<call-line call="myCall"></call-line>')(scope);
    
    $rootScope.$digest();
    var isolatedScope = element.isolateScope();   

    expect(isolatedScope.queueDisplayName).toBe('My Cars');
  });
});
