console.log("-----------------jasmine customMatchers---------------------");

var customMatchers = function () {
    var  toEqualDataMatcher = function(util, customEqualityTesters) {
        return {
            compare: function(actual, expected) {
                var passed = angular.equals(actual, expected);
                return {
                    pass : passed
                };
            }
        };
    };
    var  statEqualMatcher = function(util, customEqualityTesters) {
        return {
            compare: function(actual, name, expected) {
                var passed = (actual[name] === expected);
                return {
                    pass : passed,
                    message: 'Statistic ' + name + ' is ' + actual[name] + ' but expected to be ' + expected
                };
            }
        };
    };
    return {
        toEqualData : toEqualDataMatcher,
        statEqual : statEqualMatcher
    };
}();


var  statEqualMatcher = function(util, customEqualityTesters) {
        return {
            compare: function(actual, name, expected) {
                var passed = (actual[name] === expected);
                return {
                    pass : passed,
                    message: 'Statistic ' + name + ' is ' + actual[name] + ' but expected to be ' + expected
                };
            }
        };
    };
