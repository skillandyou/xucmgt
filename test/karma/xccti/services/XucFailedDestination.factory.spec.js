import 'xccti/cti-webpack';

describe('Factory: XucFailedDestination', function () {
  let $q;
  let $httpBackend;
  let XucFailedDestination;
  let XucLink;
  let token = "aaaa-bbbb-cccc-dddd-1234";
  let user = {username: "jbond", phoneNumber: "1001", token: token};
  let checkAuthHeader;

  beforeEach(angular.mock.module('html-templates'));
  beforeEach(angular.mock.module('karma-backend'));
  beforeEach(angular.mock.module('xcCti'));

  beforeEach(function () {
    jasmine.addMatchers({
      toEqualData: customMatchers.toEqualData
    });
  });

  beforeEach(angular.mock.inject(function (_XucFailedDestination_, _XucLink_, _$q_, _$httpBackend_) {
    XucFailedDestination = _XucFailedDestination_;
    XucLink = _XucLink_;
    $q = _$q_;
    $httpBackend = _$httpBackend_;

    spyOn(XucLink, 'whenLogged').and.returnValue($q.resolve(user));
    spyOn(XucLink, 'getServerUrl').and.returnValue("http://localhost");

    checkAuthHeader = (headers) => {
      return headers['Authorization'] === 'Bearer ' + token;
    };
  }));

  it('should get the selected dissuasion', function () {
    let queueId = 3;

    const dissuasion = {
      "id": queueId,
      "name": "sales",
      "dissuasion": {
        "type": "soundFile",
        "value": {
          "soundFile": "sales_audio_file"
        }
      }
    };

    $httpBackend.expectGET(`http://localhost/xuc/api/2.0/config/queue/${queueId}/dissuasion`, checkAuthHeader).respond(() => {
      return [200, dissuasion];
    });
    let promise = XucFailedDestination.getDissuasionSelected(queueId);
    $httpBackend.flush();

    promise.then((result) => {
      expect(result.data).toEqual(dissuasion);
    });
  });

  it('should get the list of available sound files and queues for dissuasion', function () {
    let queueId = 3;

    const dissuasion = {
      "id": 3,
      "name": "sales",
      "dissuasions": {
        "soundFiles": [
          {
            "soundFile": "sales_audio_file"
          },
          {
            "soundFile": "sales_audio_file_2"
          }
        ],
        "queues": [
          {
            "queueName": "switchboard"
          }
        ]
      }
    };

    $httpBackend.expectGET(`http://localhost/xuc/api/2.0/config/queue/${queueId}/dissuasions`, checkAuthHeader).respond(() => {
      return [200, dissuasion];
    });
    let promise = XucFailedDestination.getAvailableDissuasions(queueId);
    $httpBackend.flush();

    promise.then((result) => {
      expect(result.data).toEqual(dissuasion);
    });
  });

  it('should get a list of dissuasions if there is nothing selected', function () {
    let queueName = "promotion";
    let queueId = 3;

    const expected = {
      soundFiles: [
        {
          "fileName": "promotion_soundFile1",
          "displayName": "soundFile1",
          "selected": false
        },
        {
          "fileName": "promotion_soundFile2",
          "displayName": "soundFile2",
          "selected": false
        }
      ],
      "queues": [
        {
          "name": "switchboard",
          "selected": false
        }
      ]
    };

    const selectedSoundFile = {
      "id": queueId,
      "name": queueName,
      "dissuasion": {
        "type": "other"
      }
    };
    const listSoundFile = {
      "id": queueId,
      "name":queueName,
      "dissuasions": {
        "soundFiles": [
          {
            "soundFile": "promotion_soundFile1"
          },
          {
            "soundFile": "promotion_soundFile2"
          }
        ],
        "queues": [
          {
            "queueName": "switchboard"
          }
        ]
      }
    };

    $httpBackend.expectGET(`http://localhost/xuc/api/2.0/config/queue/${queueId}/dissuasions`, checkAuthHeader).respond(() => {
      return [200, listSoundFile];
    });

    $httpBackend.expectGET(`http://localhost/xuc/api/2.0/config/queue/${queueId}/dissuasion`, checkAuthHeader).respond(() => {
      return [200, selectedSoundFile];
    });

    let promise = XucFailedDestination.getListDissuasion(queueId, queueName);

    promise.then((result) => {
      expect(result).toEqual(expected);
    }, ()=> {
      fail('should be resolve');
    });

    $httpBackend.flush();
  });

  it('should get a list of dissuasions if there is a sound file selected', function () {
    let queueName = "promotion";
    let queueId = 3;

    const expected = {
      soundFiles: [
        {
          "fileName": "promotion_soundFile1",
          "displayName": "soundFile1",
          "selected": false
        },
        {
          "fileName": "promotion_soundFile2",
          "displayName": "soundFile2",
          "selected": true
        }
      ],
      queues: [
        {
          "name": "switchboard",
          "selected": false
        }
      ]
    };

    const selectedSoundFile = {
      "id": queueId,
      "name": queueName,
      "dissuasion": {
        "type": "soundFile",
        "value": {
          "soundFile": "promotion_soundFile2"
        }
      }
    };
    const listSoundFile = {
      "id": queueId,
      "name":queueName,
      "dissuasions": {
        "soundFiles": [
          {
            "soundFile": "promotion_soundFile1"
          },
          {
            "soundFile": "promotion_soundFile2"
          }
        ],
        "queues": [
          {
            "queueName": "switchboard"
          }
        ]
      }
    };

    $httpBackend.expectGET(`http://localhost/xuc/api/2.0/config/queue/${queueId}/dissuasions`, checkAuthHeader).respond(() => {
      return [200, listSoundFile];
    });

    $httpBackend.expectGET(`http://localhost/xuc/api/2.0/config/queue/${queueId}/dissuasion`, checkAuthHeader).respond(() => {
      return [200, selectedSoundFile];
    });

    let promise = XucFailedDestination.getListDissuasion(queueId, queueName);

    promise.then((result) => {
      expect(result).toEqual(expected);
    }, ()=> {
      fail('should be resolve');
    });

    $httpBackend.flush();
  });

  it('should get a list of dissuasions if there is a queue selected', function () {
    let queueName = "promotion";
    let queueId = 3;

    const expected = {
      soundFiles: [
        {
          "fileName": "promotion_soundFile1",
          "displayName": "soundFile1",
          "selected": false
        },
        {
          "fileName": "promotion_soundFile2",
          "displayName": "soundFile2",
          "selected": false
        }
      ],
      queues: [
        {
          "name": "switchboard",
          "selected": true
        }
      ]
    };

    const selectedSoundFile = {
      "id": queueId,
      "name": queueName,
      "dissuasion": {
        "type": "queue",
        "value": {
          "queueName": "switchboard"
        }
      }
    };
    const listSoundFile = {
      "id": queueId,
      "name":queueName,
      "dissuasions": {
        "soundFiles": [
          {
            "soundFile": "promotion_soundFile1"
          },
          {
            "soundFile": "promotion_soundFile2"
          }
        ],
        "queues": [
          {
            "queueName": "switchboard"
          }
        ]
      }
    };

    $httpBackend.expectGET(`http://localhost/xuc/api/2.0/config/queue/${queueId}/dissuasions`, checkAuthHeader).respond(() => {
      return [200, listSoundFile];
    });

    $httpBackend.expectGET(`http://localhost/xuc/api/2.0/config/queue/${queueId}/dissuasion`, checkAuthHeader).respond(() => {
      return [200, selectedSoundFile];
    });

    let promise = XucFailedDestination.getListDissuasion(queueId, queueName);

    promise.then((result) => {
      expect(result).toEqual(expected);
    }, ()=> {
      fail('should be resolve');
    });

    $httpBackend.flush();
  });

  it('should change the selected dissuasion sound file', function () {
    const fileName = 'dissuasion';
    let queueId = 3;
    const expected = {'id': queueId, 'soundFile': fileName};
    
    $httpBackend.expectPUT(`http://localhost/xuc/api/2.0/config/queue/${queueId}/dissuasion/sound_file`).respond(() => {
      return [200, expected];
    });
    let promise = XucFailedDestination.setSoundFileDissuasion(queueId, fileName);
    $httpBackend.flush();

    promise.then((result) => {
      expect(result.data).toEqual(expected);
    });
  });

  it('should select the default queue for dissuasion', function () {
    const queueName = 'dissuasion default queue';
    let queueId = 1;
    const expected ={'id': queueId, 'queueName': queueName};
    
    $httpBackend.expectPUT(`http://localhost/xuc/api/2.0/config/queue/${queueId}/dissuasion/queue`).respond(() => {
      return [200, expected];
    });

    let promise = XucFailedDestination.setQueueDestination(queueId, queueName);
    $httpBackend.flush();

    promise.then((result) => {
      expect(result.data).toEqual(expected);
    });
  });

});