describe('Xuc service flashtext', () => {
  var xucFlashText;
  var $rootScope;

  beforeEach(() => {
    spyOn(Cti,'setHandler').and.callThrough();
  });

  beforeEach(angular.mock.module('xcCti'));

  beforeEach(angular.mock.inject((_XucFlashText_,_$rootScope_) => {
    $rootScope = _$rootScope_;
    xucFlashText = _XucFlashText_;
    spyOn($rootScope, '$broadcast');
  }));

  function buildFlashtext(msg) {
    return {
      event: "FlashTextUserMessage",
      message: msg,
      date: 'fake date',
      from: {
        username: "fake", displayName: "Fake User"
      },
      sequence: 1
    };
  }

  it('setup flashtext handlers when starting', () => {
    expect(Cti.setHandler).toHaveBeenCalled();
  });


  it('get existing flashTexts', () => {
    Cti.Topic(Cti.MessageType.FLASHTEXTEVENT).publish(buildFlashtext('get'));
    $rootScope.$digest();
    let result = xucFlashText.getFlashTexts();
    expect(result.map((e) => e.toJSON())).toEqual(
      [
        {
          username: 'fake',
          displayName: 'Fake User',
          partyMessages: [
            {
              id: 1,
              message: 'get',
              direction: 'incoming',
              date: 'fake date'
            }
          ],
          lastReceptionDate: 'fake date'
        }
      ]
    );
  });

  it('create a party message', function () {
    let message = {
      date: "fake date",
      event: "FlashTextUserMessage",
      from: { username: "user1", displayName: "Hubert Gousse" },
      message: "fake message",
      sequence: 1,
      direction: 'outgoing'
    };

    let result = {
      id: 1,
      message: 'fake message',
      direction: 'outgoing',
      date: 'fake date'
    };

    let partyMessage = xucFlashText.createPartyMessage(message);
    expect(partyMessage.toJSON()).toEqual(result);
  });

  it('create a party', function () {
    let message = {
      date: "2019-08-29T13:09:26.543+02:00",
      event: "FlashTextUserMessage",
      from: { username: "user1", displayName: "Hubert Gousse" },
      message: "fake message",
      sequence: 1,
      direction: 'outgoing'
    };

    let result = {
      username: "user1",
      displayName: "Hubert Gousse",
      lastReceptionDate: '2019-08-29T13:09:26.543+02:00',
      partyMessages: [
        {
          id: 1,
          message: 'fake message',
          direction: 'outgoing',
          date: '2019-08-29T13:09:26.543+02:00'
        }
      ]
    };

    let party = xucFlashText.getParty(message);
    expect(party.toJSON()).toEqual(result);
  });

  it('notify flashtext callback when flashtext message is received', () => {
    var handler = {
      onAction : () => {}
    };

    spyOn(handler, "onAction");

    xucFlashText.subscribeToFlashText($rootScope, handler.onAction);
    $rootScope.$emit('FlashTextReceived', buildFlashtext('notify'));
    $rootScope.$digest();
    expect(handler.onAction).toHaveBeenCalled();
  });

  it('sends flashtext message', () => {
    spyOn(Cti, 'sendFlashTextMessage');
    xucFlashText.sendFlashText('user', 'my message', 'outgoing');

    expect(Cti.sendFlashTextMessage).toHaveBeenCalledWith('user', 1, 'my message');
    xucFlashText.sendFlashText('user', 'my message', 'outgoing');
    expect(Cti.sendFlashTextMessage).toHaveBeenCalledWith('user', 2, 'my message');
  });

  it('find a party using an username', () => {
    let message2 = {
      username: "user2",
      displayName: "Robert Pousse",
      lastReceptionDate: '2019-08-29T13:09:26.543+02:00',
      partyMessages: [
        {
          id: 1,
          message: 'fake message',
          direction: 'outgoing',
          date: '2019-08-29T13:09:26.543+02:00'
        }
      ]
    };

    let message1 = {
      username: "user1",
      displayName: "Hubert Gousse",
      lastReceptionDate: '2019-08-29T13:09:26.543+02:00',
      partyMessages: [
        {
          id: 1,
          message: 'fake message',
          direction: 'outgoing',
          date: '2019-08-29T13:09:26.543+02:00'
        }
      ]
    };

    let parties = [message1, message2];
    let result = xucFlashText.filterExistingParties(parties, 'user1');
    expect(result).toBe(message1);
  });

  it('can set and return the current party we are chatting with', function () {
    xucFlashText.setCurrentParty('user1');
    let currentParty = xucFlashText.returnCurrentParty();
    expect(currentParty).toEqual('user1');
  });

  it('should check if there are new unread messages', function () {
    let partyNewMsg = [
      {
        isRead: true
      }, {
        isRead: false
      }, {
        isRead: true
      },
    ];

    let partyNoNewMsg = [
      {
        isRead: true
      }, {
        isRead: true
      }, {
        isRead: true
      },
    ];

    let result1 = xucFlashText.checkForNewMessage(partyNewMsg);
    let result2 = xucFlashText.checkForNewMessage(partyNoNewMsg);
    expect(result1).toBe(true);
    expect(result2).toBe(false);
  });

});
