'use strict';
describe('Xuc service', function() {
  var xucQueue;
  var $rootScope;

  var QueueBuilder = function(id,name) {
    this.queue = {};
    this.queue.name = name;
    this.queue.displayName = '';
    this.queue.id = id;
    this.stats = {};
    this.stats.queueId = id;
    this.stats.counters = [];

    this.withStat = function(statName,value) {
      var stat = {};
      stat.statName = statName;
      stat.value = value;
      this.stats.counters.push(stat);
      return this;
    };
    this.withDisplayName = function(displayName) {
      this.queue.displayName = displayName;
      return this;
    };
    this.build = function() {
      xucQueue.onQueueConfig(this.queue);
      xucQueue.onQueueStatistics(this.stats);
      return this.queue;
    };
    return this;
  };

  var storedQueue = function(queueId) {
    for (var j = 0; j < xucQueue.getQueues().length; j++) {
      if (xucQueue.getQueues()[j].id === queueId) {
        return xucQueue.getQueues()[j];
      }
    }
  };
  
  beforeEach(angular.mock.module('xcCti'));

  beforeEach(function() {
    jasmine.addMatchers({
      toEqualData : customMatchers.toEqualData,
      statEqual: customMatchers.statEqual
    });
  });

  beforeEach(angular.mock.inject(function(_XucQueue_,_$rootScope_) {
    xucQueue = _XucQueue_;
    $rootScope = _$rootScope_;
    spyOn($rootScope, '$broadcast');
    spyOn(Cti, 'sendCallback');
  }));

  it('should have empty queues at init', function() {
    expect(xucQueue.getQueues()).toEqualData([]);
  });

  it('should init queue with longestWaittime and waiting calls on configuration on received', function(){
    var queueConfig = {'id':3};
    xucQueue.onQueueConfig(queueConfig);

    expect(storedQueue(3)).statEqual('LongestWaitTime','-');
    expect(storedQueue(3)).statEqual('WaitingCalls',0);
  });

  it('should update queue config', function() {
    var queueConfig = {'id':7, 'name' : 'Hello'};
    var updatedQueueConfig = {'id':7, 'name' : 'Bonjour'};

    xucQueue.onQueueConfig(queueConfig);
    xucQueue.onQueueConfig(updatedQueueConfig);

    expect(storedQueue(7).name).toBe('Bonjour');
  });
  it('should update queue config on queue list received broadcast a loaded event and subscribe to queue statistics', function() {
    spyOn(Cti, 'subscribeToQueueStats');
    var queueList = [{'id':17, 'name' : 'Hello'}];

    xucQueue.onQueueList(queueList);

    expect(storedQueue(17).name).toBe('Hello');
    expect($rootScope.$broadcast).toHaveBeenCalledWith('QueuesLoaded');
    expect(Cti.subscribeToQueueStats).toHaveBeenCalled();

  });

  it('should update queue statistics by id', function() {
    new QueueBuilder(78,'BlueIsland')
      .build();

    var statistics = {'queueId':78, 'counters':[{'statName':'TalkingAgents', 'value' : 8}]};

    xucQueue.onQueueStatistics(statistics);

    expect(storedQueue(78)).statEqual('TalkingAgents',8);
  });
  it('should update queue statistics by reference (name)', function() {
    new QueueBuilder(32,'Yellow')
      .build();

    var statistics = {'queueRef':'Yellow', 'counters':[{'statName':'TotalNumberCallsAbandonned', 'value' : 4}]};

    xucQueue.onQueueStatistics(statistics);

    expect(storedQueue(32)).statEqual('TotalNumberCallsAbandonned',4);
  });
  it('should reset LongestWaitTime when WaitingCalls goes back to 0', function(){

    new QueueBuilder(55,'Orange')
      .withStat('WaitingCalls',10)
      .withStat('LongestWaitTime' , 520)
      .build();

    var statistics = {'queueId':55, 'counters':[{'statName':'WaitingCalls', 'value' : 0}]};

    xucQueue.onQueueStatistics(statistics);

    expect(storedQueue(55)).statEqual('LongestWaitTime','-');
  });
  it('should not reset LongestWaitTime when WaitingCalls is 0 and we receive another statistic', function(){

    new QueueBuilder(55,'Orange')
      .withStat('WaitingCalls',0)
      .withStat('LongestWaitTime' , 0)
      .build();

    var statistics = {'queueId':55, 'counters':[{'statName':'TotalNumberCallsAbandonned', 'value' : 7}]};

    xucQueue.onQueueStatistics(statistics);

    expect(storedQueue(55)).statEqual('LongestWaitTime',0);
  });

  it('should return a queue by id', function(){
    new QueueBuilder(55,'Orange')
      .build();

    var queue = xucQueue.getQueue(55);
    expect(queue.name).toBe('Orange');
  });

  it('should return a queue by id asynchronously', function(){
    var queueList = [{'id':17, 'name' : 'Hello'}];

    var cb = {getQueueCb: function() {}};
    spyOn(cb, 'getQueueCb');

    xucQueue.getQueueAsync(17).then(cb.getQueueCb);
    xucQueue.onQueueList(queueList);
    $rootScope.$digest();
    expect(cb.getQueueCb).toHaveBeenCalledWith(queueList[0]);
  });

  it('should return a queue by name asynchronously', function(){
    var queueList = [
      {'id':15, 'name' : 'cars', 'displayName': 'Cars Q'},
      {'id':17, 'name' : 'trucks', 'displayName': 'Trucks Q'}
    ];

    var cb = {getQueueCb: function() {}};
    spyOn(cb, 'getQueueCb');

    xucQueue.getQueueByNameAsync("trucks").then(cb.getQueueCb);
    xucQueue.onQueueList(queueList);
    $rootScope.$digest();
    expect(cb.getQueueCb).toHaveBeenCalledWith(queueList[1]);
  });

  it('should return all queues asynchronously', function(){
    var queueList = [{'id':17, 'name' : 'Hello'}, {'id':18, 'name' : 'World'}];

    var cb = {getQueuesCb: function() {}};
    spyOn(cb, 'getQueuesCb');

    xucQueue.getQueuesAsync().then(cb.getQueuesCb);
    xucQueue.onQueueList(queueList);
    $rootScope.$digest();
    expect(cb.getQueuesCb).toHaveBeenCalledWith(queueList);
  });

  it('should return a list of queues excluding a list of queues', function(){
    new QueueBuilder(55,'Orange')
      .build();
    new QueueBuilder(44,'Blue')
      .build();
    new QueueBuilder(37,'Yellow')
      .build();

    var queue = {'id':44};
    var excludeQueues = [queue];

    var queues = xucQueue.getQueuesExcept(excludeQueues);

    expect(queues.length).toBe(2);
  });
  it('should return a list of queues by ids', function() {
    var q1 = new QueueBuilder(101,'Sky').build();
    var q2 = new QueueBuilder(102,'Earth').build();
    var q3 = new QueueBuilder(103,'Sea').build();

    var queueIds = [101,103];

    var queues = xucQueue.getQueueByIds(queueIds);

    expect(queues.length).toBe(2);
    expect(queues).toContain(q1);
    expect(queues).not.toContain(q2);
    expect(queues).toContain(q3);
  });
});

