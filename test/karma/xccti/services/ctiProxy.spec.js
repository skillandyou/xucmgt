describe('CtiProxy', function() {

  var ctiMethods = ['dial', 'hangup', 'answer', 'hold', 'attendedTransfer', 'completeTransfer', 'cancelTransfer', 'conference', 'unsetHandler', 'getConfig'];
  var xcWebrtcMethods = ['dial', 'answer', 'attendedTransfer', 'hold', 'dtmf', 'clearHandlers', 'setHandler', 'disableICE', 'initByLineConfig', 'stop'];

  var xcWebrtcMock;
  var oldXcWebrtc;
  var ctiMock;
  var oldCti;
  var lineCfgCallback;

  var XucPhoneState;
  var phoneCalls;
  var remoteConfiguration;
  var $q;
  var $rootScope;
  var XucPhoneEventListener;
  var errorModal;

  var webRtcAudio;

  var hostnameValue = 'testName';
  var locationValue = { hostname: hostnameValue, protocol: 'https:', port: ''};
  var disableDefer;
  var getUserMediaDefer;
  var mediaDevicesMock = {
    getUserMedia: function() {}
  }; 
  var windowProvider = {
    $get: function() {
      return {
        location: locationValue,
        navigator: {
          languages: 'en_EN',
          mediaDevices: mediaDevicesMock
        }
      };
    }
  };

  beforeEach(angular.mock.module('html-templates'));
  beforeEach(angular.mock.module('karma-backend'));

  beforeEach(function() {
    xcWebrtcMock = jasmine.createSpyObj('xc_webrtc', xcWebrtcMethods);
    xcWebrtcMock.MessageType = xc_webrtc.MessageType;
    oldXcWebrtc = xc_webrtc;
    xc_webrtc = xcWebrtcMock;

    ctiMock = jasmine.createSpyObj('Cti', ctiMethods);
    ctiMock.MessageType = Cti.MessageType;
    ctiMock.setHandler = function(msgType, callback) {
      if(msgType === Cti.MessageType.LINECONFIG) {
        lineCfgCallback = callback;
      }
    };
    oldCti = Cti;
    Cti = ctiMock;
  });

  beforeEach(function() {
    angular.mock.module(function($provide) {
      $provide.provider('$window', windowProvider);
    });
    angular.mock.module('xcCti');
    angular.mock.module('xcHelper');
    angular.mock.inject(function(_XucPhoneState_, _XucPhoneEventListener_, _remoteConfiguration_, _$q_, _$rootScope_, _errorModal_) {
      XucPhoneState = _XucPhoneState_;
      XucPhoneEventListener = _XucPhoneEventListener_;
      remoteConfiguration = _remoteConfiguration_;
      $q = _$q_;
      errorModal = _errorModal_;
      $rootScope = _$rootScope_;
      disableDefer = $q.defer();
      getUserMediaDefer = $q.defer();
      spyOn(errorModal, 'showErrorModal').and.callFake(() => { });
      spyOn(mediaDevicesMock, 'getUserMedia').and.returnValue(getUserMediaDefer.promise);
      spyOn(remoteConfiguration, 'getBooleanOrElse').and.returnValue(disableDefer.promise);
      spyOn(XucPhoneState, 'getCalls').and.callFake(function() { return phoneCalls; });
      phoneCalls = [];
      spyOn(XucPhoneEventListener, 'addHandlerCustom').and.callThrough();
      spyOn($rootScope, '$broadcast').and.callThrough();
    });
  });

  beforeEach(angular.mock.inject(function(_webRtcAudio_) {
    webRtcAudio = _webRtcAudio_;
    spyOn(webRtcAudio, 'enable');
    spyOn(webRtcAudio, 'disable');
  }));

  beforeEach(function() {
    jasmine.addMatchers({
      toEqualData : customMatchers.toEqualData
    });
  });

  beforeEach(angular.mock.inject(function($httpBackend) {
    $httpBackend.whenGET("assets/i18n/phonebar-en.json").respond({});
    $httpBackend.whenGET("/ucassistant/login.html").respond("");
  }));

  afterEach(function() {
    xc_webrtc = oldXcWebrtc;
    Cti = oldCti;
  });

  it('can instanciate service', angular.mock.inject(function(CtiProxy) {
    expect(CtiProxy).not.toBeUndefined();
  }));

  it('is using Cti in default state', angular.mock.inject(function(CtiProxy) {
    expect(CtiProxy.isUsingWebRtc()).toBe(false);
    expect(CtiProxy.isUsingCti()).toBe(true);
  }));

  var verifyProxyCalls = function(CtiProxy, target, hangupTarget) {
    CtiProxy.hangup();
    expect(hangupTarget.hangup).toHaveBeenCalledWith();

    var methods = ['dial', 'answer', 'hold', 'attendedTransfer'];
    for (var idx in methods) {
      var method = methods[idx];
      CtiProxy[method]();
      expect(target[method]).toHaveBeenCalled();
    }
  };

  it('proxy calls to Cti in default state', angular.mock.inject(function(CtiProxy) {
    expect(CtiProxy.isUsingWebRtc()).toBe(false);
    verifyProxyCalls(CtiProxy, ctiMock, ctiMock);
  }));


  it('on ctiLoggedOn registers Cti handler and requests line config', angular.mock.inject(function(CtiProxy) {
    $rootScope.$broadcast('ctiLoggedOn');
    expect(CtiProxy).not.toBeUndefined();
    expect(lineCfgCallback).not.toBeUndefined();
    expect(ctiMock.getConfig).toHaveBeenCalledWith('line');
  }));

  it('on lineConfig with webRtc and no device true inits webRTC and asks for audio permission', angular.mock.inject(function(CtiProxy) {
    $rootScope.$broadcast('ctiLoggedOn');
    var lineCfg = {
      hasDevice: false,
      webRtc: true
    };
    lineCfgCallback(lineCfg);
    expect(CtiProxy.isUsingWebRtc()).toEqual(true);
    expect(XucPhoneEventListener.addHandlerCustom).toHaveBeenCalled();
    expect(xc_webrtc.initByLineConfig)
      .toHaveBeenCalledWith(lineCfg, 'XiVO Assistant', true, 443, 'audio_remote', hostnameValue);
    expect(webRtcAudio.enable).toHaveBeenCalled();
    expect(mediaDevicesMock.getUserMedia).toHaveBeenCalledWith({
      "audio": {
        "mandatory": {
          "googAutoGainControl": "false" ,
          "googAutoGainControl2": "false" ,
          "googEchoCancellation": "true" ,
          "googEchoCancellation2": "true" ,
          "googNoiseSuppression": "false" ,
          "googNoiseSuppression2": "false" ,
          "googHighpassFilter": "false" ,
          "googAudioMirroring": "false" 
        }
      }
    });
  }));

  it('on lineConfig with webrtc false and some line id use Cti', angular.mock.inject(function(CtiProxy) {
    $rootScope.$broadcast('ctiLoggedOn');
    var lineCfg = {
      hasDevice: true,
      webRtc: false,
    };
    lineCfgCallback(lineCfg);
    expect(CtiProxy.isUsingCti()).toBe(true);
    verifyProxyCalls(CtiProxy, ctiMock, ctiMock);
  }));

  it('on lineConfig with webrtc false, without device and with name starting by Local/ report custom line', angular.mock.inject(function(CtiProxy) {
    $rootScope.$broadcast('ctiLoggedOn');
    var lineCfg = {
      hasDevice: false,
      webRtc: false,
      name: 'Local/01230041301'
    };
    lineCfgCallback(lineCfg);
    expect(CtiProxy.isUsingWebRtc()).toEqual(false);
    expect(CtiProxy.isCustomLine()).toEqual(true);
  }));

  it('on lineConfig with webrtc false, without device and with name starting by local/ (any case)  report custom line', angular.mock.inject(function(CtiProxy) {
    $rootScope.$broadcast('ctiLoggedOn');
    var lineCfg = {
      hasDevice: false,
      webRtc: false,
      name: 'loCal/01230041301'
    };
    lineCfgCallback(lineCfg);
    expect(CtiProxy.isUsingWebRtc()).toEqual(false);
    expect(CtiProxy.isCustomLine()).toEqual(true);
  }));


  it('Allow conference on Snom device when using Cti', angular.mock.inject(function(CtiProxy) {
    $rootScope.$broadcast('ctiLoggedOn');
    var lineCfg = {
      id:"1000",
      hasDevice: true,
      webRtc: false,
      vendor: "Snom"
    };
    lineCfgCallback(lineCfg);

    expect(CtiProxy.isUsingCti()).toBe(true);
    expect(CtiProxy.getDeviceVendor()).toBe("Snom");
    expect(CtiProxy.isConferenceCapable()).toBe(true);
  }));

  it('Prevent conference on Non-Snom device when using Cti', angular.mock.inject(function(CtiProxy) {
    $rootScope.$broadcast('ctiLoggedOn');
    var lineCfg = {
      id:"1000",
      hasDevice: true,
      webRtc: false,
      vendor: "Yealink"
    };
    lineCfgCallback(lineCfg);

    expect(CtiProxy.isUsingCti()).toBe(true);
    expect(CtiProxy.getDeviceVendor()).toBe("Yealink");
    expect(CtiProxy.isConferenceCapable()).toBe(false);
  }));

  it('It allow conference when using webRTC', angular.mock.inject(function(CtiProxy) {
    $rootScope.$broadcast('ctiLoggedOn');
    var lineCfg = {
      id:"1000",
      hasDevice: true,
      webRtc: false,
      vendor: "Snom"
    };
    lineCfgCallback(lineCfg);


    $rootScope.$broadcast('ctiLoggedOn');
    lineCfg = {
      hasDevice: false,
      webRtc: true,
      vendor: null
    };
    lineCfgCallback(lineCfg);

    expect(CtiProxy.isUsingWebRtc()).toBe(true);
    expect(CtiProxy.getDeviceVendor()).toBe(CtiProxy.UnknownDeviceVendor);
    expect(CtiProxy.isConferenceCapable()).toBe(true);
  }));

  it('Reset device vendor to unknown after switch from Snom to webRTC', angular.mock.inject(function(CtiProxy) {
    $rootScope.$broadcast('ctiLoggedOn');

    var lineCfg = {
      id:"1000",
      hasDevice: true,
      webRtc: false,
      vendor: "Snom"
    };
    lineCfgCallback(lineCfg);

    expect(CtiProxy.getDeviceVendor()).toBe("Snom");

    lineCfg = {
      hasDevice: false,
      webRtc: true,
      vendor: null
    };
    lineCfgCallback(lineCfg);

    expect(CtiProxy.getDeviceVendor()).toBe(CtiProxy.UnknownDeviceVendor);
  }));

  it('processLineCfg on line with webrtc false and some line id initializes with Cti', angular.mock.inject(function(CtiProxy) {
    CtiProxy._testProcessLineCfg({ webRtc: false, hasDevice: true, id: '12' });
    expect(CtiProxy.isUsingCti()).toBe(true);
    expect(webRtcAudio.disable).toHaveBeenCalled();
    verifyProxyCalls(CtiProxy, ctiMock, ctiMock);
  }));

  it('processLineCfg on line without device and with id '-' refuses to init on UCAssistant', angular.mock.inject(function(CtiProxy) {
    CtiProxy._testProcessLineCfg({ webRtc: false, hasDevice: false, id: '-' });
    expect($rootScope.$broadcast).toHaveBeenCalledWith(CtiProxy.FatalError, CtiProxy.FatalErrors.MISSING_LINE);
  }));

  it('processLineCfg on line without device and with id ' - ' init on CCAgent', angular.mock.inject(function (CtiProxy) {
    spyOn(remoteConfiguration, 'isAgent').and.returnValue(true);
    CtiProxy._testProcessLineCfg({ webRtc: false, hasDevice: false, id: '-' });
    expect($rootScope.$broadcast).not.toHaveBeenCalledWith(CtiProxy.FatalError, CtiProxy.FatalErrors.MISSING_LINE);
  }));

  it('processLineCfg on line without device but without SSL refuses to init and open popup', angular.mock.inject(function (CtiProxy, $rootScope, errorModal) {
    CtiProxy._setProtocolForTest('http:');
    CtiProxy._testProcessLineCfg({ hasDevice: false, webRtc: true, id: '23' });
    expect(CtiProxy.isUsingWebRtc()).toBe(false);
    expect($rootScope.$broadcast).toHaveBeenCalledWith(CtiProxy.FatalError, CtiProxy.FatalErrors.WEBRTC_REQUIRES_SSL);
    expect(webRtcAudio.disable).toHaveBeenCalled();
    expect(errorModal.showErrorModal).toHaveBeenCalled();
  }));

  it('when switched to webRtc, processLineCfg on line with device does switch to Cti', angular.mock.inject(function(CtiProxy) {
    CtiProxy._testProcessLineCfg({ hasDevice: false, webRtc: true, id: '23' });
    expect(CtiProxy.isUsingWebRtc()).toBe(true);
    verifyProxyCalls(CtiProxy, xcWebrtcMock, ctiMock);
    CtiProxy._testProcessLineCfg({ hasDevice: true });
    $rootScope.$digest();
    expect(CtiProxy.isUsingCti()).toBe(true);
    verifyProxyCalls(CtiProxy, ctiMock, ctiMock);
  }));

  it('when using WebRtc, it sends dtmf', angular.mock.inject(function(CtiProxy) {
    CtiProxy._testProcessLineCfg({ hasDevice: false, webRtc: true, id: '23' });
    CtiProxy.dtmf('#');
    expect(xcWebrtcMock.dtmf).toHaveBeenCalledWith('#');
  }));

  it('when using Cti, it does not send dtmf', angular.mock.inject(function(CtiProxy) {
    CtiProxy._testProcessLineCfg({ hasDevice: true });
    CtiProxy.dtmf('#');
    expect(xcWebrtcMock.dtmf).not.toHaveBeenCalled();
  }));

  it('when using Cti and stopUsingWebRtc() is called, it continues using Cti', angular.mock.inject(function(CtiProxy) {
    CtiProxy._testProcessLineCfg({ hasDevice: true });
    expect(CtiProxy.isUsingCti()).toBe(true);
    CtiProxy.stopUsingWebRtc();
    expect(xcWebrtcMock.stop).not.toHaveBeenCalledWith();
    expect(webRtcAudio.disable).toHaveBeenCalled();
    expect(CtiProxy.isUsingCti()).toBe(true);
  }));

  it('when using WebRtc and stopUsingWebRtc() is called, it switch to Cti', angular.mock.inject(function(CtiProxy) {
    CtiProxy._testProcessLineCfg({ hasDevice: false, webRtc: true, id: '23' });
    expect(CtiProxy.isUsingWebRtc()).toBe(true);
    CtiProxy.stopUsingWebRtc();
    expect(xcWebrtcMock.stop).toHaveBeenCalledWith();
    expect(webRtcAudio.disable).toHaveBeenCalled();
    expect(CtiProxy.isUsingCti()).toBe(true);
  }));

  function expectCtiDialAllowed(CtiProxy, _phoneCalls) {
    phoneCalls = _phoneCalls;
    CtiProxy._testProcessLineCfg({ hasDevice: true });
    CtiProxy.dial('12345', {foo: "bar"});
    expect(ctiMock.dial).toHaveBeenCalledWith('12345', {foo: "bar"}, false);
  }

  it('allow dial when there are no ongoing calls and not using WebRtc', angular.mock.inject(function(CtiProxy) {
    expectCtiDialAllowed(CtiProxy, []);
  }));

  it('allow dial when there are one ongoing calls and not using WebRtc', angular.mock.inject(function(CtiProxy) {
    expectCtiDialAllowed(CtiProxy, [{}]);
  }));

  it('allow dial when there are two ongoing calls and not using WebRtc', angular.mock.inject(function(CtiProxy) {
    expectCtiDialAllowed(CtiProxy, [{}, {}]);
  }));

  function testDialWebRtc(CtiProxy, _phoneCalls, video) {
    phoneCalls = _phoneCalls;
    CtiProxy._testProcessLineCfg({ hasDevice: false, webRtc: true });
    CtiProxy.dial('12345', {foo: "bar"}, typeof video === 'undefined' ? false : video);
  }

  it('allow dial when there is no ongoing call and using WebRtc', angular.mock.inject(function(CtiProxy) {
    testDialWebRtc(CtiProxy, []);
    expect(xcWebrtcMock.dial).toHaveBeenCalledWith('12345', false);
  }));

  it('allow dial when there is one ongoing call and using WebRtc', angular.mock.inject(function(CtiProxy) {
    testDialWebRtc(CtiProxy, [{}]);
    expect(xcWebrtcMock.dial).toHaveBeenCalledWith('12345', false);
  }));

  it('propagates video call parameter when using WebRTC', angular.mock.inject(function(CtiProxy) {
    testDialWebRtc(CtiProxy, [{}], true);
    expect(xcWebrtcMock.dial).toHaveBeenCalledWith('12345', true);
  }));

  it('refuse dial when there are two ongoing calls and using WebRtc', angular.mock.inject(function(CtiProxy) {
    testDialWebRtc(CtiProxy, [{}, {}]);
    expect(xcWebrtcMock.dial).not.toHaveBeenCalled();
  }));

  it('ask for line config on updateLine', angular.mock.inject(function(CtiProxy) {
    ctiMock.getConfig = jasmine.createSpy();
    CtiProxy.updateLine();
    expect(ctiMock.getConfig).toHaveBeenCalledWith('line');
  }));

  it('returns 1 as answerable calls for Cti and 2 for WebRTC', angular.mock.inject(function(CtiProxy) {
    expect(CtiProxy.getMaxAnswerableCalls()).toBe(1);
    switchToWebRTC(CtiProxy);
    expect(CtiProxy.getMaxAnswerableCalls()).toBe(2);
  }));

  it('forward conferenceMuteMe to Cti when using device', angular.mock.inject(function(CtiProxy) {
    CtiProxy._testProcessLineCfg({ hasDevice: true });
    ctiMock.conferenceMuteMe = function() {};
    spyOn(ctiMock, 'conferenceMuteMe');
    CtiProxy.conferenceMuteMe('4000');
    expect(ctiMock.conferenceMuteMe).toHaveBeenCalledWith('4000');
  }));

  it('forward conferenceUnmuteMe to Cti when using device', angular.mock.inject(function(CtiProxy) {
    CtiProxy._testProcessLineCfg({ hasDevice: true });
    ctiMock.conferenceUnmuteMe = function() {};
    spyOn(ctiMock, 'conferenceUnmuteMe');
    CtiProxy.conferenceUnmuteMe('4000');
    expect(ctiMock.conferenceUnmuteMe).toHaveBeenCalledWith('4000');
  }));

  it('forward conferenceMuteAll to Cti when using device', angular.mock.inject(function(CtiProxy) {
    CtiProxy._testProcessLineCfg({ hasDevice: true });
    ctiMock.conferenceMuteAll = function() {};
    spyOn(ctiMock, 'conferenceMuteAll');
    CtiProxy.conferenceMuteAll('4000');
    expect(ctiMock.conferenceMuteAll).toHaveBeenCalledWith('4000');
  }));

  it('forward conferenceUnmuteAll to Cti when using device', angular.mock.inject(function(CtiProxy) {
    CtiProxy._testProcessLineCfg({ hasDevice: true });
    ctiMock.conferenceUnmuteAll = function() {};
    spyOn(ctiMock, 'conferenceUnmuteAll');
    CtiProxy.conferenceUnmuteAll('4000');
    expect(ctiMock.conferenceUnmuteAll).toHaveBeenCalledWith('4000');
  }));

  it('forward conferenceMute to Cti when using device', angular.mock.inject(function(CtiProxy) {
    CtiProxy._testProcessLineCfg({ hasDevice: true });
    ctiMock.conferenceMute = function() {};
    spyOn(ctiMock, 'conferenceMute');
    CtiProxy.conferenceMute('4000', 1);
    expect(ctiMock.conferenceMute).toHaveBeenCalledWith('4000', 1);
  }));

  it('forward conferenceUnmute to Cti when using device', angular.mock.inject(function(CtiProxy) {
    CtiProxy._testProcessLineCfg({ hasDevice: true });
    ctiMock.conferenceUnmute = function() {};
    spyOn(ctiMock, 'conferenceUnmute');
    CtiProxy.conferenceUnmute('4000', 1);
    expect(ctiMock.conferenceUnmute).toHaveBeenCalledWith('4000', 1);
  }));

  it('forward conferenceKick to Cti when using device', angular.mock.inject(function(CtiProxy) {
    CtiProxy._testProcessLineCfg({ hasDevice: true });
    ctiMock.conferenceKick = function() {};
    spyOn(ctiMock, 'conferenceKick');
    CtiProxy.conferenceKick('4000', 1);
    expect(ctiMock.conferenceKick).toHaveBeenCalledWith('4000', 1);
  }));

  it('forward conferenceMuteMe to Cti when using webrtc', angular.mock.inject(function(CtiProxy) {
    CtiProxy._testProcessLineCfg({ hasDevice: false, webRtc: true, id: '23' });
    ctiMock.conferenceMuteMe = function() {};
    spyOn(ctiMock, 'conferenceMuteMe');
    CtiProxy.conferenceMuteMe('4000');
    expect(ctiMock.conferenceMuteMe).toHaveBeenCalledWith('4000');
  }));

  it('forward conferenceUnmuteMe to Cti when using webrtc', angular.mock.inject(function(CtiProxy) {
    CtiProxy._testProcessLineCfg({ hasDevice: false, webRtc: true, id: '23' });
    ctiMock.conferenceUnmuteMe = function() {};
    spyOn(ctiMock, 'conferenceUnmuteMe');
    CtiProxy.conferenceUnmuteMe('4000');
    expect(ctiMock.conferenceUnmuteMe).toHaveBeenCalledWith('4000');
  }));

  it('forward conferenceMuteAll to Cti when using webrtc', angular.mock.inject(function(CtiProxy) {
    CtiProxy._testProcessLineCfg({ hasDevice: false, webRtc: true, id: '23' });
    ctiMock.conferenceMuteAll = function() {};
    spyOn(ctiMock, 'conferenceMuteAll');
    CtiProxy.conferenceMuteAll('4000');
    expect(ctiMock.conferenceMuteAll).toHaveBeenCalledWith('4000');
  }));

  it('forward conferenceUnmuteAll to Cti when using webrtc', angular.mock.inject(function(CtiProxy) {
    CtiProxy._testProcessLineCfg({ hasDevice: false, webRtc: true, id: '23' });
    ctiMock.conferenceUnmuteAll = function() {};
    spyOn(ctiMock, 'conferenceUnmuteAll');
    CtiProxy.conferenceUnmuteAll('4000');
    expect(ctiMock.conferenceUnmuteAll).toHaveBeenCalledWith('4000');
  }));

  it('forward conferenceMute to Cti when using webrtc', angular.mock.inject(function(CtiProxy) {
    CtiProxy._testProcessLineCfg({ hasDevice: false, webRtc: true, id: '23' });
    ctiMock.conferenceMute = function() {};
    spyOn(ctiMock, 'conferenceMute');
    CtiProxy.conferenceMute('4000', 1);
    expect(ctiMock.conferenceMute).toHaveBeenCalledWith('4000', 1);
  }));

  it('forward conferenceUnmute to Cti when using webrtc', angular.mock.inject(function(CtiProxy) {
    CtiProxy._testProcessLineCfg({ hasDevice: false, webRtc: true, id: '23' });
    ctiMock.conferenceUnmute = function() {};
    spyOn(ctiMock, 'conferenceUnmute');
    CtiProxy.conferenceUnmute('4000', 1);
    expect(ctiMock.conferenceUnmute).toHaveBeenCalledWith('4000', 1);
  }));

  it('forward conferenceKick to Cti when using webrtc', angular.mock.inject(function(CtiProxy) {
    CtiProxy._testProcessLineCfg({ hasDevice: false, webRtc: true, id: '23' });
    ctiMock.conferenceKick = function() {};
    spyOn(ctiMock, 'conferenceKick');
    CtiProxy.conferenceKick('4000', 1);
    expect(ctiMock.conferenceKick).toHaveBeenCalledWith('4000', 1);
  }));

  function switchToWebRTC(CtiProxy) {
    CtiProxy._testProcessLineCfg({ hasDevice: false, webRtc: true, id: '23' });
    expect(CtiProxy.isUsingWebRtc()).toBe(true);
  }
});
