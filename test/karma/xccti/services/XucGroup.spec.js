'use strict';
describe('Xuc service group', function() {
  var xucGroup;
  var xucAgent;
  var $rootScope;

  beforeEach(function() {
    jasmine.addMatchers({
      toEqualData : customMatchers.toEqualData
    });
  });

  beforeEach(angular.mock.module('xcCti'));

  beforeEach(angular.mock.inject(function(_XucGroup_,_XucAgent_,_$rootScope_) {
    xucGroup = _XucGroup_;
    xucAgent = _XucAgent_;
    $rootScope = _$rootScope_;
    spyOn($rootScope, '$broadcast');
  }));

  it('should have empty groups at init', function() {

    expect(xucGroup.getGroups()).toEqualData([]);
  });

  it('should init agent groups on agent group list received and broadcast groups loaded', function() {
    var agentGroupList = [{'id':8, 'name' : 'one'},{'id':17, 'name' : 'two'}];
    xucGroup.onAgentGroupList(agentGroupList);

    expect(xucGroup.getGroups().length).toBe(2);
    expect($rootScope.$broadcast).toHaveBeenCalledWith('GroupsLoaded');
  });
  it('should use group name as display name when display name doest not exists', function() {
    var agentGroupList = [{'id':8, 'name' : 'one'},{'id':17, 'name' : 'two', 'displayName' : 'Group Two'}];
    xucGroup.onAgentGroupList(agentGroupList);

    expect(xucGroup.getGroups()[0].displayName).toBe('one');
  });

  it('should request groups from server on start', function() {
    spyOn(Cti,'getList');

    xucGroup.start();

    expect(Cti.getList).toHaveBeenCalledWith("agentgroup");

  });

  it('should be able to be reloaded', function(){
    spyOn(Cti,'getList');

    xucGroup.reload();

    expect(Cti.getList).toHaveBeenCalledWith("agentgroup");
  });

  it('should send back an agent group on id', function(){
    var agentGroupList = [{'id':8, 'name' : 'one'},{'id':17, 'name' : 'two'}];
    xucGroup.onAgentGroupList(agentGroupList);

    expect(xucGroup.getGroup(17)).toEqualData({'id':17, 'name' : 'two', 'displayName' : 'two'});

  });

  it('should return a group by id asynchronously', function(){
    var agentGroupList = [{'id':8, 'name' : 'one'},{'id':17, 'name' : 'two'}];
    xucGroup.onAgentGroupList(agentGroupList);

    var cb = {getGroupCb: function() {}};
    spyOn(cb, 'getGroupCb');

    xucGroup.getGroupAsync(8).then(cb.getGroupCb);
    $rootScope.$digest();
    expect(cb.getGroupCb).toHaveBeenCalledWith(agentGroupList[0]);
  });


  it('should send request move group of agents to xuc server', function() {
    spyOn(Cti,'moveAgentsInGroup');
    xucGroup.moveAgentsInGroup(54,523,4,895,8);

    expect(Cti.moveAgentsInGroup).toHaveBeenCalledWith(54,523,4,895,8);
  });
  it('should send request add group of agents to xuc server', function() {
    spyOn(Cti,'addAgentsInGroup');
    xucGroup.addAgentsInGroup(44,423,3,795,7);

    expect(Cti.addAgentsInGroup).toHaveBeenCalledWith(44,423,3,795,7);
  });

  it('should send request remove agents from queue group to xuc server', function(){
    var groupId=27, queueId = 234, penalty = 1;
    spyOn(Cti,'removeAgentGroupFromQueueGroup');

    xucGroup.removeAgentGroupFromQueueGroup(groupId, queueId, penalty);

    expect(Cti.removeAgentGroupFromQueueGroup).toHaveBeenCalledWith(groupId, queueId, penalty);
  });

  it('should send request add agent not in queue from group to queue penalty to xuc server', function(){
    var groupId=98, queueId = 543, penalty = 8;
    spyOn(Cti,'addAgentsNotInQueueFromGroupTo');

    xucGroup.addAgentsNotInQueueFromGroupTo(groupId, queueId, penalty);

    expect(Cti.addAgentsNotInQueueFromGroupTo).toHaveBeenCalledWith(groupId, queueId, penalty);
  });
  it('should be able to get groups with agents available to be added in a queue penalty', function() {
    var g1 = new MockGroupBuilder(8,'group one').build();
    var g2 = new MockGroupBuilder(7,'group two').build();
    var g3 = new MockGroupBuilder(6,'group three').build();

    xucGroup.onAgentGroupList([g1,g2,g3]);
    var queueId = 50, penalty=2;

    var noemie = new MockAgentBuilder(7,'Noemie','Ange').inGroup(8).build();
    var jean = new MockAgentBuilder(9,'Jean','Bonne').inGroup(7).build();
    var kem = new MockAgentBuilder(9,'Kem','Chown').inGroup(7).build();

    spyOn(xucAgent,'getAgentsNotInQueue').and.returnValue([jean,noemie,kem]);

    var group = xucGroup.getAvailableGroups(queueId, penalty);

    expect(group).toContain(g1);
    expect(group).toContain(g2);
    expect(group).not.toContain(g3);
    expect(group.length).toBe(2);
  });
});
