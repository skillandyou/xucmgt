'use strict';
describe('Xuc service membership', function() {
  var $rootScope;
  var $timeout;
  var xucMembership;


  var failTest = function(error) {
    expect(error).toBeUndefined();
  };

  beforeEach(function() {
    jasmine.addMatchers({
      toEqualData : customMatchers.toEqualData
    });
  });

  beforeEach(angular.mock.module('xcCti'));

  beforeEach(angular.mock.inject(function(_$rootScope_,_XucMembership_, _$timeout_) {
    $rootScope = _$rootScope_;
    xucMembership = _XucMembership_;
    $timeout = _$timeout_;
    spyOn($rootScope, '$broadcast');

  }));

  it('should init Membership object', function() {
    spyOn(Membership, 'init');

    xucMembership.init();

    expect(Membership.init).toHaveBeenCalledWith(Cti);
  });

  it('should set a user membership', function() {
    spyOn(Membership, 'setUserDefaultMembership');
    var userId = 1234;
    var membership = [{"queueId": 5, "penalty": 1}, {"queueId": 2, "penalty": 3}];

    xucMembership.init();

    xucMembership.setUserDefaultMembership(userId, membership);

    expect(Membership.setUserDefaultMembership).toHaveBeenCalledWith(userId, membership);
  });

  it('should set membership for a set of users', function() {
    spyOn(Membership, 'setUsersDefaultMembership');
    var userIds = [1, 2, 3, 4];
    var membership = [{"queueId": 5, "penalty": 1}, {"queueId": 2, "penalty": 3}];

    xucMembership.init();

    xucMembership.setUsersDefaultMembership(userIds, membership);

    expect(Membership.setUsersDefaultMembership).toHaveBeenCalledWith(userIds, membership);
  });

  it('should get a user membership', function(done) {
    spyOn(Membership, 'getUserDefaultMembership').and.callThrough();
    spyOn(Cti, 'sendCallback');

    var userId = 1234;
    var membership = [{"queueId":1,"penalty":1},{"queueId":2,"penalty":8}];
    var userMembership = {"userId":userId,"membership": membership};
    var eventData = {"msgType":"UserQueueDefaultMembership","ctiMessage":userMembership};

    xucMembership.init();

    var p = xucMembership.getUserDefaultMembership(userId);

    p.then(function(received) {
      expect(received).toEqual(membership);
      done();
    }, failTest);

    Cti.Topic(eventData.msgType).publish(eventData.ctiMessage);

    $rootScope.$digest();

    expect(Membership.getUserDefaultMembership).toHaveBeenCalledWith(userId);
  });

  it('should fail getting a user membership when timeout', function(done) {
    spyOn(Membership, 'getUserDefaultMembership').and.callThrough();
    spyOn(Cti, 'sendCallback');

    var userId = 1234;


    xucMembership.init();
    xucMembership.setTimeoutMs(100);
    var p = xucMembership.getUserDefaultMembership(userId);

    p.then(failTest, function(e) {
      expect(e.error).toEqual("timeout");
      done();
    });

    $timeout.flush();

    expect(Membership.getUserDefaultMembership).toHaveBeenCalledWith(userId);
  });

  it('should apply default membership to a list of users', function() {
    spyOn(Membership, 'applyUsersDefaultMembership');
    var userIds = [1,2,3,4];

    xucMembership.init();

    xucMembership.applyUsersDefaultMembership(userIds);

    expect(Membership.applyUsersDefaultMembership).toHaveBeenCalledWith(userIds);
  });
});
