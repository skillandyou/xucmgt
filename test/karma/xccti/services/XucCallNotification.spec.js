'use strict';

describe('XucCallNotification', function() {
  var XucCallNotification;
  var webNotification;
  var onHoldNotifier;
  var remoteConfiguration;
  var $rootScope;
  var $q;

  var holdTimeDefer;

  beforeEach(angular.mock.module('html-templates'));
  beforeEach(angular.mock.module('karma-backend'));
  beforeEach(angular.mock.module('xcHelper'));

  beforeEach(function() {
    jasmine.addMatchers({
      toEqualData : customMatchers.toEqualData
    });
  });


  function getEvent(eventType) {
    return {
      "eventType": eventType,
      "DN":"1000",
      "otherDN":"1001",
      "linkedId":"1471858488.233",
      "uniqueId":"1471858490.236",
      "userData": {}
    };
  }

  beforeEach(angular.mock.module('xcCti'));

  beforeEach(angular.mock.inject(function(_XucCallNotification_, _$rootScope_, _$q_, _webNotification_, _onHoldNotifier_, _remoteConfiguration_) {
    XucCallNotification = _XucCallNotification_;
    webNotification = _webNotification_;
    onHoldNotifier = _onHoldNotifier_;
    remoteConfiguration = _remoteConfiguration_;
    $rootScope = _$rootScope_;
    $q = _$q_;
    spyOn(Cti, 'sendCallback');
    holdTimeDefer = $q.defer();
    spyOn(remoteConfiguration, 'getIntOrElse').and.returnValue(holdTimeDefer.promise);

    Cti.Topic(Cti.MessageType.PHONEEVENT).clear();
    // Initialize XucLink
    Cti.Topic(Cti.MessageType.LOGGEDON).publish("");
    $rootScope.$digest();
  }));


  afterEach(function () {
    $rootScope.$digest();
  });

  it('check for permission when enabled', function() {    
    spyOn(window.Notification, "requestPermission");
    XucCallNotification.enableNotification();
    expect(window.Notification.requestPermission).toHaveBeenCalled();
    expect(XucCallNotification.isNotificationEnabled()).toEqual(true);
  });

  it('display desktop notification when call rings', function() {
    spyOn(webNotification, "showNotification");
    XucCallNotification.enableNotification();
    Cti.Topic(Cti.MessageType.PHONEEVENT).publish(getEvent('EventRinging'));
    $rootScope.$digest();
    expect(webNotification.showNotification).toHaveBeenCalled();
  });

  it('display desktop notification when call is on hold for a long time', function() {
    XucCallNotification.enableNotification();
    holdTimeDefer.resolve(1);
    $rootScope.$digest();
    spyOn(onHoldNotifier, 'notify').and.callFake(() => {
      var defer = $q.defer();
      defer.resolve({});
      return defer.promise;
    });

    Cti.Topic(Cti.MessageType.PHONEEVENT).publish(getEvent('EventOnHold'));
    $rootScope.$digest();
    expect(onHoldNotifier.notify).toHaveBeenCalled();
  });

  it('hide any desktop notification when call answered', function() {
    var cb = {hide: function() {}};
    spyOn(cb, "hide");
    spyOn(webNotification, "showNotification").and.callFake(function(title, options, callback) {
      callback(false, cb.hide);
    });
    XucCallNotification.enableNotification();
    Cti.Topic(Cti.MessageType.PHONEEVENT).publish(getEvent('EventRinging'));
    $rootScope.$digest();
    Cti.Topic(Cti.MessageType.PHONEEVENT).publish(getEvent('EventEstablished'));
    $rootScope.$digest();
    expect(cb.hide).toHaveBeenCalled();
  });

  it('hide any desktop notification when call released', function() {
    var cb = {hide: function() {}};
    spyOn(cb, "hide");
    spyOn(webNotification, "showNotification").and.callFake(function(title, options, callback) {
      callback(false, cb.hide);
    });
    XucCallNotification.enableNotification();
    Cti.Topic(Cti.MessageType.PHONEEVENT).publish(getEvent('EventRinging'));
    $rootScope.$digest();
    Cti.Topic(Cti.MessageType.PHONEEVENT).publish(getEvent('EventReleased'));
    $rootScope.$digest();
    expect(cb.hide).toHaveBeenCalled();
  });

  it('hide any desktop notification when disabling notifications', function() {
    var cb = {hide: function() {}};
    spyOn(cb, "hide");
    spyOn(webNotification, "showNotification").and.callFake(function(title, options, callback) {
      callback(false, cb.hide);
    });
    XucCallNotification.enableNotification();
    Cti.Topic(Cti.MessageType.PHONEEVENT).publish(getEvent('EventRinging'));
    $rootScope.$digest();
    XucCallNotification.disableNotification();
    expect(cb.hide).toHaveBeenCalled();
  });

  it('display notification when new flashtext is received', function(){
    spyOn(webNotification, "showNotification");
    XucCallNotification.enableNotification();
    XucCallNotification.onFlashTextReceived({message: "myMessage"});
    $rootScope.$digest();
    expect(webNotification.showNotification).toHaveBeenCalled();
  });

});
