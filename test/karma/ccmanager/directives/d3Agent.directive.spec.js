'use strict';

describe('Directive: d3 agent', function () {
  var $compile, $rootScope, preferences;

  var compactView = false;

  beforeEach(angular.mock.module('karma-backend'));
  beforeEach(angular.mock.module('html-templates'));

  beforeEach(angular.mock.module('ccManager'));

  beforeEach(angular.mock.inject(function (_$compile_, _$rootScope_, Preferences, $httpBackend) {
    var loginPage = '<login-form on-login="onLogin()" host-and-port="hostAndPort" error-code="error" use-sso="useSso" title="Login"/>';

    $httpBackend.when('GET', '/ccmanager/login').respond(loginPage);
    
    $compile = _$compile_;
    $rootScope = _$rootScope_;
    preferences = Preferences;
    spyOn(preferences, 'getBoolOption').and.callFake(function () {
      return compactView;
    });
  }));

  it('should render the agent on normal view with full height', function () {
    compactView = false;
    $rootScope.agent = { firstName: "John", lastName: "TheBlue", state: "AgentOnCall" };

    var element = $compile('<d3-agent data="agent"></d3-agent>')($rootScope);

    $rootScope.$digest();

    expect(element.find('svg').attr('height')).toBe('50');

  });

  it('should render the agent on compact view with small height', function () {
    compactView = true;
    $rootScope.agent = { firstName: "John", lastName: "TheBlue", state: "AgentOnCall" };

    var element = $compile('<d3-agent data="agent"></d3-agent>')($rootScope);

    $rootScope.$digest();

    expect(element.find('svg').attr('height')).toBe('20');

  });

});
