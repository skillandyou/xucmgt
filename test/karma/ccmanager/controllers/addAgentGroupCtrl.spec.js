describe('add agent group controller', function() {
  var $scope;
  var ctrl;

  beforeEach(angular.mock.module('ccManager'));

  beforeEach(angular.mock.inject(function($controller, $rootScope) {
    $scope = $rootScope.$new();
    ctrl = $controller('addAgentGroupCtrl', {
      '$scope' : $scope
    });
  }));

  it('can instanciate controller', function() {
    expect(ctrl).not.toBeUndefined();
  });

});
