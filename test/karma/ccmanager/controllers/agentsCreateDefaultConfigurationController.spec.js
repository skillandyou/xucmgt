import _ from 'lodash';

describe('agents create default configuration controller', function() {
  var $scope;
  var $rootScope;
  var ctrl;
  var $uibModalInstance;
  var xucQueue;
  var xucMembership;
  var agents = [];


  beforeEach(angular.mock.module('ccManager'));

  beforeEach(angular.mock.inject(function($controller, _$rootScope_, _$uibModal_, _XucQueue_, _NgTableParams_, _XucTableHelper_, _XucMembership_) {
    $rootScope = _$rootScope_;
    $scope = $rootScope.$new();
    xucQueue = _XucQueue_;
    xucMembership = _XucMembership_;
    agents = [new MockAgentBuilder(10, 'James', 'Bond').build(), new MockAgentBuilder(12, 'Jason', 'Bourne').build()];


    $uibModalInstance = _$uibModal_.open({
      templateUrl: 'assets/javascripts/ccmanager/controllers/agentsCreateDefaultConfiguration.html'
    });

    ctrl = $controller('agentsCreateDefaultConfigurationController', {
      '$scope' : $scope,
      '$uibModalInstance' : $uibModalInstance,
      'xucQueue': xucQueue,
      'NgTableParams': _NgTableParams_,
      'XucTableHelper': _XucTableHelper_,
      'XucMembership': xucMembership,
      'selectedAgents' : agents,
      'selectedQueues' : []
    });
  }));

  it('should instanciate controller', function() {

    expect(ctrl).not.toBeUndefined();

  });

  it('should close popup when canceled', function() {
    spyOn($uibModalInstance, 'dismiss');

    $scope.cancel();
    expect($uibModalInstance.dismiss).toHaveBeenCalledWith('cancel');

  });

  it('should save membership and close popup when submited', function() {
    spyOn($uibModalInstance, 'dismiss');
    spyOn(xucMembership, 'setUsersDefaultMembership');
    var queue1 = new QueueBuilder('cars', 'Cars').build();
    var queue2 = new QueueBuilder('trucks', 'Trucks').build();
    var userIds = _.map(agents, 'userId');
    var membership = [{"queueId": queue1.id, "penalty": 1}, {"queueId": queue2.id, "penalty": 3}];

    $scope.addQueue(queue1, 1);
    $scope.addQueue(queue2, 3);

    $scope.ok();
    expect(xucMembership.setUsersDefaultMembership).toHaveBeenCalledWith(userIds, membership);
    expect($uibModalInstance.dismiss).toHaveBeenCalledWith('ok');


  });

});
