describe('view global controller', function() {
  var preferences;
  var xucQueue;
  var XucQueueRecording;
  var $scope;
  var ctrl;

  beforeEach(angular.mock.module('ccManager'));

  beforeEach(angular.mock.inject(function(_XucQueue_,_Preferences_, $controller, $rootScope, _XucQueueRecording_) {
    $scope = $rootScope.$new();
    xucQueue = _XucQueue_;
    preferences = _Preferences_;
    XucQueueRecording = _XucQueueRecording_;

    ctrl = $controller('ccViewGlobalController', {
      '$scope' : $scope,
      'XucQueue' : xucQueue,
      'Preferences' : preferences,
      'XucQueueRecording' : XucQueueRecording
    });
  }));

  it('can instantiate controller', function() {
    expect(ctrl).not.toBeUndefined();
  });
});
