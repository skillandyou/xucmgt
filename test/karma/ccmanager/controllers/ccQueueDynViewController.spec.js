describe('queue dyn view controllers', function() {
  var xucQueueTotal;
  var $scope;
  var ctrl;
  var $filter;
  var NgTableParams;
  var $rootScope;
  var viewColBuilder;
  var preferences;
  var thresholds;

  beforeEach(angular.mock.module('ccManager'));

  beforeEach(angular.mock.inject(function(_NgTableParams_, _$filter_, _XucQueueTotal_, $controller, _$rootScope_, _ViewColBuilder_, _Preferences_, _Thresholds_) {
    $scope = _$rootScope_.$new();
    $rootScope = _$rootScope_;
    xucQueueTotal = _XucQueueTotal_;
    $filter = _$filter_;
    NgTableParams = _NgTableParams_;
    viewColBuilder = _ViewColBuilder_;
    preferences = _Preferences_;
    thresholds = _Thresholds_;

    $scope.queues = [];

    var selectedColumns = [{id:'col1', show: false},{id:'col2', show: true}];
    spyOn(preferences, 'getViewColumns').and.returnValue(selectedColumns);

    ctrl = $controller('ccQueueDynViewController', {
      '$scope' : $scope,
      'NgTableParams': NgTableParams,
      '$filter': $filter,
      'xucQueueTotal': xucQueueTotal,
      'viewColBuilder': viewColBuilder,
      'preferences' : preferences,
      'thresholds' : thresholds
    });
  }));

  it('can instanciate controller', function() {
    expect(ctrl).not.toBeUndefined();
  });

  it('should get queue columns to be displayed from preferences', function() {

    expect(preferences.getViewColumns).toHaveBeenCalled();

  });

  it('should reload cols on preferences saved', function(){

    $rootScope.$broadcast('preferences.queueViewColumns');

    expect(preferences.getViewColumns.calls.count()).toEqual(2);
  });


  it('should calculate display class for a counter', function() {

    spyOn(thresholds, 'thresholdClass').and.returnValue('middleClass');

    var cl = $scope.thresholdClass('WaitingCalls',1);

    expect(cl).toBe('middleClass');

  });

  it('should calculate nothing for a unkown counter', function() {
    $scope.thresholds = {};
    $scope.thresholds['WaitingCalls'] = {low:7, high:11};

    var cl = $scope.thresholdClass('TalkingAgents',1);

    expect(cl).toBe('');

  });


});
