describe('qeueue select controllers', function() {
  var preferences;
  var xucQueue;
  var $scope;
  var ctrl;

  beforeEach(angular.mock.module('ccManager'));

  beforeEach(angular.mock.inject(function(_XucQueue_,_Preferences_, $controller, $rootScope) {
    $scope = $rootScope.$new();
    xucQueue = _XucQueue_;
    preferences = _Preferences_;
    ctrl = $controller('queueSelectCtrl', {
      '$scope' : $scope,
      'XucQueue' : xucQueue,
      'Preferences' : preferences
    });
  }));

  it('can instantiate controller', function() {
    expect(ctrl).not.toBeUndefined();
  });

  it('should select a queue', function(){
    spyOn(preferences, 'toggleQueueSelection');

    $scope.toggleQueue({id: 28});

    expect(preferences.toggleQueueSelection).toHaveBeenCalledWith(28);
  });

  it('should update selection based on data from the multiselect', function(){
    spyOn(preferences, 'isQueueSelected').and.returnValue(false);
    spyOn(preferences, 'toggleQueueSelection');
    $scope.queues = [ { id: 54, ticked: true } ];
    $scope.updateSelection();

    expect(preferences.toggleQueueSelection).toHaveBeenCalledWith(54);
  });

});
