describe('CCmanager queueSliderController', function() {
  var $rootScope;
  var $scope;
  var ctrl;
  var thresholds;

  beforeEach(angular.mock.module('ccManager'));

  beforeEach(function() {
    jasmine.addMatchers({
      toEqualData : customMatchers.toEqualData
    });
  });


  beforeEach(angular.mock.inject(function(_$rootScope_, $controller, _Thresholds_) {
    $rootScope =_$rootScope_;
    $scope = $rootScope.$new();
    thresholds = _Thresholds_;

    spyOn(thresholds, 'getThresholdDefs').and.returnValue(['Counter1','Counter2']);
    spyOn(thresholds, 'loadThreshold').and.returnValue({low:7,high:12});

    ctrl = $controller('queueSliderController', {
      '$scope' : $scope,
      '$rootScope' : $rootScope,
      'thresholds' : thresholds
    });
  }));

  it('can instanciate controller', function(){
    expect(ctrl).not.toBeUndefined();
  });

  it('should load counters thresholds on startup', function(){

    expect(thresholds.loadThreshold).toHaveBeenCalled();

    expect($scope.sliders.Counter1.values[0]).toBe(7);
    expect($scope.sliders.Counter1.values[1]).toBe(12);
    expect($scope.sliders.Counter1.min).toBe(0);
    expect($scope.sliders.Counter1.max).toBeGreaterThan(12);
    expect($scope.sliders.Counter1.options.range).toBe(true);
    expect($scope.sliders.Counter1.options.stop).toBeDefined();
  });

  it('should update max when high is upper', function(){

    $scope.sliders = { WaitingCalls : { min: 0, max: 40 }};
    $scope.updateMax('WaitingCalls',50);

    expect($scope.sliders.WaitingCalls.max).toBe(80);
  });

  it ('should save high and low in threshold on stop', function(){
    $scope.sliders = { WaitingCalls : { min: 0, max: 40 }};

    spyOn(thresholds, 'saveThreshold').and.returnValue(7,12);

    $scope.onStop('WaitingCalls',10,20);

    expect(thresholds.saveThreshold).toHaveBeenCalledWith('WaitingCalls',10,20);
  });


});
