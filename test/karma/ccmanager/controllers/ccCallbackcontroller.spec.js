import moment from 'moment';

describe('ccm cccallbackcontroller', function() {
  var xucCallback;
  var $rootScope;
  var $scope;
  var Preferences;
  var ctrl;
  var $window;

  beforeEach(angular.mock.module('karma-backend'));
  beforeEach(angular.mock.module('html-templates'));

  beforeEach(angular.mock.module('ccManager'));

  beforeEach(function() {
    jasmine.addMatchers({
      toEqualData : customMatchers.toEqualData
    });
  });

  beforeEach(angular.mock.inject(function(_XucCallback_, _$rootScope_, $controller, _Preferences_, _$window_) {
    $rootScope =_$rootScope_;
    $scope = $rootScope.$new();
    xucCallback = _XucCallback_;
    Preferences = _Preferences_;
    $window = _$window_;
    $window = {location: {protocol: 'https:'}};
    
    ctrl = $controller('ccCallbackController', {
      '$scope' : $scope,
      'XucCallback' : xucCallback,
      'Preferences' : Preferences,
      '$window': $window
    });
  }));

  it('can instantiate controller', function() {
    expect(ctrl).not.toBeUndefined();
  });

  it('should get callbacks for preferred queues on callbacks loaded', function() {
    var l1 = CallbackListBuilder('First List', 1, []).build();
    var l2 = CallbackListBuilder('Second List', 2, []).build();
    var l3 = CallbackListBuilder('Second List', 3, []).build();

    spyOn(Preferences, 'isQueueSelected').and.callFake(function(qid) {
      if(qid === 1 || qid === 2) return true;
      return false;
    });

    spyOn(xucCallback,'getCallbackLists').and.returnValue([l1, l2, l3]);

    $rootScope.$broadcast('CallbacksLoaded');

    expect(xucCallback.getCallbackLists).toHaveBeenCalled();
    expect($scope.callbackLists).toEqual([l1, l2]);
  });

  it('generates server URL', function() {
    expect($scope.getServerBaseUrl('192.168.56.101:8000')).toEqual('https://192.168.56.101');
  });

  it('should compute oldest callback', function() {
    const cb1 = CallbackRequestBuilder('1000').withDueDate(moment().add(1, "days").format("YYYY-MM-DD")).build();
    const cb2 = CallbackRequestBuilder('1001').withDueDate(moment().subtract(1, "days").format("YYYY-MM-DD")).build();
    const cb3 = CallbackRequestBuilder('1002').withDueDate(moment().format("YYYY-MM-DD")).build();

    const l1 = CallbackListBuilder('First List', 1, [cb1]).build();
    const l2 = CallbackListBuilder('Second List', 2, [cb2]).build();
    const l3 = CallbackListBuilder('Third List', 3, [cb3]).build();

    spyOn(Preferences, 'isQueueSelected').and.callFake(() => {
      return true;
    });
    spyOn(xucCallback,'getCallbackLists').and.returnValue([l1, l2, l3]);
    $rootScope.$broadcast('CallbacksLoaded');

    expect($scope.oldestCallback).toEqual(cb2);
  });
});
