describe('agent edit controller', function() {
  var $scope;
  var $rootScope;
  var xucAgent;
  var xucQueue;
  var xucGroup;
  var ctrl;
  var modal;
  var $q;


  beforeEach(function() {
    jasmine.addMatchers({
      toEqualData : customMatchers.toEqualData
    });
  });

  beforeEach(angular.mock.module('karma-backend'));
  beforeEach(angular.mock.module('html-templates'));

  beforeEach(angular.mock.module('ccManager'));

  beforeEach(angular.mock.inject(function(_XucAgent_,_XucQueue_, $controller, _$rootScope_, $uibModal, _$q_) {
    $rootScope = _$rootScope_;
    $scope = _$rootScope_.$new();
    xucAgent = _XucAgent_;
    xucQueue = _XucQueue_;
    modal = $uibModal;
    $q = _$q_;

    spyOn(modal, 'open').and.returnValue({result: $q.resolve({})});
    
    ctrl = $controller('agentEditController', {
      '$scope' : $scope,
      'XucAgent' : xucAgent,
      'XucQueue' : xucQueue,
      'XucGroup' : xucGroup,
      '$uibModal' : modal
    });
  }));


  it('can instanciate controller', function() {
    expect(ctrl).not.toBeUndefined();
  });

  it('loads agent on edit agent', function(){
    var agent = {};
    agent.id = 32;
    agent.firstName = 'john';
    agent.queueMembers = {};
    agent.queueMembers['1']= 7;
    agent.userId=42;


    spyOn(xucAgent, 'getAgent').and.callFake(function() {return agent;});

    $scope.editAgent(32);
    $rootScope.$apply();

    expect(modal.open).toHaveBeenCalled();
    expect(xucAgent.getAgent).toHaveBeenCalledWith(32);
    expect($scope.agent).toEqualData(agent);

  });
});
