describe('View controller', function () {
  var $rootScope;
  var $scope;
  var $state;
  var XucPhoneEventListener;
  var phoneEventHandler;
  var XucUtils;
  var XucFlashText;
  var ctrl;
  var electronWrapper;

  beforeEach(angular.mock.module('ucAssistant'));
  beforeEach(angular.mock.module('xcCti'));

  beforeEach(function () {
    jasmine.addMatchers({
      toEqualData: customMatchers.toEqualData
    });
  });

  beforeEach(angular.mock.inject(function (_XucUtils_, _$rootScope_, $controller, _$state_, _XucPhoneEventListener_, _XucFlashText_, _electronWrapper_) {
    $rootScope = _$rootScope_;
    $scope = $rootScope.$new();
    $state = _$state_;
    XucPhoneEventListener = _XucPhoneEventListener_;
    XucUtils = _XucUtils_;
    XucFlashText = _XucFlashText_;
    electronWrapper = _electronWrapper_;

    spyOn(XucPhoneEventListener, 'addHandler').and.callFake(function (passedScope, handler) {
      phoneEventHandler = handler;
    });

    spyOn(XucFlashText, 'subscribeToFlashText').and.callFake(() => {});

    ctrl = $controller('ViewController', {
      '$scope': $scope,
      '$rootScope': $rootScope,
      '$state': $state,
      'XucPhoneEventListener': XucPhoneEventListener,
      'XucUtils': XucUtils,
      'XucFlashText': XucFlashText

    });
  }));

  function buildFlashtext(msg) {
    return {
      event: "FlashTextUserMessage",
      message: msg
    };
  }

  it('display the badge when a new flashtext is received', function() {
    expect(ctrl.hasMessage).toBe(false);
    spyOn(XucFlashText, 'hasNewMessage').and.returnValue(true);
    $rootScope.$broadcast('FlashTextReceived', buildFlashtext('my message'));
    expect(ctrl.hasMessage).toBe(true);
  });

  it('does not display the badge when in conversation with the user who sent the new flashtext', function () {
    expect(ctrl.hasMessage).toBe(false);
    spyOn(XucFlashText, 'hasNewMessage').and.returnValue(false);
    $rootScope.$broadcast('FlashTextReceived', buildFlashtext('my message'));
    expect(ctrl.hasMessage).toBe(false);
  });

  it('can instantiate controller', function () {
    expect(ctrl).not.toBeUndefined();
  });

  it('triggers search if letters are inputted', () => {
    spyOn($state, 'go');
    var input = 'steve';
    $scope.destination = input;
    ctrl.dialOrSearch();
    expect($state.go).toHaveBeenCalledWith('interface.search', Object({ showFavorites: false, search: 'steve' }));
  });

  it('check if user pressed enter', () => {
    spyOn(ctrl, 'dialOrSearch');
    const escapeEvent = document.createEvent('CustomEvent');
    escapeEvent.which = 13;
    escapeEvent.initEvent('keypress', true, true);
    document.dispatchEvent(escapeEvent);
    ctrl.isEnterKey(escapeEvent);
    expect(ctrl.dialOrSearch).toHaveBeenCalled();
  });

  it('change button icon to a phone if number are inputted', () => {
    spyOn(XucUtils, 'isaPhoneNb').and.returnValue(true);

    ctrl.changeIcon({});
    expect($scope.isPhone).toBe(true);
  });

  it('change button icon to a search icon if letters are inputted', () => {
    spyOn(XucUtils, 'isaPhoneNb').and.returnValue(false);

    ctrl.changeIcon();
    expect($scope.isPhone).toBe(false);
  });

  it('clear input when user click on x', () => {
    $scope.destination = 'oof';
    ctrl.emptyInput();
    expect($scope.destination).toBe("");
  });

  it('Have missed icon in system tray if a FlashText is pending', () => {
    spyOn(XucFlashText, 'getFlashTexts').and.returnValue(true);
    spyOn(electronWrapper, 'setTrayIcon');
    spyOn(XucFlashText, 'hasNewMessage').and.returnValue(true);
    $rootScope.$broadcast('FlashTextReceived', buildFlashtext('boubou'));
    expect(electronWrapper.setTrayIcon).toHaveBeenCalledWith('missed');
  });

  it('Have missed icon in system tray if a call is missed', () => {
    ctrl.callHistory.push({
      details: [
        {
          status: 'missed',
          start: new Date()
        }
      ]
    });
    spyOn(electronWrapper, 'setTrayIcon');
    ctrl.updateMissedCalls();
    expect(electronWrapper.setTrayIcon).toHaveBeenCalledWith('missed');
  });

  it('Have missed icon in system tray if a call is missed and a FlashText is pending', () => {
    ctrl.missedCalls = 2;
    spyOn(XucFlashText, 'getFlashTexts').and.returnValue([]);
    spyOn(electronWrapper, 'setTrayIcon');
    spyOn(XucFlashText, 'hasNewMessage').and.returnValue(true);
    $rootScope.$broadcast('FlashTextReceived', buildFlashtext('boubou'));
    expect(electronWrapper.setTrayIcon).toHaveBeenCalledWith('missed');
  });

  it('Have default icon in system tray if no call missed and no FlashText pending', () => {
    spyOn(electronWrapper, 'setTrayIcon');
    ctrl.missedCalls = 0;
    ctrl.hasMessage = false;
    ctrl.updateElectronIcon();
    expect(electronWrapper.setTrayIcon).toHaveBeenCalledWith('default');
  });
});
