import moment from 'moment';

describe('CallControl controller', function() {
  var $rootScope;
  var $scope;
  var ctrl;
  var audioCall = {id: 1, mediaType: xc_webrtc.mediaType.AUDIO};
  var videoCall = {id: 1, mediaType: xc_webrtc.mediaType.AUDIOVIDEO};
  var CtiProxy;
  var lastIndex = 10;
  var $document;

  beforeEach(angular.mock.module('html-templates'));
  beforeEach(angular.mock.module('karma-backend'));
  beforeEach(angular.mock.module('xcCti'));
  beforeEach(angular.mock.module('xcHelper'));
  beforeEach(angular.mock.module('ucAssistant'));

  function nextIndex() {
    return ++lastIndex;
  }

  function createParticipant(number, isTalking = false, role = 'User', isMuted = false, isMe = false, index = nextIndex()) {
    return {
      index: index,
      callerIdName: 'Number ' + number,
      callerIdNum: number,
      isTalking: isTalking,
      role: role,
      isMuted: isMuted,
      isMe: isMe
    };
  }

  function createConference(participants, currentUserRole = 'User', number = "4000") {
    return {
      conferenceName: "Conference " + number,
      conferenceNumber: number,
      participants: participants,
      since: 0,
      currentUserRole: currentUserRole,
      startTime: moment().toDate()
    };
  }

  beforeEach(function() {
    jasmine.addMatchers({
      toEqualData : customMatchers.toEqualData
    });
  });

  beforeEach(angular.mock.inject(function(_$rootScope_, $controller, _CtiProxy_, _$document_) {
    $rootScope =_$rootScope_;
    $document = _$document_;
    $scope = $rootScope.$new();
    CtiProxy = _CtiProxy_;
    ctrl = $controller('CallControlController', {
      '$scope' :                  $scope,
      '$rootScope' :              $rootScope,
      'CtiProxy': CtiProxy
    });
  }));

  it('can instantiate controller', function(){
    expect(ctrl).not.toBeUndefined();
  });

  it('says if a call is a video call', function() {
    expect($scope.isVideo(audioCall)).toBe(false);
    expect($scope.isVideo(videoCall)).toBe(true);
  });

  it('says if one of calls is a video call', function() {
    expect($scope.isAnyCallVideo([audioCall])).toBe(false);
    expect($scope.isAnyCallVideo([audioCall, videoCall])).toBe(true);
    expect($scope.isAnyCallVideo([videoCall])).toBe(true);
  });

  it('forward conference muteme action to CtiProxy', function() {
    spyOn(CtiProxy, 'conferenceMuteMe');
    $scope.conferenceMuteMe('4000');
    expect(CtiProxy.conferenceMuteMe).toHaveBeenCalledWith('4000');
  });

  it('forward conference unmuteme action to CtiProxy', function() {
    spyOn(CtiProxy, 'conferenceUnmuteMe');
    $scope.conferenceUnmuteMe('4000');
    expect(CtiProxy.conferenceUnmuteMe).toHaveBeenCalledWith('4000');
  });

  it('forward conference mute all action to CtiProxy', function() {
    spyOn(CtiProxy, 'conferenceMuteAll');
    $scope.conferenceMuteAll('4000');
    expect(CtiProxy.conferenceMuteAll).toHaveBeenCalledWith('4000');
  });

  it('forward conference unmute all action to CtiProxy', function() {
    spyOn(CtiProxy, 'conferenceUnmuteAll');
    $scope.conferenceUnmuteAll('4000');
    expect(CtiProxy.conferenceUnmuteAll).toHaveBeenCalledWith('4000');
  });

  it('forward conference mute action to CtiProxy', function() {
    spyOn(CtiProxy, 'conferenceMute');
    $scope.conferenceMute('4000', 1);
    expect(CtiProxy.conferenceMute).toHaveBeenCalledWith('4000', 1);
  });

  it('forward conference unmute action to CtiProxy', function() {
    spyOn(CtiProxy, 'conferenceUnmute');
    $scope.conferenceUnmute('4000', 1);
    expect(CtiProxy.conferenceUnmute).toHaveBeenCalledWith('4000', 1);
  });

  it('should allow to mute all when someone in the conference (other from the organizer) is not muted', function() {
    var participants = [
      createParticipant('1000', false, 'Organizer', false, true),
      createParticipant('1001'),
      createParticipant('1002')
    ];
    var conference = createConference(participants, 'Organizer');
    
    expect($scope.canMuteAll(conference)).toEqual(true);
  });

  it('should not allow to mute all when not an organizer', function() {
    var participants = [
      createParticipant('1000', false, 'User', false, true),
      createParticipant('1001'),
      createParticipant('1002')
    ];
    var conference = createConference(participants, 'User');
    
    expect($scope.canMuteAll(conference)).toEqual(false);
  });

  it('should allow to unmute all when everyone in the conference (other than self) is muted', function() {
    var participants = [
      createParticipant('1000', false, 'Organizer', false, true),
      createParticipant('1001', false, 'User', true),
      createParticipant('1002', false, 'User', true)
    ];
    var conference = createConference(participants, 'Organizer');
    
    expect($scope.canUnmuteAll(conference)).toEqual(true);
  });

  it('should not allow to unmute all when not an organizer', function() {
    var participants = [
      createParticipant('1000', false, 'User', false, true),
      createParticipant('1001'),
      createParticipant('1002')
    ];
    var conference = createConference(participants, 'User');
    
    expect($scope.canUnmuteAll(conference)).toEqual(false);
  });

  it('should not allow to unmute all when someone (other than me) is not muted', function() {
    var participants = [
      createParticipant('1000', false, 'Organizer', false, true),
      createParticipant('1001', false, 'User', true),
      createParticipant('1002', false, 'User', false)
    ];    
    var conference = createConference(participants, 'Organizer');

    expect($scope.canUnmuteAll(conference)).toEqual(false);
  });

  it('forward conference kick action to CtiProxy', function() {
    var participant = createParticipant('1001', false, 'User', false, false, 1);
    spyOn(CtiProxy, 'conferenceKick');
    $scope.conferenceKick('4000', participant);
    expect(CtiProxy.conferenceKick).toHaveBeenCalledWith('4000', 1);
  });

  it('set kicked property to true when kicking out a participant', function() {
    var participant = createParticipant('1001', false, 'User', false, false, 1);
    spyOn(CtiProxy, 'conferenceKick');
    $scope.conferenceKick('4000', participant);
    expect(participant.kicked).toBeTruthy();
  });

  it('should display participant action for myself if not organizer', function() {
    var participant = createParticipant('1001', false, 'User', false, true, 1);
    var conference = createConference([participant], 'User');
    
    var result = $scope.displayParticipantActions(conference, participant);
    expect(result).toBeTruthy();
  });

  it('should not display participant action for other if not organizer', function() {
    var participant = createParticipant('1001', false, 'User', false, false, 1);    
    var conference = createConference([participant], 'User');
    
    var result = $scope.displayParticipantActions(conference, participant);
    expect(result).toBeFalsy();
  });

  it('should display participant action for other if organizer', function() {
    var participant = createParticipant('1001', false, 'User', false, false, 1);    
    var conference = createConference([participant], 'Organizer');
    
    var result = $scope.displayParticipantActions(conference, participant);
    expect(result).toBeTruthy();
  });

  it('should not display participant action for kicked participant', function() {
    var participant = createParticipant('1001', false, 'User', false, false, 1);
    participant.kicked = true;
    var conference = createConference([participant], 'Organizer');
    
    var result = $scope.displayParticipantActions(conference, participant);
    expect(result).toBeFalsy();
  });

  it('should properly display the long names when hovering a call line', function() {
    spyOn($document, 'find').and.returnValue([
      {
        clientWidth : 30
      }
    ]);
    
    $scope.updateNameWidthOnMouseenter();
    expect($scope.currentNameColumnWidth).toBe(3);
  });

  it('should properly display the long names on mouseleave on a call line', function() {
    spyOn($document, 'find').and.returnValue([
      {
        clientWidth : 30
      }
    ]);
    
    $scope.nameWidthBackToNormal();
    expect($scope.currentNameColumnWidth).toBe(30);
  });

  it('should display flashtext icon if the state is established', function() {
    var call = {
      state: "Established",
      username: "Alain"
    };
    var result = $scope.canSendFlashtext(call, call.username);
    expect(result).toBeTruthy(); 
  });

  it('should display flashtext icon if the state is OnHold', function() {
    var call = {
      state: "OnHold"
    };
    var participant = {
      username: "Alain"
    };
    var result = $scope.canSendFlashtext(call, participant.username);
    expect(result).toBeTruthy(); 
  });

  it('should not display flashtext icon if there is no username', function() {
    var call = {
      state: "OnHold"
    };
    var participant = {
    };
    var result = $scope.canSendFlashtext(call, participant.username);
    expect(result).toBeFalsy(); 
  });

  it('should not display flashtext icon if call is non established', function() {
    var call = {
      state: "EventReleased"
    };
    var participant = {
      username: "Alain"
    };
    var result = $scope.canSendFlashtext(call, participant.username);
    expect(result).toBeFalsy(); 
  });

});
