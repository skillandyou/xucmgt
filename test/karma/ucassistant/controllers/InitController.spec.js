describe('InitController', function() {
  var $rootScope;
  var $scope;
  var $state;
  var ctrl;
  var XucLink;
  var CtiProxy;
  var XucPhoneState;
  var errorModal;
  var $window = {
    location: {
      search: ""
    }
  };

  beforeEach(angular.mock.module('html-templates'));
  beforeEach(angular.mock.module('karma-backend'));
  beforeEach(angular.mock.module('ucAssistant'));
  beforeEach(angular.mock.module('xcHelper'));

  beforeEach(angular.mock.inject(function (_$rootScope_, $controller, _$state_, _XucLink_, _CtiProxy_, _XucPhoneState_, _errorModal_) {
    $rootScope =_$rootScope_;
    $scope = $rootScope.$new();
    $state = _$state_;
    XucLink = _XucLink_;
    CtiProxy = _CtiProxy_;
    XucPhoneState = _XucPhoneState_;
    errorModal = _errorModal_;
    ctrl = $controller('InitController', {
      '$scope' :      $scope,
      'XucLink':      XucLink,
      'CtiProxy':     CtiProxy,
      '$state':       $state,
      '$window':      $window,
      'XucPhoneState': XucPhoneState,
      'errorModal': errorModal
    });
  }));

  it('can instanciate controller', function() {
    expect(ctrl).not.toBeUndefined();
  });

  it('upon linkDisConnected stop XucLink and redirect to login page', function() {
    spyOn(XucLink, 'logout');
    spyOn($state, 'go');
    spyOn(CtiProxy, 'isUsingWebRtc').and.returnValue(false);

    $rootScope.$broadcast('linkDisConnected');
    $rootScope.$apply();

    expect(XucLink.logout).toHaveBeenCalled();
    expect($state.go).toHaveBeenCalledWith('login', {'error': 'LinkClosed'});
  });

  it('upon linkDisConnected stop webrtc if active and no call', function() {
    spyOn(XucLink, 'logout');
    spyOn($state, 'go');
    spyOn(CtiProxy, 'isUsingWebRtc').and.returnValue(true);
    spyOn(CtiProxy, 'stopUsingWebRtc');
    spyOn(XucPhoneState, 'getCalls').and.returnValue([]);

    $rootScope.$broadcast('linkDisConnected');
    $rootScope.$apply();

    expect(CtiProxy.stopUsingWebRtc).toHaveBeenCalled();
    expect(XucLink.logout).toHaveBeenCalled();
    expect($state.go).toHaveBeenCalledWith('login', {'error': 'LinkClosed'});
  });

  it('upon linkDisConnected when using webrtc, only display error if call is ongoing', function() {
    spyOn(XucLink, 'logout');
    spyOn($state, 'go');
    spyOn(CtiProxy, 'isUsingWebRtc').and.returnValue(true);
    spyOn(CtiProxy, 'stopUsingWebRtc');
    spyOn(errorModal, 'showErrorModal');
    var dummyCall = {srcNum: '1234'};
    spyOn(XucPhoneState, 'getCalls').and.returnValue([dummyCall]);

    $rootScope.$broadcast('linkDisConnected');
    $rootScope.$apply();

    expect(errorModal.showErrorModal).toHaveBeenCalledWith('CTI_SOCKET_CLOSED_WHILE_USING_WEBRTC');
    expect(CtiProxy.stopUsingWebRtc).not.toHaveBeenCalled();
    expect(XucLink.logout).not.toHaveBeenCalled();
    expect($state.go).not.toHaveBeenCalled();
  });

  it('on logout stop XucLink and WebRtc', function () {
    spyOn(XucLink, 'logout');
    spyOn(CtiProxy, 'isUsingWebRtc').and.returnValue(true);
    spyOn(CtiProxy, 'stopUsingWebRtc');

    $scope.logout();

    expect(XucLink.logout).toHaveBeenCalled();
    expect(CtiProxy.stopUsingWebRtc).toHaveBeenCalled();
  });

});
