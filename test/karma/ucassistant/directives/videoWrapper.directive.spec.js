describe('videoWrapper directive', () => {
  var $compile;
  var $rootScope;
  var $document;
  var el;
  var scope;
  var videoDirective = "<video-wrapper sip-call-id='121.124.45@192.168'></video-wrapper>";

  function compile(directive) {
    el = angular.element(directive);
    $compile(el)($rootScope.$new());
    $rootScope.$digest();
    scope = el.isolateScope() || el.scope();
  }
  
  beforeEach(angular.mock.module('html-templates'));
  beforeEach(angular.mock.module('karma-backend'));
  beforeEach(angular.mock.module('ucAssistant'));

  beforeEach(angular.mock.inject(function(_$compile_, _$rootScope_, _$document_) {
    $compile = _$compile_;
    $rootScope = _$rootScope_;
    $document = _$document_;
  }));

  beforeEach(function() {
    spyOn(xc_webrtc, 'setLocalStreamHandler').and.callFake(function(){});
    spyOn(xc_webrtc, 'setRemoteStreamHandler').and.callFake(function(){});
  });

  it('should set local and remote streams handler with same sip call id', () => {
    compile(videoDirective);

    expect(xc_webrtc.setLocalStreamHandler.calls.all()[0].args[0]).toBe(scope.setLocalStream);
    expect(xc_webrtc.setLocalStreamHandler.calls.all()[0].args[1]).toEqual('121.124.45@192.168');

    expect(xc_webrtc.setRemoteStreamHandler.calls.all()[0].args[0]).toBe(scope.setRemoteStream);
    expect(xc_webrtc.setRemoteStreamHandler.calls.all()[0].args[1]).toEqual('121.124.45@192.168');

  });

  it('should find video elem to attach stream', () => {
    spyOn($document, 'find');
    compile(videoDirective);
    scope.setStream('my-stream','my-video');
    scope.$digest();
    expect($document.find).toHaveBeenCalledWith("#my-video");
  });

});
