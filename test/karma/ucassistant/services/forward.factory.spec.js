import 'xccti/cti-webpack';

describe('Factory: forward calls', function () {
  var forward;

  beforeEach(angular.mock.module('html-templates'));
  beforeEach(angular.mock.module('karma-backend'));
  beforeEach(angular.mock.module('ucAssistant'));

  beforeEach(angular.mock.inject(function(_forward_) {
    forward = _forward_;
  }));


  it('should get correct icon with forward precedence', function() {
    var u = UserBuilder('Mike').build();

    u.naFwdEnabled = true;
    expect(forward.getIcon(u)).toEqual('forward_na');
    u.uncFwdEnabled = true;
    expect(forward.getIcon(u)).toEqual('forward');
    u.dndEnabled = true;
    expect(forward.getIcon(u)).toEqual('forward_dnd');
  });

  it('should get forward destination with forward precedence', function() {
    var u = UserBuilder('Stefen').build();

    u.naFwdEnabled = true;
    u.naFwdDestination = '4321';
    expect(forward.getDestination(u)).toEqual('4321');
    u.uncFwdEnabled = false;
    u.uncFwdDestination = '1234';
    expect(forward.getDestination(u)).toEqual('4321');
    u.uncFwdEnabled = true;
    expect(forward.getDestination(u)).toEqual('1234');
  });

  it('should say if user is forwarded or dnd', function() {
    var u = UserBuilder('John').build();

    u.naFwdEnabled = false;
    u.uncFwdEnabled = false;
    u.busyFwdEnabled = false;
    u.dndEnabled = false;
    expect(forward.isSet(u)).toBe(false);

    u.uncFwdEnabled = true;
    expect(forward.isSet(u)).toBe(true);

    u.uncFwdEnabled = false;
    u.naFwdEnabled = true;
    expect(forward.isSet(u)).toBe(true);

    u.naFwdEnabled = false;
    u.dndEnabled = true;
    expect(forward.isSet(u)).toBe(true);
  });

  it('should apply dnd', function() {
    var u = UserBuilder('Adam').build();
    u.dndEnabled = false;
    spyOn(Cti, 'dnd');

    forward.setDnd(u);
    expect(Cti.dnd).toHaveBeenCalledWith(true);
  });

  it('should reset dnd', function() {
    var u = UserBuilder('Carl').build();
    u.dndEnabled = true;
    spyOn(Cti, 'dnd');

    forward.setDnd(u);
    expect(Cti.dnd).toHaveBeenCalledWith(false);
  });

  it('should apply forwarding', function() {
    var u = UserBuilder('Georges').build();
    u.naFwdEnabled = true;
    u.naFwdDestination = '4321';
    u.uncFwdEnabled = true;
    u.uncFwdDestination = '1234';

    spyOn(Cti, 'naFwd');
    spyOn(Cti, 'uncFwd');

    forward.setForward(u);
    expect(Cti.naFwd).toHaveBeenCalledWith('4321', true);
    expect(Cti.uncFwd).toHaveBeenCalledWith('1234', true);
  });

});
