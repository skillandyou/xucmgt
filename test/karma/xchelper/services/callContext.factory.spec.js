describe('callContext', function() {
  var XucPhoneState;
  var XucUtils;
  var CtiProxy;
  var callContext;
  var vars = {a: 21};

  beforeEach(angular.mock.module('xcHelper'));
  beforeEach(angular.mock.module('xcCti'));

  beforeEach(angular.mock.inject(function(_callContext_, _XucPhoneState_, _XucUtils_, _CtiProxy_) {
    callContext = _callContext_;
    XucPhoneState = _XucPhoneState_;
    XucUtils = _XucUtils_;
    CtiProxy = _CtiProxy_;
    spyOn(XucPhoneState, 'getCalls').and.returnValue([]);
    spyOn(CtiProxy, 'dial');
  }));

  it('propagates parameters on dial request', function(){
    callContext.dialOrAttTrans('123', vars, true);
    expect(CtiProxy.dial).toHaveBeenCalledWith('123', vars, true);
  });

  it('propagates parameters on dial request with fallback on false for video and {} for vars', function(){
    callContext.dialOrAttTrans('123');
    expect(CtiProxy.dial).toHaveBeenCalledWith('123', {}, false);
  });

  it('propagates parameters on normalized dial request', function(){
    spyOn(XucUtils, 'normalizePhoneNb').and.returnValue('456');
    callContext.normalizeDialOrAttTrans('123', vars, true);
    expect(CtiProxy.dial).toHaveBeenCalledWith('456', vars, true);
  });

  it('propagates parameters on normalized dial request with fallback on false for video and {} for vars', function(){
    spyOn(XucUtils, 'normalizePhoneNb').and.returnValue('456');
    callContext.normalizeDialOrAttTrans('123');
    expect(CtiProxy.dial).toHaveBeenCalledWith('456', {}, false);
  });

});
