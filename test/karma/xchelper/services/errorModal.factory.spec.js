describe('errorModal', function () {
  var $uibModal;
  var $q;
  var errorModal;

  beforeEach(angular.mock.module('html-templates'));
  beforeEach(angular.mock.module('karma-backend'));
  beforeEach(angular.mock.module('xcHelper'));
  beforeEach(angular.mock.module('xcCti'));

  beforeEach(angular.mock.inject(function (_$uibModal_, _$q_, _errorModal_) {
    $uibModal = _$uibModal_;
    $q = _$q_;
    errorModal = _errorModal_;
  }));

  it('should open a modal when function is called', () => {
    spyOn($uibModal, 'open').and.returnValue({ result: $q.resolve({}) });
    errorModal.showErrorModal('CTI_SOCKET_CLOSED_WHILE_USING_WEBRTC');
    expect($uibModal.open).toHaveBeenCalled();
  });
});
