describe('incomingCall', function() {
  var $rootScope;
  var XucPhoneEventListener;
  var $uibModal;
  var registeredCallback = null;

  beforeEach(angular.mock.module('html-templates'));
  beforeEach(angular.mock.module('karma-backend'));
  beforeEach(angular.mock.module('xcHelper'));
  beforeEach(angular.mock.module('xcCti'));

  beforeEach(function() {
    jasmine.addMatchers({
      toEqualData : customMatchers.toEqualData
    });
  });

  beforeEach(angular.mock.inject(function(_$rootScope_, _XucPhoneEventListener_, _$uibModal_) {
    XucPhoneEventListener = _XucPhoneEventListener_;
    $rootScope = _$rootScope_;
    $uibModal = _$uibModal_;
    spyOn($rootScope, '$broadcast');
    spyOn(XucPhoneEventListener, 'addRingingHandler').and.callFake(function(scope, callback) {
      registeredCallback = callback;
    });

  }));

  function triggerCall(call) {
    if(registeredCallback != null) {
      registeredCallback(call);
    }
  }

  it('Open popup upon call reception', angular.mock.inject(function(incomingCall){
    incomingCall.init();
    expect(XucPhoneEventListener.addRingingHandler).toHaveBeenCalled();
    spyOn($uibModal, 'open').and.returnValue({result: { then: function(){}}});
    triggerCall({otherDN: "1001"});
    expect($uibModal.open).toHaveBeenCalled();
  }));

  it('Do not open popup upon second call ringing', angular.mock.inject(function(incomingCall){
    incomingCall.init();
    expect(XucPhoneEventListener.addRingingHandler).toHaveBeenCalled();
    spyOn($uibModal, 'open').and.returnValue({result: { then: function(){}}});
    triggerCall({otherDN: "1001"});
    expect($uibModal.open).toHaveBeenCalled();
    $uibModal.open.calls.reset();
    triggerCall({otherDN: "1000"});
    expect($uibModal.open).not.toHaveBeenCalled();
  }));

});
