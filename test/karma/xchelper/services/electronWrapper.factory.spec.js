'use strict';

describe('Service: Electron wrapper', function () {

  var electronWrapper;
  beforeEach(angular.mock.module('html-templates'));
  beforeEach(angular.mock.module('karma-backend'));
  beforeEach(angular.mock.module('xcCti', 'xcHelper'));


  it('should know if web app is running without electron context', function() {
    angular.mock.inject(function(_electronWrapper_) {
      electronWrapper = _electronWrapper_;
    });

    expect(electronWrapper.isElectron()).toBe(false);
  });

  it('should know if web app is running with electron context', function() {
    window.setElectronConfig = () => {};

    angular.mock.inject(function(_electronWrapper_) {
      electronWrapper = _electronWrapper_;
    });

    expect(electronWrapper.isElectron()).toBe(true);
    delete window.setElectronConfig;
  });


  it('should set electron config', function() {
    window.setElectronConfig = () => {};
    spyOn(window,'setElectronConfig');

    angular.mock.inject(function(_electronWrapper_) {
      electronWrapper = _electronWrapper_;
    });

    electronWrapper.setElectronConfig({title:'myTitle'});
    expect(window.setElectronConfig).toHaveBeenCalledWith({title: 'myTitle'});

    electronWrapper.setElectronConfig({width:100, height:200});
    expect(window.setElectronConfig).toHaveBeenCalledWith({size: {w: 100, h: 200, mini: false}});

    electronWrapper.setElectronConfig({width:100, height:200, title:'myTitle', minimalist: true});
    expect(window.setElectronConfig).toHaveBeenCalledWith({size: {w: 100, h: 200, mini: true}, title: 'myTitle'});

    electronWrapper.setElectronConfig({width:null, height:200, title:null});
    expect(window.setElectronConfig).toHaveBeenCalledWith({});

    electronWrapper.setElectronConfig({confirmQuit: {data: {}}});
    expect(window.setElectronConfig).toHaveBeenCalledWith({confirmQuit: {data: {}}});

    delete window.setElectronConfig;
  });

  it('should focus electron window', function() {
    window.setElectronConfig = () => {};
    spyOn(window,'setElectronConfig');

    angular.mock.inject(function(_electronWrapper_) {
      electronWrapper = _electronWrapper_;
    });

    electronWrapper.setFocus();
    expect(window.setElectronConfig).toHaveBeenCalledWith({focus: true});

    delete window.setElectronConfig;
  });

  it('should send runExecutable command to electron', function() {
    window.setElectronConfig = () => {};
    spyOn(window,'setElectronConfig');

    angular.mock.inject(function(_electronWrapper_) {
      electronWrapper = _electronWrapper_;
    });

    var path = "executable";
    var args = ["titi", "tutu"];
    electronWrapper.runExecutable(path, args);
    expect(window.setElectronConfig).toHaveBeenCalledWith({runExecutable: path, executableArgs: args});

    delete window.setElectronConfig;
  });

});
