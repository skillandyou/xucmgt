describe('show flash text directive', () => {

  var scope;
  var directiveScope;
  var XucFlashText;
  var $compile;

  beforeEach(angular.mock.module('html-templates'));
  beforeEach(angular.mock.module('karma-backend'));
  beforeEach(angular.mock.module('xcHelper'));
  beforeEach(angular.mock.module('xcCti'));
  beforeEach(angular.mock.module('dbaq.emoji'));

  beforeEach(function() {

    angular.mock.inject(function (_$compile_, $rootScope, _XucFlashText_) {
      scope = $rootScope.$new();
      $compile = _$compile_;
      XucFlashText = _XucFlashText_;
    });
  });

  it('generates html when instantiated', function() {
    var elementStr = angular.element('<show-flash-text></show-flash-text>');
    var element = $compile(elementStr)(scope);
    scope.$digest();
    directiveScope = element.children().scope();
    directiveScope.flashTexts = [{sequence:1, message: 'hello'}];
    scope.$digest();

    expect(element.html()).toContain('<div class="flashtext-view');
  });

  it('loads flashtext at startup', function() {
    spyOn(XucFlashText, 'getFlashTexts');

    var elementStr = angular.element('<show-flash-text></show-flash-text>');
    $compile(elementStr)(scope);
    scope.$digest();

    expect(XucFlashText.getFlashTexts).toHaveBeenCalled();
  });

  it('subscribe to receive new flashtext', function() {
    spyOn(XucFlashText, 'subscribeToFlashText');

    var elementStr = angular.element('<show-flash-text></show-flash-text>');
    $compile(elementStr)(scope);
    scope.$digest();

    expect(XucFlashText.subscribeToFlashText).toHaveBeenCalled();
  });

});