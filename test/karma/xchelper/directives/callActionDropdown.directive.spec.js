import _ from 'lodash';

describe('callActionDropdown directive', function() {

  var scope;
  var $compile;
  var directiveScope;
  var sampleContact = {
    "status": 0,
    "entry": [
      "aaa2 aaa2",
      "1009",
      "2000",
      "3000",
      false,
      "noreply@avencall.com"
    ],
    "contact_id": "69",
    "source": "internal",
    "favorite": false
  };

  beforeEach(angular.mock.module('html-templates'));
  beforeEach(angular.mock.module('karma-backend'));
  beforeEach(angular.mock.module('xcHelper'));
  beforeEach(angular.mock.module('xcCti'));

  beforeEach(function() {
    jasmine.addMatchers({
      toEqualData : customMatchers.toEqualData
    });

    angular.mock.inject(function (_$compile_, $rootScope) {
      scope = $rootScope.$new();
      scope.contact = sampleContact;
      scope.phoneNumber = "bar";

      $compile = _$compile_;

      var elementStr = angular.element('<call-action-dropdown contact="contact" phoneNumber="123"></call-action-dropdown>');
      var element = $compile(elementStr)(scope);
      scope.$digest();
      directiveScope = element.children().scope();
    });
  });

  it('bind directive', function() {
    expect(_.isArray(directiveScope.phoneNumbers)).toBe(true);
  });

  it('extract phone numbers from contact', function() {
    expect(directiveScope.phoneNumbers).toEqual(["1009", "2000", "3000"]);
  });

  it('extract email from contact', function() {
    expect(directiveScope.email).toEqual('noreply@avencall.com');
  });

  it('hide video button if video is not enabled', function() {
    var elementStr = angular.element('<call-action-dropdown contact="contact"></call-action-dropdown>');
    var element = $compile(elementStr)(scope);
    scope.$digest();
    directiveScope = element.children().scope();

    expect(element.html()).not.toContain('<i class="fa fa-video-camera"></i>');
  });

  it('add video button if video is enabled', function() {
    var elementStr = angular.element('<call-action-dropdown contact="contact" enable-video="true"></call-action-dropdown>');
    var element = $compile(elementStr)(scope);
    scope.$digest();
    directiveScope = element.children().scope();

    expect(element.html()).toContain('<i class="fa fa-video-camera"></i>');
  });

  it('add flashtext button if flashtext is enabled', function() {
    var elementStr = angular.element('<call-action-dropdown contact="contact" enable-flashtext="true"></call-action-dropdown>');
    var element = $compile(elementStr)(scope);
    scope.$digest();
    directiveScope = element.children().scope();

    expect(element.html()).toContain('<i class="fa fa-commenting"></i>');
  });

  it('hide flashtext button if flashtext is disabled', function() {
    var elementStr = angular.element('<call-action-dropdown contact="contact"></call-action-dropdown>');
    var element = $compile(elementStr)(scope);
    scope.$digest();
    directiveScope = element.children().scope();

    expect(element.html()).not.toContain('<i class="fa fa-commenting"></i>');
  });

  it('should not enable flashtext if user is unavailable', function(){
    scope.contact.status = 4;
    var result = directiveScope.canShowFlashtext();
    expect(result).toBeFalsy();
  });

  it('should not enable flashtext if user is deactivated', function(){
    scope.contact.status = -1;
    var result = directiveScope.canShowFlashtext();
    expect(result).toBeFalsy();
  });

  it('should not enable flashtext if user is non existing', function(){
    scope.contact.status = -2;
    var result = directiveScope.canShowFlashtext();
    expect(result).toBeFalsy();
  });

  it('should enable flashtext if user is available', function(){
    scope.contact.status = 1;
    var result = directiveScope.canShowFlashtext();
    expect(result).toBeTruthy();
  });
});