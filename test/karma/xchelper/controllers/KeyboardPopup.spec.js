describe('KeyboardPopup', function() {

  var $rootScope;
  var $scope;
  var $document;
  var CtiProxy;
  var XucPhoneEventListener;
  var onPhoneEvent;
  var $uibModalInstance;
  var ctrl;

  beforeEach(angular.mock.module('xcHelper'));
  beforeEach(angular.mock.module('xcCti'));

  beforeEach(angular.mock.inject(function(_$rootScope_, $controller, _CtiProxy_, _$document_, _XucPhoneEventListener_, _$uibModal_) {
    $rootScope =_$rootScope_;
    $scope = $rootScope.$new();
    CtiProxy = _CtiProxy_;
    $document = _$document_;
    XucPhoneEventListener = _XucPhoneEventListener_;

    $uibModalInstance = _$uibModal_.open({
      templateUrl: 'assets/javascripts/xchelper/controllers/keyboard.html'
    });

    spyOn(XucPhoneEventListener, 'addReleasedHandler').and.callFake(function(scope, handler) {
      onPhoneEvent = handler;
    });

    ctrl = $controller('KeyboardPopup', {
      '$scope' :      $scope,
      '$uibModalInstance' :   $uibModalInstance,
      'CtiProxy' : CtiProxy,
      '$document' :   $document,
      'XucPhoneEventListener' :   XucPhoneEventListener
    });
  }));

  it('can instantiate controller', function(){
    expect(ctrl).not.toBeUndefined();
  });

  it('can instantiate handler', function() {
    expect(onPhoneEvent).not.toBeUndefined();
  });

  it('register XucPhoneEventListener callback', function() {
    expect(XucPhoneEventListener.addReleasedHandler).toHaveBeenCalled();
  });

  it('modalInstance is closed by default', function() {
    expect($scope.modalOpened).toEqual(false);
  });

  it('closes the keypad by click', function() {
    $scope.modalOpened=true;
    spyOn($uibModalInstance, 'close');

    $scope.close();
    expect($uibModalInstance.close).toHaveBeenCalled();
  });

  it('closes the keypad by event', function() {
    $scope.modalOpened=true;
    $scope.keysHistory = '12345';
    spyOn($uibModalInstance, 'close');

    onPhoneEvent({eventType: XucPhoneEventListener.EVENT_RELEASED});
    expect($uibModalInstance.close).toHaveBeenCalled();
    expect($scope.keysHistory).toEqual("");
  });
});
