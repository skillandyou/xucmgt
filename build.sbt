import com.typesafe.sbt.packager.docker._
import com.typesafe.sbt.packager.MappingsHelper._
import com.typesafe.sbt.web.pipeline.Pipeline
import com.typesafe.sbt.web.{GeneralProblem, SbtWeb}
import play.sbt.PlayImport.PlayKeys.playRunHooks
import sbt.Keys.scalacOptions
import sbt.compiler.CompileFailed

val appName = "xucmgt"
// Do not change version directly but change TARGET_VERSION
val appVersion = sys.env.getOrElse("TARGET_VERSION", "dev-version")
val appOrganisation = "xivo"

val main = Project(appName, file("."))
  .enablePlugins(play.sbt.PlayScala)
  .enablePlugins(SbtWeb)
  .enablePlugins(DockerPlugin)
  .enablePlugins(BuildInfoPlugin)
  .settings(
    name := appName,
    version := appVersion,
    crossPaths := false,
    scalaVersion := Dependencies.scalaVersion,
    organization := appOrganisation,
    resolvers ++= Dependencies.resolutionRepos,
    libraryDependencies ++= Dependencies.runDep ++ Dependencies.testDep,
    includeFilter in (Assets, LessKeys.less) := "*.less",
    excludeFilter in (Assets, LessKeys.less) := "_*.less",
    excludeFilter in (Assets, JshintKeys.jshint) := new FileFilter{ def accept(f: File) = ".*(/javascripts/(ccagent/|xccti/|xchelper/|ccmanager/|ucassistant/)|webpack).*".r.pattern.matcher(f.getAbsolutePath).matches },
    publishArtifact in(Compile, packageDoc) := false,
    publishArtifact in packageDoc := false,
    playRunHooks += baseDirectory.map(base => Webpack(base)).value,
    webpack := {
      val log = new CustomLogger
      if("npm i".!(log) != 0) throw new CompileFailed(Array(), "npm install failed",  Array(new GeneralProblem(log.buf.toString, file("./package.json"))))
      if("npm run build".!(log) != 0) throw new CompileFailed(Array(), "webpack build failed", Array(new GeneralProblem(log.buf.toString, file("./webpack.config.js"))))
    },
    webpackPipeline := { mappings =>
      val webpackBaseDir = baseDirectory.value / "target/web/public/main/javascripts/dist"
      val webpackRebaseDir = baseDirectory.value / "target/web/public/main/"
      val webpackFiles = PathFinder(webpackBaseDir).*** --- webpackBaseDir pair relativeTo(webpackRebaseDir)
      mappings ++ webpackFiles
    },
    npmTest := {
      val log = new CustomLogger
      if("npm i".!(log) != 0) throw new CompileFailed(Array(), "npm install failed",  Array(new GeneralProblem(log.buf.toString, file("./package.json"))))
      if("npm run test-headless".!(log) != 0) throw new RuntimeException("NPM Tests failed")
    },
    (test in Test) := ((test in Test) dependsOn npmTest).value,
    pipelineStages := Seq(webpackPipeline, digest),
    dist := (dist dependsOn webpack).value,
    (stage in Docker) := ((stage in Docker) dependsOn webpack).value
  )
  .settings(
    testOptions in Test += Tests.Argument(TestFrameworks.ScalaTest, "-o", "-u","target/test-reports"),
    testOptions in Test += Tests.Setup(() => { System.setProperty("XUCMGT_VERSION", appVersion) }),
    scalacOptions ++= Seq(
      "-encoding", "UTF-8",
      "-unchecked",
      "-deprecation",
      "-feature",
      "-language:existentials",
      "-language:higherKinds",
      "-language:implicitConversions",
      "-Xfuture",
      "-Yno-adapted-args",
      "-Ywarn-dead-code"
    )
  )
  .settings(dockerSettings: _*)
  .settings(editSourceSettings: _*)
  .settings(docSettings: _*)
  .settings(
    setVersionVarTask := { System.setProperty("XUCMGT_VERSION", appVersion) },
    edit in EditSource := ((edit in EditSource) dependsOn (EditSourcePlugin.autoImport.clean in EditSource)).value,
    packageBin in Compile := ((packageBin in Compile) dependsOn (edit in EditSource)).value,
    run in Compile := ((run in Compile) dependsOn setVersionVarTask).evaluated
  )
  .settings(buildInfoSettings: _*)


lazy val setVersionVarTask = taskKey[Unit]("Set version to a env var")
lazy val webpack = taskKey[Unit]("Webpack stage")
lazy val webpackPipeline = taskKey[Pipeline.Stage]("Webpack pipeline")
lazy val npmTest = taskKey[Unit]("Run NPM headless test")


lazy val buildInfoSettings = Seq(
  buildInfoKeys := Seq[BuildInfoKey](name, version, scalaVersion, sbtVersion),
  buildInfoPackage := "server.info"
)

lazy val dockerSettings = Seq(
  maintainer in Docker := "R&D <randd@xivo.solutions>",
  dockerBaseImage := "openjdk:8u162-jdk-slim-stretch",
  dockerExposedPorts := Seq(9000),
  dockerExposedVolumes := Seq("/conf","/logs"),
  dockerRepository := Some("xivoxc"),
  dockerCommands += Cmd("LABEL", s"""version="$appVersion""""),
  dockerEntrypoint := Seq("bin/xucmgt_docker")
)

lazy val editSourceSettings = Seq(
  flatten in EditSource := true,
  mappings in Universal += file("target/version/appli.version") -> "conf/appli.version",
  mappings in Universal ++= directory("updates"),
  targetDirectory in EditSource := baseDirectory(_/"target/version").value,
  variables in EditSource += version {_ => ("SBT_EDIT_APP_VERSION", appVersion)}.value,
  (sources in EditSource) ++= (baseDirectory map { bd =>
    (bd / "src/res" * "appli.version").get
  }).value
)

lazy val docSettings = Seq(
  publishArtifact in(Compile, packageDoc) := false,
  publishArtifact in packageDoc := false,
  sources in(Compile, doc) := Seq.empty
)
