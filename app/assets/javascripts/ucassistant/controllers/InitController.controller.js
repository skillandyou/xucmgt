(function () {
  'use strict';
  angular
    .module('ucAssistant')
    .controller('InitController', initController);

  initController.$inject = ['$scope', '$rootScope', '$state', '$window', '$log', 'XucUtils', '$translate', 'errorModal', 'XucPhoneState', 'CtiProxy', 'callContext', 'XucLink', 'XucFlashText', 'XucSheet'];

  function initController($scope, $rootScope, $state, $window, $log, xucUtils, $translate, errorModal, XucPhoneState, CtiProxy, callContext, XucLink, XucFlashText, XucSheet) {

    this.XucFlashText = XucFlashText;
    this.XucSheet = XucSheet;

    CtiProxy.disableMediaKeys();
    
    $scope.getPhoneStateLabel = function (state) {
      return "USER_STATUS_" + state;
    };

    $scope.logout = function () {
      XucLink.logout();
      $scope.isQuitting = true;
      CtiProxy.stopUsingWebRtc();
      $state.go("login", {
        error: "Logout"
      });
    };

    $scope.$on('linkDisConnected', function () {
      $log.warn('InitController - Received linkDisConnected');
      if (CtiProxy.isUsingWebRtc() && XucPhoneState.getCalls().length > 0) {
        $log.warn('CTI WS closed when CtiProxy is using WebRtc');
        errorModal.showErrorModal('CTI_SOCKET_CLOSED_WHILE_USING_WEBRTC');
      } else {
        $window.location.search = '';
        if (CtiProxy.isUsingWebRtc()) {
          CtiProxy.stopUsingWebRtc();
        }
        XucLink.logout();
        $state.go('login', { 'error': 'LinkClosed' });
      }
    });
  }
})();
