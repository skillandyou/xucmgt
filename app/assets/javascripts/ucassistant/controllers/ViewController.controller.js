import moment from 'moment';
import _ from 'lodash';

export default class ViewController {

  constructor($scope, $state, $rootScope, localStorageService, XucCallHistory, XucPhoneEventListener, $transitions, electronWrapper, XucUtils, callContext, XucFlashText, $stateParams) {
    this.$scope = $scope;
    this.$state = $state;
    this.$stateParams = $stateParams;
    this.$rootScope = $rootScope;
    this.localStorageService = localStorageService;
    this.XucCallHistory = XucCallHistory;
    this.XucPhoneEventListener = XucPhoneEventListener;
    this.XucFlashText = XucFlashText;
    this.XucUtils = XucUtils;
    this.$transitions = $transitions;
    this.electronWrapper = electronWrapper;
    this.callHistoryCheckedAtKey = 'ViewController.callHistoryCheckedAt';
    this.callHistoryCheckedAt;
    this.callHistory = [];
    this.callHistoryCheckedAtStr;
    this.callContext = callContext;
    this.isPhone = true;
    this.hasMessage = false;
    this.missedCalls = 0;

    $scope.isActive = (viewName) => {
      return this.$state.is(viewName);
    };

    this.$scope.$watch( () =>  {
      return XucFlashText.hasNewMessage();
    },  (newValue) => {
      this.hasMessage = newValue;
      this.updateElectronIcon();
    }
    );

    this.init();
  }

  updateElectronIcon() {
    if (this.hasMessage || this.missedCalls) {
      this.electronWrapper.setTrayIcon('missed');
    } else {
      this.electronWrapper.setTrayIcon('default');
    }
  }

  setFlashTextReceptionHandler () {
    this.$rootScope.$on('FlashTextReceived', (event, ft) => {
      if (ft.event == 'FlashTextUserMessage') {
        if (this.XucFlashText.hasNewMessage()) {
          this.hasMessage = true;
          this.updateElectronIcon();
        }
      }
    });
  }
  
  updateMissedCalls() {
    this.missedCalls = _.sumBy(this.callHistory, (day) => {
      return _.sumBy(day.details, (call) => {
        return (call.status === 'missed' && moment(call.start).isAfter(this.callHistoryCheckedAt)) ? 1 : 0;
      });
    });
    this.updateElectronIcon();
  }

  callHistoryUpdated(_callHistory) {
    this.callHistory = _callHistory;
    this.updateMissedCalls();
  }

  historyChecked() {
    this.callHistoryCheckedAt = moment();
    this.localStorageService.set(this.callHistoryCheckedAtKey, moment(this.callHistoryCheckedAt).toISOString());
    this.updateMissedCalls();
  }

  dialOrSearch() {
    const input = this.XucUtils.reshapeInput(this.$scope.destination);
    if (input.type === 'phone') {
      this.callContext.dialOrAttTrans(input.value);
    }
    else {
      this.$state.go('interface.search', { 'showFavorites': false, 'search': input.value });
    }
  }

  isEnterKey(event) {
    if (event.which == 13) {
      angular.element('#numberTodial').blur();
      event.preventDefault();
      this.dialOrSearch();
    } else {
      this.changeIcon();
    }
  }

  changeIcon() {
    let normalizedValue = this.XucUtils.normalizePhoneNb(this.$scope.destination);
    this.$scope.isPhone = this.XucUtils.isaPhoneNb(normalizedValue);
  }

  clickSearch() {
    this.dialOrSearch();
  }

  emptyInput() {
    this.$scope.destination = "";
    this.changeIcon();
  }

  init() {
    this.callHistoryCheckedAtStr = this.localStorageService.get(this.callHistoryCheckedAtKey) || "1970-01-01";
    this.callHistoryCheckedAt = moment(this.callHistoryCheckedAtStr);
    this.XucCallHistory.subscribeToUserCallHistory(this.$scope, this.callHistoryUpdated.bind(this));
    this.XucCallHistory.updateUserCallHistory();
    this.$transitions.onSuccess({}, trans => {
      if (trans.$to().name == 'interface.history') {
        this.historyChecked();
      }
    });
    this.setFlashTextReceptionHandler();
  }

}