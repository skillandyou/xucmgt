import _ from 'lodash';

(function() {
  'use strict';
  angular
    .module('ucAssistant')
    .controller('ContactsController', contactsController);

  contactsController.$inject = ['$rootScope', '$scope', '$timeout', '$state', '$stateParams', 'XucDirectory', '$http', '$log', 'XucLink', 'CtiProxy', 'remoteConfiguration'];

  function contactsController($rootScope, $scope, $timeout, $state, $stateParams, XucDirectory, $http, $log, XucLink, CtiProxy, remoteConfiguration) {

    $scope.$on('searchResultUpdated', function() {
      $scope.searchResult = _.sortBy(XucDirectory.getSearchResult(), function(contact) {
        return contact.entry[0].toLowerCase();
      });
      $scope.headers = XucDirectory.getHeaders().slice(0, 5);
      if (!$scope.$$phase) $scope.$apply();
    });

    $scope.$on('favoritesUpdated', function() {
      $scope.favorites = _.sortBy(XucDirectory.getFavorites(), function(contact) {
        return contact.entry[0].toLowerCase();
      });
      $scope.headers = XucDirectory.getHeaders().slice(0, 5);
      if (!$scope.$$phase) $scope.$apply();
    });

    $scope.isEmpty = function(field) {
      if (typeof field != 'undefined') {
        if (field.length > 0) {
          return true;
        }
      }
      return false;
    };

    $scope.$on('DirectoryLookUp', function(event, term) {
      $scope.search(term);
    });

    $scope.search = function(term) {
      Cti.directoryLookUp(term);
      $scope.showSearch = true;
    };

    $scope.getFavorites = function() {
      Cti.getFavorites();
    };

    $scope.addFavorite = function(contact) {
      Cti.addFavorite(contact.contact_id, contact.source);
    };

    $scope.removeFavorite = function(contact) {
      Cti.removeFavorite(contact.contact_id, contact.source);
    };

    $scope.init = function() {
      if ($stateParams.showFavorites === false) {
        $scope.showSearch = true;
      } else {
        $scope.getFavorites();
        $scope.showSearch = false;
      }

      if (typeof($stateParams.search) != 'undefined') {
        $scope.search($stateParams.search);
      }

      $scope.setVideoCapabilities();
    };

    $scope.isCallable = function(contact) {
      return _.some([1, 2, 3, 5], function(index) {
        return !_.isEmpty(contact.entry[index]);
      });
    };

    $scope.isPersonal = function(contact) {
      return contact.hover && contact.source == 'personal';
    };

    $scope.setVideoCapabilities = function() {
      remoteConfiguration.getBooleanOrElse('enableVideo', false).then(function(answer) {
        if(answer) {
          $scope.setVideo(CtiProxy.isUsingWebRtc() && answer);
        } else {
          $log.info('Unable to get video enable, disabling');
          $scope.setVideo(false);
        }
      }, function() {
        $log.warn('Unable to get video enable, disabling');
        $scope.setVideo(false);
      }).catch( function(e) {
        $log.warn('Exception thrown when asking for video configuration: ', e);
        $scope.setVideo(false);
      });
    };

    $scope.setVideo = function(value) {
      $scope.videoEnabled = value;
      $log.debug('Video is: ', $scope.videoEnabled);
    };

    $scope.init();
  }
})();