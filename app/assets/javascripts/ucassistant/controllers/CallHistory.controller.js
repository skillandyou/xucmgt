import moment from 'moment';

(function () {
  'use strict';
  angular
    .module('ucAssistant')
    .controller('CallHistoryController', callHistoryController);

  callHistoryController.$inject = ['callHistoryPartyFormatter', '$scope', 'XucUtils', '$state', 'XucPhoneState', 'CtiProxy', 'XucCallHistory', '$translate', 'callContext'];

  function callHistoryController(callHistoryPartyFormatter, $scope, XucUtils, $state, XucPhoneState, CtiProxy, XucCallHistory, $translate, callContext) {
    $scope.history = [];

    $scope.statusImage = function (status) {
      var baseUrl = '/assets/images/ucassistant/call_statut_';
      if (status == 'emitted') return baseUrl + 'outgoing.svg';
      else if (status == 'answered') return baseUrl + 'incoming.svg';
      else if (status == 'missed') return baseUrl + 'missed.svg';
      return '';
    };

    $scope.dial = function (number) {
      callContext.normalizeDialOrAttTrans(number);
    };

    $scope.createContact = (number, firstname, lastname) => {
      let contact = {
        "firstname": firstname,
        "lastname": lastname,
        "number": number
      };
      $state.go('interface.personalContact', { "contact": contact });
    };

    $scope.formatHistoryDay = function (date) {
      return moment(date).calendar(null, {
        sameDay: '[' + $translate.instant('CALENDAR_SAME_DAY') + ']',
        lastDay: '[' + $translate.instant('CALENDAR_LAST_DAY') + ']',
        lastWeek: 'dddd [' + $translate.instant('CALENDAR_LAST_WEEK') + ']',
        sameElse: 'DD/MM/YYYY'
      });
    };

    $scope.formatHistoryTime = function (date) {
      return moment(date).format('HH:mm');
    };

    $scope.formatHistoryDuration = function (duration) {
      return duration;
    };

    $scope.showChildrens = function (item) {
      item.display = !item.display;
    };

    $scope.getDisplayClass = function (item) {
      if (item.display === false) {
        return "children-hiddens";
      } else if (item.display === true) {
        return "children-show";
      } else {
        return "children-hiddens";
      }
    };

    $scope.getChevronClass = function (item) {
      if (item.display === false) {
        return "fa-chevron-right";
      } else if (item.display === true) {
        return "fa-chevron-down";
      } else {
        return "fa-chevron-right";
      }
    };

    $scope.init = function () {
      XucCallHistory.subscribeToUserCallHistory($scope, function (callHistory) {
        $scope.history = callHistoryPartyFormatter.toPartyCallsPerDay(callHistory);
      });
      XucCallHistory.updateUserCallHistory();
    };
    $scope.init();
  }

})();
