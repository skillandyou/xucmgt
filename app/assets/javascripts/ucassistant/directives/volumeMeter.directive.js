export default function volumeMeter() {

  return {
    restrict: 'E',
    templateUrl: 'assets/javascripts/ucassistant/directives/volumeMeter.html',
    scope: {
      micOrAudio: '@',
      sipCallId: '@',
      show: '='
    },
    controller: ($scope, processVolume) => {
      $scope.id = $scope.micOrAudio + $scope.sipCallId;
      $scope.volume = 0;
      $scope.context = processVolume.getAudioContext();
      $scope.pack = {'processor': $scope.context.createScriptProcessor() ,'analyser': $scope.context.createAnalyser()};

      $scope.setAndConnect = function(stream) {
        processVolume.setProcess($scope.pack, $scope.id);
        processVolume.connect($scope.pack, stream);
      };

      if ($scope.micOrAudio === "mic") {
        $scope.unregisterStreamHandler = processVolume.setLocalStreamHandler($scope.setAndConnect, $scope.sipCallId);
      }

      else if ($scope.micOrAudio === "audio") {
        $scope.unregisterStreamHandler = processVolume.setRemoteStreamHandler($scope.setAndConnect, $scope.sipCallId);
      }

      else {
        throw 'micOrAudio should be "mic" or "audio"';
      }

      $scope.onHoldAndResume = function(eventType) {
        switch(eventType) {
        case 'Resume': {
          processVolume.connect($scope.pack);
          break;
        }
        case 'Hold': {
          processVolume.disconnect($scope.pack);
          break;
        }
        case 'Terminated': {
          break;
        }}
      };

      processVolume.setEventTypeHandler($scope.onHoldAndResume, $scope.sipCallId);

      $scope.setVolume = function(volume) {
        $scope.volume = volume;
      };

      $scope.unregisterVolumeHandler = processVolume.setVolumeHandler($scope.setVolume, $scope.id);

      $scope.unregister = function() {
        processVolume.stopProcess($scope.pack);
        if (angular.isDefined($scope.unregisterEventHandler)) {
          $scope.unregisterEventHandler();
        }
        if (angular.isDefined($scope.unregisterVolumeHandler)) {
          $scope.unregisterVolumeHandler();
        }
        if (angular.isDefined($scope.unregisterStreamHandler)) {
          $scope.unregisterStreamHandler();
        }
      };
      $scope.$on('$destroy', $scope.unregister);
    }
  };
}
