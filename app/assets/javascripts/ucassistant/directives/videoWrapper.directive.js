export default function videoWrapper() {

  return {
    restrict: 'E',
    templateUrl: 'assets/javascripts/ucassistant/directives/videoWrapper.html',
    scope: {
      sipCallId: '@'
    },
    controller: ($scope, $document) => {
      $scope.setStream = (stream, tag) => {
        let elem = $document.find('#'+tag);
        if (elem) elem[0].srcObject = stream;
      };

      $scope.setLocalStream = (stream) => {
        $scope.setStream(stream, 'video-local');
      };

      $scope.setRemoteStream = (stream) => {
        $scope.setStream(stream, 'video-remote');
      };

      $scope.unregisterLocalStreamHandler = xc_webrtc.setLocalStreamHandler($scope.setLocalStream, $scope.sipCallId);
      $scope.unregisterRemoteStreamHandler = xc_webrtc.setRemoteStreamHandler($scope.setRemoteStream, $scope.sipCallId);

      $scope.unregister = () => {
        if (angular.isDefined($scope.unregisterLocalStreamHandler)) {
          $scope.unregisterLocalStreamHandler();
        }
        if (angular.isDefined($scope.unregisterRemoteStreamHandler)) {
          $scope.unregisterRemoteStreamHandler();
        }
      };
      $scope.$on('$destroy', $scope.unregister);
    }
  };
}
