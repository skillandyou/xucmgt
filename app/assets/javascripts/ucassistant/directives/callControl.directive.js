export default function callControl() {

  return {
    restrict: 'E',
    templateUrl: 'ucassistant/callControl.html',
    controller: 'CallControlController'
  };
}