export default function run($rootScope, $state, $uibModal, $window, XucLink, XucUser, CtiProxy, $log, $transitions, externalEvent, electronWrapper, remoteConfiguration) {
  electronWrapper.setElectronConfig({width: 470, height: 750, title: 'UC Assistant'});
  $rootScope.$state = $state;

  $transitions.onStart({
    to: function(state) {
      return state.data !== null && state.data.requireLogin === true;
    }
  }, function(trans) {
    if (!XucLink.isLogged()) {
      $log.info("Login required");
      return trans.router.stateService.target('login');
    }
  });

  $transitions.onSuccess({ to: 'login' }, () => {
    electronWrapper.setElectronConfig({confirmQuit: {reset: true}});
    electronWrapper.setTrayIcon('logout');
  });
  $transitions.onSuccess({ from: 'login' }, () => {
    remoteConfiguration.get("version").then((serverVersion) => {
      if (serverVersion != $window.appVersion){
        $log.warn("Server version mismatch", serverVersion);
        $window.location.replace("/#!/login?error=Version");
      }
    });
    electronWrapper.setTrayIcon('default');
  });
  var unregisterFatalError = $rootScope.$on(CtiProxy.FatalError, () => {
    externalEvent.unRegisterExternalEventListener();
  });
  $rootScope.$on('$destroy', unregisterFatalError);
}