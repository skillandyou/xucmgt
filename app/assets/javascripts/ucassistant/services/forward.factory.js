import _ from 'lodash';

export default function forward($translate, XucUtils){

  const forwardTypes = [
    {id: 'uncFwd', label: $translate.instant('FORWARD_TYPE_UNC'), icon: 'forward', enable: 'uncFwdEnabled', destination:'uncFwdDestination'},
    {id: 'naFwd', label: $translate.instant('FORWARD_TYPE_NA'), icon: 'forward_na', enable: 'naFwdEnabled', destination:'naFwdDestination'}
  ];

  const dndTypes = [
    {id: 'dnd', icon: 'forward_dnd', enable: 'dndEnabled'}
  ];

  var _getType = function(user) {
    var type;

    if (user.naFwdEnabled) {
      type = _.find(forwardTypes, { 'id': 'naFwd'});
    }
    if (user.uncFwdEnabled) {
      type = _.find(forwardTypes, { 'id': 'uncFwd'});
    }
    if (user.dndEnabled) {
      type = _.find(dndTypes, { 'id': 'dnd'});
    }

    return type;
  };

  var _validateExtensions = function(user) {
    if (typeof(user.uncFwdDestination) === "undefined" || !XucUtils.isaPhoneNb(user.uncFwdDestination)) {
      user.uncFwdDestination = "";
    }
    if (typeof(user.naFwdDestination) === "undefined" || !XucUtils.isaPhoneNb(user.naFwdDestination)) {
      user.naFwdDestination = "";
    }
  };

  var _getIcon = function(user) {
    var fwd = _getType(user);
    return (fwd) ? fwd.icon : null;
  };

  var _listFwdTypes = function() {
    return forwardTypes;
  };

  var _getDestination = function(user) {
    _validateExtensions(user);
    if (user.dndEnabled) return $translate.instant('FORWARD_DND');
    if (user.uncFwdEnabled) return user.uncFwdDestination;
    if (user.naFwdEnabled) return user.naFwdDestination;
    if (user.busyFwdEnabled) return user.busyFwdDestination;
  };

  var _isForwarded = function(user) {
    return user.naFwdEnabled || user.uncFwdEnabled || user.busyFwdEnabled;
  };

  var _isSet =  function(user) {
    return _isForwarded(user) || user.dndEnabled;
  };

  var _setForward = function(user) {
    user.naFwdDestination = XucUtils.normalizePhoneNb(user.naFwdDestination);
    user.uncFwdDestination = XucUtils.normalizePhoneNb(user.uncFwdDestination);
    _validateExtensions(user);

    Cti.naFwd(user.naFwdDestination, user.naFwdEnabled);
    Cti.uncFwd(user.uncFwdDestination, user.uncFwdEnabled);
  };

  var _setDnd = function(user) {
    Cti.dnd(!user.dndEnabled);
  };

  return {
    listFwdTypes : _listFwdTypes,
    getIcon : _getIcon,
    getDestination : _getDestination,
    isSet : _isSet,
    setForward : _setForward,
    setDnd : _setDnd
  };
}
