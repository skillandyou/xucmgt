export default function processVolume(){
  var volumeHandlers = {};

  function getAudioContext() {
    return xc_webrtc.getAudioContext();
  }

  function setRemoteStreamHandler(handler, sipCallId) {
    return xc_webrtc.setRemoteStreamHandler(handler, sipCallId);
  }

  function setLocalStreamHandler(handler, sipCallId) {
    return xc_webrtc.setLocalStreamHandler(handler, sipCallId);
  }

  function setEventTypeHandler(handler, sipCallId) {
    var filterById = function(event) {
      if (event.sipCallId === sipCallId) {
        handler(event.type);
      }
    };
    var unregisterIncoming = xc_webrtc.setHandler(xc_webrtc.MessageType.INCOMING, filterById);
    var unregisterOutgoing = xc_webrtc.setHandler(xc_webrtc.MessageType.OUTGOING, filterById);
    return function() {
      unregisterIncoming();
      unregisterOutgoing();
    };
  }

  function setVolumeHandler(handler, id) {
    if (!(id in volumeHandlers)){
      volumeHandlers[id] = [];
    }
    volumeHandlers[id].push(handler);
    return function() { return _unsetVolumeHandler(handler, id); };
  }

  function _unsetVolumeHandler(handler, id) {
    volumeHandlers[id] = volumeHandlers[id].filter(function(elem) { return elem !== handler;});
    if (volumeHandlers[id].length === 0) {
      delete volumeHandlers[id];
    }
    return handler;
  }

  function setProcess(pack, id) {
    pack.processor.onaudioprocess = function() {
      var freqByteData = new Uint8Array(pack.analyser.frequencyBinCount);
      pack.analyser.getByteFrequencyData(freqByteData);
      var volume = getAverageVolume(freqByteData);
      volumeHandlers[id].forEach(function(fct) {fct(volume); });
    };
  }

  function connect(pack, stream) {
    if (typeof stream !== 'undefined' && stream.active === true) {
      var source = getAudioContext().createMediaStreamSource(stream);
      source.connect(pack.analyser);
    }
    pack.processor.connect(pack.analyser);
  }

  function disconnect(pack) {
    pack.processor.disconnect();
  }

  function stopProcess(pack) {
    disconnect(pack);
    pack.processor.onaudioprocess = null;
  }

  function getAverageVolume (array) {
    var values = 0;
    for (var i = 0; i < array.length; i++) {
      values += Math.pow(array[i], 2);
    }
    return Math.sqrt(values / array.length);
  }

  return {
    getAudioContext: getAudioContext,
    setRemoteStreamHandler: setRemoteStreamHandler,
    setLocalStreamHandler: setLocalStreamHandler,
    setEventTypeHandler: setEventTypeHandler,
    setVolumeHandler: setVolumeHandler,
    setProcess: setProcess,
    connect: connect,
    disconnect: disconnect,
    stopProcess: stopProcess
  };
}
