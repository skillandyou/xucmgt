import {ELECTRON_DEFAULT_WIDTH, ELECTRON_DEFAULT_HEIGHT} from '../agent.run';

export default function callControl($translate, $log, XucPhoneState, CtiProxy, electronWrapper, remoteConfiguration) {

  return {
    restrict: 'E',
    templateUrl: 'assets/javascripts/ccagent/directives/callControl.html',
    controller: ($scope) => {
      
      const keyBindings = new Set(["F3", "F4", "F7", "F8", "F9"]);

      document.addEventListener("keydown", event => {
        if (keyBindings.has(event.key)) {
          event.preventDefault();
          switch (event.key) {
          case 'F3':
            $scope.triggerAction('answer');
            break;
          case 'F4':
            $scope.triggerAction('hangup');
            break;
          case 'F7':
            CtiProxy.completeTransfer();
            break;
          case 'F8':
            document.getElementById("search").focus();
            break;
          case 'F9':
            $log.info('Not implemented yet');
            break;
          }
        }
      });

      $scope.calls = XucPhoneState.getCalls();
      $scope.conference = XucPhoneState.getConference();

      $scope.showRecordingControls = false;

      remoteConfiguration.getBoolean("showRecordingControls").then(value => $scope.showRecordingControls = value);

      $scope.isRinging = () => {
        return $scope.calls.some(c => c.state == XucPhoneState.STATE_RINGING);
      };

      $scope.isEstablished = () => {
        return $scope.calls.length > 0 && !$scope.isRinging();
      };

      $scope.triggerAction = (preciseAction = undefined) => {
        if ($scope.isRinging() && preciseAction != 'hangup') {
          electronWrapper.setElectronConfig({width: ELECTRON_DEFAULT_WIDTH, height: ELECTRON_DEFAULT_HEIGHT});
          CtiProxy.answer();
        }
        if ($scope.isEstablished() && preciseAction != 'answer') {
          CtiProxy.hangup();
        }
      };
    }
  };
}
