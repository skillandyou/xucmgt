import _ from 'lodash';
import moment from 'moment';

export default function agentState($translate, $log, XucAgentUser, XucLink, XucAgent, $state, $interval, externalEvent, electronWrapper) {

  return {
    restrict: 'E',
    templateUrl: 'assets/javascripts/ccagent/directives/agentState.html',
    link: (scope) => {
      const logoutState = 'AgentLoggedOut';
      const onCallState = 'AgentOnCall';

      var stopTime;
      var quitApp = false;
      const stateRefreshTimeout = 1000;
      scope.agentState = {};
      scope.loggedIn = false;

      scope.refreshState = function() {
        scope.agentState.timeInState = moment().countdown(scope.agentState.momentStart);
      };
      stopTime = $interval(scope.refreshState, stateRefreshTimeout);

      scope.$on('$destroy', function() {
        $interval.cancel(stopTime);
      });

      scope.checkLogout = function(state) {
        if(state.name === logoutState) {
          if (scope.loggedIn) {
            XucLink.logout();
            scope.loggedIn = false;
            $state.go("login", {error: "Logout"});
            if (quitApp) electronWrapper.forceQuit();
          }
        }
        else {
          scope.loggedIn = true;
        }
      };

      XucAgentUser.subscribeToAgentState(scope, function(agtState) {
        $log.debug("agentState: received "+agtState.name +" AgentStateEvent");
        scope.checkLogout(agtState);
        scope.agentState = agtState;

        let m = XucAgent.buildMoment(agtState.since);
        scope.agentState.momentStart = m.momentStart;
        scope.agentState.timeInState = m.timeInState;

        scope.possibleStates = XucAgentUser.getPossibleAgentStates();
      });
      externalEvent.registerConfirmQuitCallback(() => {
        quitApp = true;
        scope.switchState({name: logoutState, userStatus: null});
      });

      scope.getAgentStateClassName = function(state) {
        var icon = state.name === logoutState ? "fa-power-off" : "fa-chevron-down";
        return icon + ' state-' + _.kebabCase(state.name);
      };

      scope.switchState = function(state) {
        XucAgentUser.switchAgentState(state);
      };

      scope.displayTime = function(){
        return scope.agentState.name !== onCallState;
      };
    }
  };
}
