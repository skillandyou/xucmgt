export default function switchBoard(XucSwitchboard) { 

  return {
    restrict: 'E',
    templateUrl: 'assets/javascripts/ccagent/directives/switchBoard.html',
    link: (scope) => {
      scope.switchboardIncomingCalls = XucSwitchboard.getSwitchboardIncomingCalls();
      scope.switchboardHoldCalls = XucSwitchboard.getSwitchboardHoldCalls();

      scope.retrieve = (queueCall) => {
        XucSwitchboard.retrieveQueueCall(queueCall);
      };
    }
  };

}