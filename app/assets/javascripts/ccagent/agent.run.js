export const ELECTRON_DEFAULT_WIDTH = 362;
export const ELECTRON_DEFAULT_HEIGHT = 800;
export const ELECTRON_MINI_WIDTH = 72;
export const ELECTRON_MINI_HEIGHT = 675;
export const ELECTRON_THIRD_PARTY_MIN_WIDTH = 1000;
export const ELECTRON_QUIT_OPTIONS = {
  type: 'question',
  buttons: ['AGT_DIALOG_CANCEL', 'AGT_DIALOG_QUIT'],
  defaultId: 1,
  message: 'AGT_DIALOG_MSG',
  detail: 'AGT_DIALOG_DETAIL'
};

export default function run($rootScope, $uibModal, $state, errorModal, $window, XucLink, XucUser, $log, $translate, $transitions, CtiProxy, externalEvent, electronWrapper, remoteConfiguration){
  electronWrapper.setElectronConfig({width: ELECTRON_DEFAULT_WIDTH, height: ELECTRON_DEFAULT_HEIGHT, title: 'CC Agent'});
  $rootScope.$state = $state;

  $transitions.onStart({
    to: function(state) {
      return state.data !== null && state.data.requireLogin === true;
    }
  },function(trans) {
    if(!XucLink.isLogged()) {
      $log.info("Login required");
      return trans.router.stateService.target('login');
    }
    return true;
  });

  $transitions.onSuccess({ to: 'login' }, () => {
    electronWrapper.setElectronConfig({confirmQuit: {reset: true}});
    electronWrapper.setTrayIcon('logout');
  });

  $transitions.onSuccess({ from: 'login' }, () => {
    remoteConfiguration.getBooleanOrElse('logoffOnExit', false).then(value => {
      if (value) electronWrapper.setElectronConfig({confirmQuit: ELECTRON_QUIT_OPTIONS});
    });
    remoteConfiguration.get("version").then((serverVersion) => {
      if (serverVersion != $window.appVersion){
        $log.warn("Server version mismatch", serverVersion);
        $window.location.replace("/ccagent#!/login?error=Version");
      }
    });
    electronWrapper.setTrayIcon('default');
  });

  var unregisterFatalError = $rootScope.$on(CtiProxy.FatalError, () => {
    externalEvent.unRegisterExternalEventListener();
  });
  $rootScope.$on('$destroy', unregisterFatalError);
}
