import {ELECTRON_DEFAULT_WIDTH, ELECTRON_DEFAULT_HEIGHT, ELECTRON_MINI_WIDTH, ELECTRON_MINI_HEIGHT} from '../agent.run';

export default class MainController {

  constructor($sce, $log, $scope, $state, $interval, $window, $translate, XucAgentUser, XucUser, XucLink, CtiProxy, XucUtils, XucDirectory, XucSheet, callContext,
    agentCallbackHelper, agentActivitiesHelper, electronWrapper, XucCallNotification, focus, onHold, XucQueueTotal, remoteConfiguration, applicationConfiguration) {
    this.$scope = $scope;
    this.$state = $state;
    this.$window = $window;
    this.XucLink = XucLink;
    this.XucUser = XucUser;
    this.XucAgentUser = XucAgentUser;
    this.CtiProxy = CtiProxy;
    this.XucUtils = XucUtils;
    this.XucDirectory = XucDirectory;
    this.XucSheet = XucSheet;
    this.XucQueueTotal = XucQueueTotal;
    this.callContext = callContext;
    this.agentCallbackHelper = agentCallbackHelper;
    this.agentActivitiesHelper = agentActivitiesHelper;
    this.electronWrapper = electronWrapper;
    this.focus = focus;
    this.onHold = onHold;
    this.showExternalViewButton = false;
    this.appConfig = applicationConfiguration.getCurrentAppConfig();

    this.electronDefaultSize = { width: ELECTRON_DEFAULT_WIDTH, height: ELECTRON_DEFAULT_HEIGHT };
    this.toggled = false;

    this.agentActivitiesHelper.init(this.$scope);

    this.agentCallbackHelper.loadAllCallbacks();
    $interval(this.agentCallbackHelper.loadAllCallbacks, 3600000, 0, true); // every hour

    XucAgentUser.loginAgent();
    XucUser.getUserAsync().then((user) => {
      $scope.user = user;
      XucCallNotification.enableNotification();
    });

    XucAgentUser.whenAgentError().then(this.onAgentError.bind(this));

    $scope.$on('linkDisConnected', function () {
      $state.go('login', { 'error': 'LinkClosed' });
    });

    remoteConfiguration.get('externalViewUrl').then((resultUrl) => {
      if (resultUrl != "") {
        this.showExternalViewButton = true;
      } else {
        this.showExternalViewButton = false;
      }
    }, (error) => {
      $log.error(error);
    });

    CtiProxy.disableMediaKeys();

    this.show('activities');
  }

  onAgentError(event) {
    var params = {error: event.Error};
    if (event.Error === "LoggedInOnAnotherPhone") {
      params.phoneNumber = event.phoneNb;
    }
    this.XucLink.logout();
    this.$state.go("login", params);
  }


  onKeyPress(keyCode) {

    var setPhoneClass = () => {
      angular.element('#xuc_search i').removeClass('fa-search');
      angular.element('#xuc_search i').addClass('fa-phone');
    };

    var setSearchClass = () => {
      angular.element('#xuc_search i').removeClass('fa-phone');
      angular.element('#xuc_search i').addClass('fa-search');
    };

    var normalizedValue = this.XucUtils.normalizePhoneNb(this.$scope.searchValue || '');
    (this.XucUtils.isaPhoneNb(normalizedValue)) ? setPhoneClass() : setSearchClass();

    if (keyCode == 13) {
      angular.element('#search').blur();
      this.searchOrDial();
    }
  }

  searchOrDial(){
    const input = this.XucUtils.reshapeInput(this.$scope.searchValue);
    if (input.type === 'phone') {
      this.callContext.dialOrAttTrans(input.value);
    }
    else {
      this.XucDirectory.directoryLookup(input.value);
      this.show('search');
    }
  }

  isActive(contentName) {
    return this.$state.includes('content.'+contentName);
  }

  show(contentName) {
    if (this.toggled){
      this.toggleWindow();
    }
    return this.$state.go('content.'+contentName).then((tResult)=> {
      if(tResult.name === 'content.search') {
        this.focus('search');
      }
    });
  }

  getCallbackStatus() {
    return this.agentCallbackHelper.getStatus();
  }

  getWaitingCalls() {
    let stats = this.XucQueueTotal.getCalcStats();
    return this.$scope.waitingCalls = stats ? stats.sum.WaitingCalls : 0;
  }

  isElectron() {
    return this.electronWrapper.isElectron();
  }

  toggleWindow() {
    if (!this.toggled){
      this.electronWrapper.setElectronConfig({width: ELECTRON_MINI_WIDTH, height: ELECTRON_MINI_HEIGHT, minimalist: true});
    }
    else {
      this.electronWrapper.setElectronConfig(this.electronDefaultSize);
    }
    this.toggled = !this.toggled;
  }

  showVersion() {
    return this.$window.appVersion;
  }

  displayAudioSettings() {
    return this.CtiProxy.isUsingWebRtc();
  }

  emptyInput(){
    this.$scope.searchValue = "";
    this.onKeyPress("");
  }

}
