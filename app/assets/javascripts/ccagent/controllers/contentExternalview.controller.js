export default class ContentExternalviewController {

  constructor(remoteConfiguration, $log) {
    remoteConfiguration.get('externalViewUrl').then((result) => {
      this.externalViewUrl = result;
    }, (error) => {
      $log.error(error);
    });
  }
}
