export default class LoginController {

  constructor($state, $stateParams, XucAgentUser, CtiProxy, electronWrapper, remoteConfiguration) {
    this.xucAgentUser = XucAgentUser;
    this.error = $stateParams.error;
    this.errorParam = $stateParams.phoneNumber;
    this.$state = $state;
    this.ctiProxy = CtiProxy;

    /* eslint-disable no-undef */
    this.hostAndPort = externalConfig.hostAndPort;
    this.useSso = externalConfig.useSso;
    this.casServerUrl = externalConfig.casServerUrl;
    this.casLogoutEnable = externalConfig.casLogoutEnable;
    if (externalConfig.switchboard) remoteConfiguration.setSwitchBoard();
    /* eslint-enable no-undef */

    electronWrapper.setElectronConfig({confirmQuit: null});
  }

  onLogin() {
    this.$state.go('content');
  }
}
