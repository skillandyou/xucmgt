import moment from 'moment';

export default class ContentHistoryController {

  constructor($scope, XucCallHistory, $translate, callContext) {
    this.$scope = $scope;
    this.XucCallHistory = XucCallHistory;
    this.$translate = $translate;
    this.callContext = callContext;
    this.labels = {};

    $translate(['AGT_HISTORY_TODAY', 'AGT_HISTORY_YESTERDAY', 'AGT_HISTORY_LAST']).then((translations) => {
      this.labels.today = translations.AGT_HISTORY_TODAY;
      this.labels.yesterday = translations.AGT_HISTORY_YESTERDAY;
      this.labels.last = translations.AGT_HISTORY_LAST;
    });

    this.init();
  }

  init() {
    this.$scope.isLoading = true;
    this.XucCallHistory.subscribeToAgentCallHistory(this.$scope, callHistory => {
      this.$scope.history = callHistory;
      this.$scope.isLoading = false;
    });
    this.XucCallHistory.updateAgentCallHistory();
  }

  getCallStatus(status) {
    var baseUrl = '/assets/images/ccagent/history/call_status_';
    if (status == 'emitted') return baseUrl + 'outgoing.svg';
    else if (status == 'answered') return baseUrl + 'incoming.svg';
    else if (status == 'missed' || status == 'abandoned' || status == 'exit_with_key') return baseUrl + 'missed.svg';
    return '';
  }

  dial(number) {
    this.callContext.normalizeDialOrAttTrans(number);
  }

  formatHistoryDay(date) {
    return moment(date).calendar(null, {
      sameDay: this.labels.today,
      lastDay: this.labels.yesterday,
      lastWeek: this.labels.last,
      sameElse: 'DD/MM/YYYY'
    });
  }

  formatHistoryTime(date) {
    return moment(date).format('HH:mm');
  }
}
