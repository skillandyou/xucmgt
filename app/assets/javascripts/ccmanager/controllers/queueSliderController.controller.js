(function() {
  'use strict';

  angular.module('ccManager').controller('queueSliderController', queueSliderController);

  queueSliderController.$inject = ['$scope', '$log', 'Thresholds'];

  function queueSliderController($scope, $log, thresholds) {
    $log.debug('queueSliderController');


    var _saveCounterThreshold = function(counter, min, max) {
      thresholds.saveThreshold(counter, min, max);
    };

    $scope.getValues = function() {
      
      var _initSlider = function(counter) {
        $scope.sliders[counter] = {};
        $scope.sliders[counter].min = 0;
        $scope.sliders[counter].max = 0;
        $scope.sliders[counter].values = [];
        $scope.sliders[counter].options = {range : true, stop:_buildOnStop(counter) };
      };

      $scope.sliders = {};
      var counters = thresholds.getThresholdDefs();
      for (var i= 0; i < counters.length; i++) {
        var counter = counters[i];
        var thr = thresholds.loadThreshold(counter);
        _initSlider(counter);
        $scope.sliders[counter].values = [thr.low, thr.high];
        $scope.updateMax(counter,thr.high);
      }
    };

    $scope.updateMax = function(counter, high) {
      var range =  $scope.sliders[counter].max -  $scope.sliders[counter].min;
      $log.debug($scope.sliders[counter]);
      if (high >= $scope.sliders[counter].max) {
        $scope.sliders[counter].max = (Math.floor((high + Math.floor(range*0.5))/10) +1)*10;
      }
    };

    $scope.leftWidth = function(counter) {
      var range =  $scope.sliders[counter].max -  $scope.sliders[counter].min;
      return ($scope.sliders[counter].values[0]/range)*100;
    };
    $scope.rightWidth = function(counter) {
      var range =  $scope.sliders[counter].max -  $scope.sliders[counter].min;
      return 100 - ($scope.sliders[counter].values[1]/range)*100;
    };

    $scope.onStop = function(counter, lowValue, highValue) {
      _saveCounterThreshold(counter, lowValue, highValue);
      $scope.updateMax(counter, highValue);
    };

    var _buildOnStop = function(counterName) {
      return function(event, ui) {
        $scope.onStop(counterName, ui.values[0], ui.values[1]);
      };
    };

    $scope.getValues();

  }
})();
