import $ from 'jquery';

(function(){
  'use strict';

  angular
  .module('ccManager')
  .controller('ccCallbackController',ccCallbackController);

  ccCallbackController.$inject= ['$scope','XucCallback', 'Preferences', '$log', '$window', '$filter', "$location", "$anchorScroll"];

  function ccCallbackController($scope, xucCallback, Preferences, $log, $window, $filter, $location, $anchorScroll) {
    $anchorScroll.yOffset = 70;
    $scope.callbackLists = [];

    $scope.getServerBaseUrl = function(hostAndPort) {
      return $filter('prepareServerUrl')($window.location.protocol, hostAndPort, 'http');
    };

    $scope.refreshCallbacks = function() {
      $log.debug('refreshCallbacks');
      delete $scope.oldestCallback;
      $scope.callbacksCount = 0;
      $scope.callbackLists = xucCallback.getCallbackLists().filter((list) => {
        return Preferences.isQueueSelected(list.queueId);
      });
      $scope.callbackLists.forEach((list) => {
        list.callbacks.forEach((c) => {
          $scope.callbacksCount++;
          if (!$scope.oldestCallback || c.dueDate < $scope.oldestCallback.dueDate) {
            $scope.oldestCallback = c;
          }
        });
      });
    };

    $scope.refreshCallbacksAndApply = function() {
      $scope.refreshCallbacks();
    };

    $scope.upload = function(listUuid) {
      var charset = 'utf-8';
      if(window.navigator.platform == 'Win32') {
        charset = 'iso-8859-1';
      }
      $.ajax({
        type : "POST",
        contentType: "text/plain;charset=" + charset,
        data: $('#import-'+listUuid)[0].files[0],
        /* eslint-disable no-undef */
        url : $scope.getServerBaseUrl(externalConfig.hostAndPort) + '/xuc/api/1.0/callback_lists/' + listUuid + '/callback_requests/csv',
        /* eslint-enable no-undef */
        processData: false,
        error: function(e) {
          $log("Error uploading call back list !!!!!!!");
          $log(e);
        },
        success: function() {
          xucCallback.refreshCallbacks();
        }
      });
    };

    $scope.gotoAnchor = function(id) {
      $anchorScroll('import-'+id);
    };

    $scope.$on('CallbacksLoaded', $scope.refreshCallbacksAndApply);
    $scope.$on('CallbackTaken', $scope.refreshCallbacksAndApply);
    $scope.$on('CallbackReleased', $scope.refreshCallbacksAndApply);
    $scope.$on('CallbackClotured', $scope.refreshCallbacksAndApply);
    $scope.$on('preferences.queueSelected', $scope.refreshCallbacksAndApply);

    xucCallback.refreshCallbacks();
  }

})();
