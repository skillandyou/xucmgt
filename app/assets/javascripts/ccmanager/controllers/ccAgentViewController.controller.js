(function() {
  'use strict';

  angular
    .module('ccManager')
    .controller('ccAgentViewController', ccAgentViewController);

  ccAgentViewController.$inject = ['$scope', 'NgTableParams', '$filter', 'XucAgent', 'Preferences', '$q'];

  function ccAgentViewController($scope, NgTableParams, $filter, xucAgent, preferences, $q) {

    $scope.agents = [];

    var selectAgentsFromQueueSelected = function() {
      var qIds = preferences.getSelectedQueues();
      $scope.agents = xucAgent.getAgentsInQueueByIds(qIds);
      $scope.agentTable.reload();
    };

    $scope.tableSettings = function() {
      var calculatePage = function(params) {
        if (params.total() <= (params.page() - 1) * params.count()) {
          var setPage = (params.page() - 1) > 0 && (params.page() - 1) || 1;
          params.page(setPage);
        }
      };
      return {
        total: $scope.agents.length, // length of data
        counts: [],
        getData: function(params) {
          var filteredData = params.filter() ?
            $filter('filter')($scope.agents, params.filter()) : $scope.agents;
          var orderedData = params.sorting() ?
            $filter('orderBy')(filteredData, params.orderBy()) : $scope.agents;
          params.total(orderedData.length);
          calculatePage(params);
          return orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count());
        }
      };
    };

    $scope.agentTable = new NgTableParams(
      {
        page: 1,
        count: 50,
        sorting: {
          firstName: 'asc'
        }
      },
      $scope.tableSettings()
    );

    $scope.states = function() {
      var def = $q.defer(),
        statesDef = ['AgentReady', 'AgentOnIncomingCall', 'AgentOnOutgoingCall', 'AgentOnAcdCall', 'AgentOnWrapup', 'AgentRinging', 'AgentLoggedOut', 'AgentDialing', 'AgentOnPause'],
        states = [];
      angular.forEach(statesDef, function(state) {
        states.push({
          'id': state,
          'title': state
        });
      });
      def.resolve(states);
      return def;
    };

    $scope.$on('AgentStatisticsUpdated', function() {
      selectAgentsFromQueueSelected();
    });

    $scope.$on('AgentStateUpdated', function() {
      selectAgentsFromQueueSelected();
    });

    $scope.$on('AgentsLoaded', function() {
      selectAgentsFromQueueSelected();
    });

    $scope.$on('QueueMemberUpdated', function() {
      selectAgentsFromQueueSelected();
    });

    $scope.$on('preferences.queueSelected', function() {
      selectAgentsFromQueueSelected();
    });
  }
})();
