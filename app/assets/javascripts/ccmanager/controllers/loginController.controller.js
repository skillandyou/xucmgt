(function() {
  'use strict';
  angular.module('ccManager').controller('LoginController', ['$scope', '$state', '$stateParams',
    function($scope, $state, $stateParams) {

      $scope.error = $stateParams.error;

      $scope.onLogin = function() {
        $state.go('interface');
      };
      /* eslint-disable no-undef */
      $scope.hostAndPort = externalConfig.hostAndPort;
      $scope.useSso = externalConfig.useSso;
      $scope.casServerUrl = externalConfig.casServerUrl;
      $scope.casLogoutEnable = externalConfig.casLogoutEnable;
      /* eslint-enable no-undef */
    }
  ]);
})();
