import _ from 'lodash';

(function() {
  'use strict';

  angular.module('ccManager').filter('agentFilter', agentFilter);

  agentFilter.$inject = [ '$filter' ];

  function agentFilter($filter) {

    var _filterAgents = function(agents, states) {
      if (states.length === 0)
        return agents;
      var items = {
        states : [],
        out : []
      };
      angular.forEach(states, function(state) {
        if (state.id)
          this.states.push(state.id);
      }, items);
      angular.forEach(agents, function(agent) {
        if (this.states.indexOf(agent.state) >= 0 || this.states.indexOf(agent.status) >= 0) {
          this.out.push(agent);
        }
      }, items);
      return items.out;
    };

    var _filterArray = function(agents, property, matches) {
      if(matches.length === 0) return agents;

      var matchArray = _.filter(_.map(matches, "id"), function(o) {return o;});
      var result = [];
      angular.forEach(agents, function(agent) {
        if(matchArray.indexOf(agent[property]) !== -1) result.push(agent);
      });

      return result;
    };

    return function(agents, filters) {
      var remainingFilters = {};
      var filteredData = agents;

      _.forEach(filters, function(filter, key) {
        if(key == "stateName") {
          filteredData = _filterAgents(filteredData, filters[key]);
        } else if(_.isArray(filters[key])){
          filteredData = _filterArray(filteredData, key, filters[key]);
        } else {
          remainingFilters[key] = filters[key];
        }
      });

      return remainingFilters ? $filter('filter')(filteredData, remainingFilters) : filteredData;
    };
  }
})();
