
function checkKerberosAuthenticationMode(url, autoSubmit) {
    var request = {
        url: url + "/xuc/sso/1.0/login",
        xhrFields: {
            withCredentials: true
        }
    };
    $.ajax(request).done(function(result) {
        console.log(result);
        if(result && result.success) {
            $("[name='authenticationMode']").val("kerberos");
            $("[name='ctiUsername']").val(result.login);
            $("[name='ctiUsername']").prop("readonly", true);
            $("#passwordRow").hide();
            if(autoSubmit) {
                $("#loginForm").submit();
            }
        }
    });
}

function checkAutologin(url, isAgent) {
    if (typeof(Storage) !== "undefined" && typeof(localStorage.xucToken) !== "undefined") {
        $("[name='authenticationMode']").val("token");
        $("[name='ctiUsername']").val("________");
        $("[name='ctiUsername']").prop("readonly", true);
        $("#passwordRow input").prop("readonly", true);
        $("#passwordRow input").val("________");
        $("#alreadyLoggedIn").show();
        
        if(isAgent) {
          if(typeof(localStorage.agentNumber) !== "undefined") {
            $("[name='phoneNumber']").val(localStorage.agentNumber);
            $("#loginForm").submit();
          }
        } else {
          $("#loginForm").submit();
        }
    }
    else {
        $("#alreadyLoggedIn").hide();
    }
}
