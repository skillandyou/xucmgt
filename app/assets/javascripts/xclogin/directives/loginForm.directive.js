var login = angular.module('xcLogin');
login.directive('loginForm', function(XucLink, $translate, $log, $timeout, $window){
  return {
    restrict: 'E',
    templateUrl: 'assets/javascripts/xclogin/directives/loginForm.html',
    scope: {
      requirePhoneNumber: "@",
      title:"@",
      onLogin: "&",
      errorCode: "=",
      errorParam: "=",
      hostAndPort: "=",
      useSso: "=",
      casServerUrl: "=",
      casLogoutEnable: "="
    },
    link: function(scope) {

      scope.error = null;
      scope.username = "";
      scope.password = "";
      scope.phoneNumber = "";
      scope.persistCredentials = "";
      scope.autoLogin = false;
      scope.requireCredentials = true;
      var defaultAutoLoginTimeout = 5;
      var maxAutoLoginTimeout = 60;
      var retryAutoLoginTimeout = defaultAutoLoginTimeout;
      var retryAutoLoginPromise = null;
      scope.retryAutoLoginDelay = retryAutoLoginTimeout;
      scope.retryAutoLogin = false;
      scope.translateData = { retryAutoLoginDelay: scope.retryAutoLoginDelay };

      if(typeof(scope.errorCode) !== "undefined") {
        scope.error = {error: scope.errorCode, message: ''};
      }

      
      if(Cti.webSocket) {
        Cti.close();
      }

      var _updateTranslateData = function() {
        scope.translateData = { retryAutoLoginDelay: scope.retryAutoLoginDelay };
      };

      var _loginAndRedirect = function(event) {
        if(event.name == "ctiLoggedOn") {
          scope.error = null;
          scope.onLogin();
        }
      };

      var _isNumberValid = function(number) {
        if(typeof(number) !== "undefined" && angular.isString(number) && number.length > 0) {
          return true;
        } else {
          return false;
        }
      };

      var _getSsoCredentials = function(credentialPromise) {
        return credentialPromise.then(function(ssoCredentials) {
          scope.requireCredentials = false;
          scope.ssoCredentials = ssoCredentials;
          scope.username = ssoCredentials.login;
          return ssoCredentials;
        });
      };

      scope.login = function() {
        scope.error = null;
        scope.autoLogin = false;
        scope.retryAutoLogin = false;
        if(retryAutoLoginPromise !== null) {
          $timeout.cancel(retryAutoLoginPromise);
          retryAutoLoginPromise = null;
        }
        if(scope.requirePhoneNumber && scope.phoneNumber.length === 0) {
          scope.error = {error: 'RequirePhoneNumber', message: ''};
        } else {
          if(scope.ssoCredentials) {
            XucLink.loginWithSso(scope.ssoCredentials, scope.phoneNumber, scope.persistCredentials).catch(function (error) {
              $log.error("error: ", error);
              scope.error = error;
            });
          } else {
            XucLink.login(scope.username, scope.password, scope.phoneNumber, scope.persistCredentials).catch(function (error) {
              console.log("error: ", error);
              scope.error = error;
            });
          }
        }
      };

      var _canPerformAutoLogin = function() {
        scope.storedCredentials = XucLink.getStoredCredentials();
        if(scope.storedCredentials !== null && (scope.error === null || scope.error.error === 'LinkClosed' || scope.error.error === 'NoResponse')) {
          return true;
        } else {
          return false;
        }
      };

      var _tryAutoLoginDelayed = function() {
        if(_canPerformAutoLogin()) {
          $log.debug("retrying connection in " + retryAutoLoginTimeout + " seconds");
          scope.retryAutoLoginDelay = retryAutoLoginTimeout;
          retryAutoLoginTimeout = retryAutoLoginTimeout * 2;
          if(retryAutoLoginTimeout >= maxAutoLoginTimeout) {
            retryAutoLoginTimeout = maxAutoLoginTimeout;
          }
          scope.retryAutoLogin = true;
          _updateTranslateData();
          retryAutoLoginPromise = $timeout(_autoLoginCountdown, 1000);
        }
      };

      var _autoLoginCountdown = function() {
        scope.retryAutoLoginDelay--;
        retryAutoLoginPromise = null;
        _updateTranslateData();
        if(scope.retryAutoLoginDelay <=0) {
          scope.forceAutoLogin();
        } else {
          retryAutoLoginPromise = $timeout(_autoLoginCountdown, 1000);
        }
      };

      scope.getToken = function (url) {
        if (url !== undefined) {
          if (url.includes('token=')) {
            var urlArr = url.split('token=');
            var token = String(urlArr[1]).split('#!');
            return token[0];
          }
        }
      };

      scope.forceAutoLogin = function() {
        if(retryAutoLoginPromise !== null) {
          $timeout.cancel(retryAutoLoginPromise);
          retryAutoLoginPromise = null;
        }
        scope.error = null;
        _tryAutoLogin();
      };

      scope.showVersion = function() {
        return $window.appVersion;
      };
      
      var _tryAutoLogin = function() {
        scope.autoLogin = false;
        scope.retryAutoLogin = false;
        var token = scope.getToken($window.location.href);
        if (token) {
          scope.storedCredentials = {
            "token": token,
            "user": ""
          };
          XucLink.storeCredentials(scope.storedCredentials);
        }
        if (scope.storedCredentials !== null) {
          scope.username = scope.storedCredentials.login;
          scope.phoneNumber = scope.storedCredentials.phoneNumber;
          scope.persistCredentials = true;
          if(scope.useSso) {
            _getSsoCredentials(XucLink.getSsoCredentials());
          }
          if(_canPerformAutoLogin()) {
            if(!scope.requirePhoneNumber || _isNumberValid(scope.storedCredentials.phoneNumber)) {
              scope.autoLogin = true;
              scope.error = null;
              XucLink.loginWithStoredCredentials().catch(function (error) {
                $log.error("Automatic login error: ", error);
                scope.autoLogin = false;
                scope.error = error;
                _tryAutoLoginDelayed();
              });
            }
          }
        } else if(scope.useSso) {
          _getSsoCredentials(XucLink.getSsoCredentials()).then(function(ssoCredentials) {
            if(!scope.requirePhoneNumber && scope.error === null) {
              scope.autoLogin = true;
              XucLink.loginWithSso(scope.ssoCredentials, "0", false).catch(function (error) {
                $log.error("SSO error: ", error);
                scope.error = error;
                scope.autoLogin = false;
              });
            }
          });
        } else if(angular.isDefined(scope.casServerUrl)) {
          scope.autoLogin = true;
          scope.requireCredentials = false;
          var lastError = XucLink.parseUrlParameters($window.location.search).lastError;
          if(angular.isDefined(lastError)) {
            if(lastError == "Logout" && scope.casLogoutEnable) {
              XucLink.logoutFromCas(scope.casServerUrl);
              return;
            } else {
              scope.error = {error: lastError, message: ''};
            }
          }

          var param = (scope.error !== null)? '?lastError=' + scope.error.error: '';
          var casCredentials = XucLink.getCasCredentials(scope.casServerUrl, param);
          _getSsoCredentials(casCredentials).then(function(ssoCredentials) {
            scope.requireCredentials = false;
            if(!scope.requirePhoneNumber && scope.error === null) {
              scope.autoLogin = true;
              XucLink.loginWithSso(scope.ssoCredentials, "0", false).catch(function (error) {
                $log.error("CAS SSO error: ", error);
                scope.error = error;
                scope.autoLogin = false;
              });
            } else {
              scope.autoLogin = false;
            }
          }).catch(function(error) {
            scope.autoLogin = false;
            scope.error = error;
            scope.requireCredentials = true;
          });
        }
      };

      var _abortLogin = function() {
        scope.autoLogin = false;
        if(scope.error === null) {
          scope.error = {error: 'NoResponse', message: 'No response from server'};
        }
        _tryAutoLoginDelayed();
      };


      scope.$on('ctiLoggedOn', _loginAndRedirect);
      scope.$on('linkDisConnected', _abortLogin);
      $log.info("Host : " + scope.hostAndPort);
      if (angular.isDefined(scope.hostAndPort)) {
        XucLink.setHostAndPort(scope.hostAndPort);
      }
      XucLink.setRedirectToHomeUrl(false);
      scope.storedCredentials = XucLink.getStoredCredentials();
      _tryAutoLogin();
    }
  };
});
