export default function showFlashText(XucFlashText){
  return {
    restrict: 'E',
    templateUrl: 'assets/javascripts/xchelper/directives/showFlashText.html',
    scope: {
      user: '@user',
      displayName: '@displayName'
    },
    link: (scope) => {

      const getFlashText = () => {
        scope.parties = XucFlashText.getFlashTexts();
      };

      const getPartyFlashTexts = (user, displayName) => {
        scope.singleParty = XucFlashText.getPartyFlashTexts(user, displayName);
        document.querySelector('.flashtext-text').focus();
      };

      if (scope.user == undefined || scope.user == null) {
        getFlashText();
        scope.inPartyChat = false;
      } else {
        getPartyFlashTexts(scope.user, scope.displayName);
        XucFlashText.setCurrentParty(scope.user);
        scope.inPartyChat = true;
      }

      scope.setPartyAsRead = (party) => {
        party.isRead = true;
      };

      XucFlashText.subscribeToFlashText(scope, getFlashText);

      scope.checkForEnter = ($event, displayName) => {
        if ($event.keyCode === 13) {
          $event.preventDefault();
          if (scope.areaMessage != '') {
            $event.shiftKey ? scope.areaMessage += '\n' : scope.sendMessage(displayName);
          }
        }
      };

      scope.sendMessage = (displayName) => {
        XucFlashText.sendFlashText(scope.user, scope.areaMessage, displayName, 'outgoing');
        scope.areaMessage = '';
      };
    }
  };
}