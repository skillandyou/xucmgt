import _ from 'lodash';

export default function callActionDropdown(){
  return {
    restrict: 'E',
    templateUrl: 'assets/javascripts/xchelper/directives/callActionDropdown.html',
    scope: {
      contact: "=",
      phoneNumber: "=",
      stacked: "=",
      emailMaxChars: '@',
      enableVideo : '=',
      enableFlashtext : '='
    },
    link: function(scope) {

      scope.phoneNumbers = [];
      scope.contact.displayName = scope.contact.entry[0];

      var init = function() {
        if(_.isObject(scope.contact)) {
          _.forEach([1, 2, 3], function (index) {
            if (!_.isEmpty(scope.contact.entry[index])) {
              scope.phoneNumbers.push(scope.contact.entry[index]);
            }
          });

          if (!_.isEmpty(scope.contact.entry[5])) {
            scope.email = scope.contact.entry[5];
          }
        }
      };
      init();

      scope.canShowFlashtext = () => {
        var s = scope.contact.status;
        return s !== 4 && s !== -1 && s !== -2;
      };
    }
  };
}
