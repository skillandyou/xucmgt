export default function isPhoneNumber(XucUtils) {

  return {
    require: 'ngModel',
    link: function($scope, elm, attrs, ctrl) {
      ctrl.$validators.isPhoneNumber = function(modelValue, viewValue) {
       
        if (ctrl.$isEmpty(modelValue)) {
          return true;
        }

        if (XucUtils.isaPhoneNb(XucUtils.normalizePhoneNb(viewValue))) {
          return true;
        } else {
          return false;
        }
      }; 
    }
  };
}

      