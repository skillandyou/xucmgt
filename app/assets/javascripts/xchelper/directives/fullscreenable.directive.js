export default function fullscreenable() {

  return {
    restrict: 'EA',
    transclude: false,
    controller: ($scope, $element, $log, $document, electronWrapper) => {

      $scope.elem = $element[0];
      $scope._document = $document;
      $log.debug('elem: ', $scope.elem);

      $scope._requestFullScreen = function() {
        if(electronWrapper.isElectron()) {
          electronWrapper.allowFullscreen().then(() => {
            $log.debug('Going fullscreen when electron is ready');
            $scope.elem.webkitRequestFullscreen();
          });
        } else {
          $log.debug('Going fullscreen');
          $scope.elem.webkitRequestFullscreen();
        }
      };

      $scope._exitFullScreen = function() {
        $log.debug('Leaving fullscreen');
        $scope._document[0].webkitExitFullscreen();
        if(electronWrapper.isElectron()) {
          electronWrapper.disableFullscreen();
        }
      };

      $scope.toggleFullscreen = function() {
        if (!$scope._document[0].webkitIsFullScreen) {
          $scope._requestFullScreen();
        } else {
          if ($scope._document[0].webkitExitFullscreen) {
            $scope._exitFullScreen();
          }
          else {
            $log.error('Unable to exit fullscreen, webkit method unavailable');
            alert('Unable to exit fullscreen, use Esc key');
          }
        }

      };
    }
  };
}
