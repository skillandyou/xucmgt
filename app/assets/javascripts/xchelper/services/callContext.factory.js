export default function callContext(CtiProxy, XucUtils, XucPhoneState) {

  var _dialOrAttTrans = (normalizedNumber, vars, video) => {
    if (XucPhoneState.getCalls().length === 0) {
      CtiProxy.dial(normalizedNumber, vars || {}, _getVideoValue(video));
    } else {
      CtiProxy.attendedTransfer(normalizedNumber);
    }
  };

  var _getVideoValue = (video) => {
    return (typeof video === 'undefined' || video === null) ? false : video;
  };

  var _normalizeDialOrAttTrans = (number, vars, video) => {
    var normalizedNumber = XucUtils.normalizePhoneNb(number);
    if (XucUtils.isaPhoneNb(normalizedNumber)) {
      _dialOrAttTrans(normalizedNumber, vars, _getVideoValue(video));
    }
  };

  return {
    dialOrAttTrans: _dialOrAttTrans,
    normalizeDialOrAttTrans: _normalizeDialOrAttTrans
  };
}
