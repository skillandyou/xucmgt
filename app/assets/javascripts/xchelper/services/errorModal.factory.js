export default function errorModal($uibModal) {

  var _showErrorModal = function (errorMessage) {
    return $uibModal.open({
      templateUrl: 'assets/javascripts/xchelper/services/errorModal.html',
      size: 'sm',
      windowClass: 'fatal-error',
      controller: 'FatalErrorController',
      resolve: {
        errorMessage: function () {
          return errorMessage;
        }
      }
    });
  };

  return {
    showErrorModal: _showErrorModal
  };

}
