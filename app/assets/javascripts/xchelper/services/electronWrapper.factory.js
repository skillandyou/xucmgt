import _ from 'lodash';

export default function electronWrapper($window, $q, $translate, externalEvent) {
  const obj = $window;
  const path = 'setElectronConfig';
  const isElectron = _.isFunction(_.get(obj, path));
  let currentIcon;

  var promiseFullscreen;

  var _onFullscreenReceived = () => {
    promiseFullscreen.resolve();
  };

  const _isElectron = () => {
    return isElectron;
  };

  const _setFocus = () => {
    _execFn({focus : true});
  };

  const _allowFullscreen = () => {
    _execFn({allowFullscreen : true});
    promiseFullscreen = $q.defer();
    return promiseFullscreen.promise;
  };

  const _disableFullscreen = () => {
    _execFn({disableFullscreen : true});
  };

  const _forceQuit = () => {
    _execFn({forceQuit : true});
  };

  const _setTrayIcon = (icon) => {
    if (icon != currentIcon) {
      _execFn({trayIcon : icon});
    }
    currentIcon = icon;
  };

  const _runExecutable = (path, args) => {
    _execFn({runExecutable: path, executableArgs: args});
  };

  const _setElectronConfig = ({width, height, title, minimalist = false, confirmQuit}) => {
    let data = {};
    Object.assign(data,
      title && {title: title},
      (width && height) && {size: {w: width, h: height, mini: minimalist}},
      confirmQuit && {confirmQuit: translateDialog(confirmQuit)}
    );
    _execFn(data);
  };

  const translateDialog = (confirmQuit) => {
    let dialog = {};

    angular.forEach(confirmQuit, (value, key) => {
      switch (key) {
      case 'message':
      case 'detail':
        dialog[key] = $translate.instant(value);
        break;

      case 'buttons':
        dialog[key] = value.map(i => $translate.instant(i));
        break;

      default:
        dialog[key] = value;
      }
    });

    return dialog;
  };

  let _execFn = function() {
    if (isElectron) _.spread(_.bindKey(obj, path))(arguments);
  };

  externalEvent.registerFullscreenCallback(_onFullscreenReceived);

  return {
    isElectron: _isElectron,
    forceQuit: _forceQuit,
    setElectronConfig: _setElectronConfig,
    setFocus: _setFocus,
    setTrayIcon: _setTrayIcon,
    runExecutable: _runExecutable,
    allowFullscreen: _allowFullscreen,
    disableFullscreen: _disableFullscreen
  };
}
