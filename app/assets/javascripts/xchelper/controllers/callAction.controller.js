export default class CallAction {

  constructor($scope, XucPhoneState, XucUtils, CtiProxy, callContext) {
    $scope.dial = function(dst, vars, video) {
      callContext.normalizeDialOrAttTrans(dst, vars || {}, video || false);
    };

    $scope.attendedTransfer = function(dst) {
      CtiProxy.attendedTransfer(XucUtils.normalizePhoneNb(dst));
    };

    $scope.directTransfer = function(dst) {
      CtiProxy.directTransfer(XucUtils.normalizePhoneNb(dst));
    };
  }
}
