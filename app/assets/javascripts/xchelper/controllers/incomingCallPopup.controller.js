export default class IncomingCallPopup {

  constructor($scope, $uibModalInstance, $state, XucPhoneEventListener, XucPhoneState,incomingCallNumber, incomingCallName, callType, CtiProxy) {
    var _closing = false;

    $scope.number = incomingCallNumber;
    $scope.name = incomingCallName;
    $scope.calls = XucPhoneState.getCalls();
    $scope.callType = callType;
    $scope.maxAnswerableCalls = CtiProxy.getMaxAnswerableCalls();

    $scope.accept = function() {
      _closing = true;
      CtiProxy.answer();
      $uibModalInstance.dismiss('accept');
    };

    $scope.decline = function() {
      _closing = true;
      CtiProxy.hangup();
      $uibModalInstance.dismiss('decline');
    };

    $scope.cancel = function() {
      _closing = true;
      $uibModalInstance.dismiss('cancel');
    };

    $scope.isVideo = function() {
      return ($scope.callType===xc_webrtc.mediaType.AUDIOVIDEO);
    };

    var _autoClose = function() {
      if(!_closing) {
        _closing = true;
        $uibModalInstance.dismiss('autoclose');
      }
    };

    XucPhoneEventListener.addEstablishedHandler($scope, _autoClose);
    XucPhoneEventListener.addReleasedHandler($scope, _autoClose);

  }
}
