import moment from 'moment';
export default function timestampToHoursMinutes() {
  return function (input) {
    if (!input) return "";
    return moment(input, 'YYYY-MM-DD hh:mm:ss').format('HH:mm');
  };
}