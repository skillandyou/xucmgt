export default function hour($filter) {
  return function(input) {
    if(!input) return "";
    var _date = $filter('date')(new Date(input), 'HH:mm');
    return _date.toUpperCase();
  };
}
