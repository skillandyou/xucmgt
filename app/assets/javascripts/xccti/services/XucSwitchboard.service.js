import _ from 'lodash';

export default function XucSwitchboard($log, $q, XucLink, XucAgentUser, XucQueue, XucUser, XucAgent) {
  let _switchboardRegistered = false;
  let _switchboardIncomingCalls = [];
  let _switchboardHoldCalls = [];
  let orderedQueues;
  let incomingQueue;
  let holdQueue;

  $log.debug('XucSwitchboard service initialization');

  const _unregisterOnSwitchboard = () => {
    if (_switchboardRegistered == true) {
      Cti.unSubscribeToQueueCalls(orderedQueues[1]);
      Cti.unSubscribeToQueueCalls(orderedQueues[2]);
      _switchboardRegistered = false;
    }
  };

  const _getHoldQueue = (incomingQueueName) => {
    var defer = $q.defer();
    XucQueue.getQueuesAsync().then(allQueues => {
      XucUser.getUserAsync().then(user => XucAgent.getAgentAsync(user.agentId).then(agent => {
        let holdQueue = allQueues.find(queue => queue.name == incomingQueueName + '_hold_' + agent.number);
        if (holdQueue) {
          defer.resolve(holdQueue);
        } else {
          holdQueue = allQueues.find(queue => queue.name == incomingQueueName + '_hold');
          if (holdQueue) {
            defer.resolve(holdQueue);
          } else {
            defer.reject('No hold queue defined');
          }
        }
      }));
    });
    return defer.promise;
  };

  const _getSwitchboardIncomingCalls = () => {
    return _switchboardIncomingCalls;
  };

  const _getSwitchboardHoldCalls = () => {
    return _switchboardHoldCalls;
  };

  const _retrieveQueueCall = (queueCall) => {
    Cti.retrieveQueueCall(queueCall);
  };

  const _registerOnSwitchboard = () => {
    XucAgentUser.getQueuesAsync().then(queues => {
      if (queues.length == 1) {
        incomingQueue = queues[0];
        _getHoldQueue(incomingQueue.name).then(res => {
          holdQueue = res;
          Cti.subscribeToQueueCalls(holdQueue.id);
          Cti.subscribeToQueueCalls(incomingQueue.id);
          Cti.setHandler(Cti.MessageType.QUEUECALLS, _processQueueCall);
          _switchboardRegistered = true;
        }, err => {
          $log.error(err);            
        });
      } else {
        $log.error('Agent must be logged in one queue only');
      }
    });
  };

  const _processQueueCall = (queueCalls) => {
    if (queueCalls.queueId == incomingQueue.id) {
      _switchboardIncomingCalls.length = 0;
      _.merge(_switchboardIncomingCalls, queueCalls.calls);
    }
    else if (queueCalls.queueId == holdQueue.id) {
      _switchboardHoldCalls.length = 0;
      _.merge(_switchboardHoldCalls, queueCalls.calls);
    }
  };

  var _unInit = function () {
    _switchboardIncomingCalls = [];
    _switchboardHoldCalls = [];
    if (_unregisterOnSwitchboard === true) {
      _unregisterOnSwitchboard();
      $log.info("Unloading XucSwitchboard service");
    }
    XucLink.whenLogged().then(_init);
  };

  var _init = function () {
    if (_switchboardRegistered === false) {
      _registerOnSwitchboard();
      $log.info("Starting XucSwitchboard service");
    }

    XucLink.whenLoggedOut().then(_unInit);
  };

  XucLink.whenLogged().then(_init);

  return {
    getHoldQueue : _getHoldQueue,
    registerOnSwitchboard: _registerOnSwitchboard,
    processQueueCall: _processQueueCall,
    getSwitchboardIncomingCalls: _getSwitchboardIncomingCalls,
    getSwitchboardHoldCalls: _getSwitchboardHoldCalls,
    retrieveQueueCall: _retrieveQueueCall,
  };

}