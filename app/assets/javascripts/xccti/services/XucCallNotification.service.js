import moment from 'moment';

export default function XucCallNotification($log, $rootScope, XucLink, XucPhoneEventListener, XucPhoneState, webNotification, $translate, electronWrapper, onHoldNotifier, remoteConfiguration, XucFlashText) {

  var _notifications = [];

  var _isNotificationEnabled = false;
  var _onHoldNotificationTime = 0;

  var _onNotificationClick = function() {
    try {
      electronWrapper.setFocus();
      window.focus();
      _autoClose();
    } catch(ex) {
      $log.debug("Ignoring error when focusing window", ex);
    }
  };
  
  var _onRinging = function(event) {
    $log.debug("_onRinging ", event);
    if(_isNotificationEnabled) {
      var options = {
        body: event.otherDName ? event.otherDName + ' ' + event.otherDN : event.otherDN,
        icon: "/assets/images/incoming_call.ico",
        onClick: _onNotificationClick,
        autoClose: 0
      };
      webNotification.showNotification($translate.instant('IncomingNotificationTitle'), options, _onNotificationShown);
    }
  };

  var _onHold = function(event) {
    $log.debug("_onHold ", event);
    if(_isNotificationEnabled && _onHoldNotificationTime > 0) {
      onHoldNotifier.notify(_onHoldNotificationTime*1000, event).finally(
        () => {_autoClose();},
        (notify) => {
          var options = {
            body: notify.call.otherDName ? notify.call.otherDName + ' ' + notify.call.otherDN : notify.call.otherDN,
            icon: "/assets/images/on_hold.ico",
            onClick: _onNotificationClick,
            autoClose: 0
          };
          var timer = moment.duration(notify.count*_onHoldNotificationTime, 'seconds').format("h _, m _, s _", {trim: 'all'});
          webNotification.showNotification(
            $translate.instant('OnHoldNotificationTitle')+" "+timer,
            options, _onNotificationShown
          );
          $rootScope.$broadcast('OnHoldNotification', options.body, timer);
        }
      );
    }
  };

  var _onNotificationShown = function(error, hide) {
    if(error) {
      $log.debug('XucCallNotification error: ' + error.message);
    } else {
      _notifications.push(hide);
    }
  };

  var _onFlashTextReceived = (event) => {
    $log.debug("_onFlashTextReceived ", event);
    if(_isNotificationEnabled) {
      var options = {
        body: event.message,
        icon: "/assets/images/new_flashtext.ico",
        onClick: _onNotificationClick,
        autoClose: 0
      };
      webNotification.showNotification($translate.instant('ReceivedFlashText'), options, _onNotificationShown);
    }
  };

  var _autoClose = function() {
    onHoldNotifier.stop();
    angular.forEach(_notifications, function(hide) { hide(); });
    _notifications.length = 0;
  };

  var _enableNotification = function() {
    _isNotificationEnabled = true;
    remoteConfiguration.getIntOrElse('notifyOnHold', 0).then((value) => {
      _onHoldNotificationTime = value;
    });
    webNotification.allowRequest = false;
    if(!webNotification.permissionGranted) {
      window.Notification.requestPermission();
    }
  };

  var _disableNotification = function() {
    _isNotificationEnabled = false;
    _onHoldNotificationTime = 0;
    _autoClose();
  };

  var _getNotificationEnabled = function() { return _isNotificationEnabled; };

  var _unInit = function() {
    $log.info('Unloading XucCallNotification service');
    _autoClose();
    XucLink.whenLogged().then(_init);
  };

  var _init = function() {
    $log.info('Starting XucCallNotification service');
    remoteConfiguration.getIntOrElse('notifyOnHold', 0).then((value) => {
      _onHoldNotificationTime = value;
      XucPhoneState.getCallsOnHold().map(_onHold);
    });
    XucLink.whenLoggedOut().then(_unInit);
  };

  XucPhoneEventListener.addRingingHandler($rootScope, _onRinging);
  XucPhoneEventListener.addOnHoldHandler($rootScope, _onHold);
  XucPhoneEventListener.addEstablishedHandler($rootScope, _autoClose);
  XucPhoneEventListener.addReleasedHandler($rootScope, _autoClose);
  XucFlashText.subscribeToFlashText($rootScope, _onFlashTextReceived, true);

  XucLink.whenLogged().then(_init);

  return {
    enableNotification: _enableNotification,
    disableNotification: _disableNotification,
    isNotificationEnabled: _getNotificationEnabled,
    onFlashTextReceived: _onFlashTextReceived
  };
}
