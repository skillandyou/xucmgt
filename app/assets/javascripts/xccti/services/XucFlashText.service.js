import _ from 'lodash';
class Party {
  constructor(username, partyMessages, lastReceptionDate, displayName, isRead) {
    this.username = username;
    this.displayName = displayName;
    this.partyMessages = partyMessages;
    this.lastReceptionDate = lastReceptionDate;
    this.isRead = isRead;
    this.unreadCount = 0;
  }

  toJSON() {
    return {
      username: this.username,
      displayName: this.displayName,
      partyMessages: this.partyMessages.map(e => e.toJSON()),
      lastReceptionDate: this.lastReceptionDate
    };
  }
}
class PartyMessage {
  constructor(id, message, date, direction) {
    this.id = id;
    this.message = message;
    this.direction = direction;
    this.date = date;
  }

  toJSON() {
    return {
      id: this.id,
      message: this.message,
      direction: this.direction,
      date: this.date
    };
  }
}

export default function XucFlashText($rootScope, $log, $translate, XucLink, toast) {

  const toastDuration = 3000;
  let currentParty = 'none';
  let sequence = 0;
  var parties = [];

  const _onFlashTextEvent = (flashText) => {
    let isRead;
    if (flashText.event == "FlashTextUserMessage") {
      flashText.direction = 'incoming';
      currentParty == 'none' || currentParty != flashText.from.username ? isRead = false : isRead = true;
      _addOrUpdateParty(flashText, isRead);
    }
    $rootScope.$broadcast('FlashTextReceived', flashText);
  };

  const _subscribeToFlashText = (scope, callback, withToast=false) => {

    const triggerToast = (message, className) => {
      if(withToast) {
        toast({
          duration    : toastDuration,
          message     : message,
          className   : className,
          position    : "center",
          container   : '#toast-container'
        });
      }
    };

    var handler = $rootScope.$on('FlashTextReceived', (event, ft) => {
      switch(ft.event) {
      case 'FlashTextUserMessage':
        $rootScope.$applyAsync(() => {
          callback(ft);
        });
        break;
      case 'FlashTextSendMessageAck':
        triggerToast($translate.instant('FlashtextSuccess'), "alert-success");
        break;
      case 'FlashTextSendMessageNack':
        triggerToast($translate.instant('FlashtextError'), "alert-danger");
        break;
      }
    });
    scope.$on('$destroy', handler);
  };

  const _sendMessage = (username, message, displayName) => {
    sequence = sequence + 1;
    _addOwnMessage(username, message, displayName, sequence, 'outgoing');
    Cti.sendFlashTextMessage(username, sequence, message);
  };

  const _getMessages = () => {
    return sortPartiesByDate().reverse();
  };

  const _getPartyMessages = (username, displayName) => {
    let party = _.find(parties, ['username', username]);
    party = creatPartyIfUnexisting(party, username, undefined, displayName, true);
    return party;
  };

  const _addOrUpdateParty = (flashText, isRead) => {
    let party;
    party = retrievePartyInParties(flashText);

    if (isNotExisting(party)) {
      party = createPartyAndAddToParties(parties, flashText.from.username, flashText.date, flashText.from.displayName, isRead);
    } else {
      party.isRead = isRead;
    }
    createAndAddMessageToParty(party, flashText);
    if(isRead == false) party.unreadCount += 1;
    updateLastReceptionDate(party, flashText.date);
    return party;
  };

  const _setCurrentParty = (username) => {
    currentParty = username;
  };

  const _returnCurrentParty = () => {
    return currentParty;
  };

  const _checkForNewMessage = (parties) => {
    for (let party of parties) {
      if (party.isRead == false) {
        return true;
      }
    }
    return false;
  };

  const _hasNewMessage = () => {
    return _checkForNewMessage(parties);
  };

  const _addOwnMessage = (username, message, displayName, sequence, direction) => {
    let party = _filterExistingParties(parties, username);
    let messageDate = new Date();
    
    party = creatPartyIfUnexisting(party, username, messageDate, displayName, true);

    let partyMessage = _createPartyMessage(partyMessageFormatter(sequence, username, displayName, message, messageDate, direction));
    createAndAddMessageToParty(party, partyMessage);
    updateLastReceptionDate(party, messageDate);
    return party;
  };

  const _createPartyMessage = (message) => {
    return new PartyMessage(
      message.sequence,
      message.message,
      message.date,
      message.direction
    );
  };

  function partyMessageFormatter(sequence, username, displayName, message, messageDate, direction) {
    return {
      'sequence': sequence,
      'from': {
        'username': username,
        'displayName': displayName
      },
      'message': message,
      'date': messageDate,
      'direction': direction
    };
  }

  function updateLastReceptionDate(party, messageDate) {
    party.lastReceptionDate = messageDate;
  }

  function createAndAddMessageToParty(party, flashText) {
    party.partyMessages.unshift(_createPartyMessage(flashText));
  }

  function creatPartyIfUnexisting(party, username, date, displayName, isRead) {
    if (isNotExisting(party)) {
      party = createPartyAndAddToParties(party, username, date, displayName, isRead);
    }
    return party;
  }

  function retrievePartyInParties(flashText) {
    return parties.filter(party => party.username == flashText.from.username).pop();
  }

  function isNotExisting(party) {
    return angular.isUndefined(party);
  }

  function createPartyAndAddToParties(party, username, date, displayName, isRead) {
    party = new Party(username, [], date, displayName, isRead);
    parties.unshift(party);
    return party;
  }

  function sortPartiesByDate() {
    return _.sortBy(parties, function (party) {
      return new Date(party.lastReceptionDate);
    });
  }

  function _filterExistingParties(parties, username) {
    let party = parties.filter(party => party.username == username).pop();
    return party;
  }

  const _init = () => {
    $log.info("Starting XucFlashText service");
    XucLink.whenLoggedOut().then(_uninit);
  };

  const _uninit = () => {
    $log.info("Unloading XucFlashText service");
    parties = [];
    XucLink.whenLogged().then(_init);
  };

  Cti.setHandler(Cti.MessageType.FLASHTEXTEVENT, _onFlashTextEvent);

  XucLink.whenLogged().then(_init);

  return {
    subscribeToFlashText: _subscribeToFlashText,
    getFlashTexts: _getMessages,
    sendFlashText: _sendMessage,
    getParty: _addOrUpdateParty,
    createPartyMessage: _createPartyMessage,
    getPartyFlashTexts: _getPartyMessages,
    filterExistingParties: _filterExistingParties,
    setCurrentParty: _setCurrentParty,
    returnCurrentParty: _returnCurrentParty,
    hasNewMessage: _hasNewMessage,
    checkForNewMessage: _checkForNewMessage
  };
}
