class appConfig {
  constructor({ switchboard, livestats } = {}) {
    this.switchboard = switchboard || false;
    this.livestats = livestats || false;
  }
}

export default function applicationConfiguration() {

  this.defaultConfig = new appConfig();

  this.ccAgentConfig = new appConfig({ livestats: true });

  this.switchBoardConfig = new appConfig({ livestats: false, switchboard: true });

  this.$get = (remoteConfiguration) => {

    var _getCurrentAppConfig = () => {
      if (remoteConfiguration.hasSwitchBoard()) {
        return this.switchBoardConfig;
      }
      if (remoteConfiguration.isAgent()) {
        return this.ccAgentConfig;
      }
      return this.defaultConfig;
    };

    return {
      getCurrentAppConfig: _getCurrentAppConfig
    };
  };

}