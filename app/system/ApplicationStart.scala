package system

import configuration.{CcConfig, UcConfig}
import javax.inject._
import play.api.Logger
import play.api.inject.ApplicationLifecycle

import scala.concurrent.Future

@Singleton
class ApplicationStart @Inject()(lifecycle: ApplicationLifecycle, config: CcConfig, configUC: UcConfig) {

  dumpConfiguration()

  lifecycle.addStopHook { () =>
    Future.successful(Logger.info("Stopping xucmgt"))
  }

  private def dumpConfiguration(): Unit = {
    Logger.info("Starting xucmgt")
    Logger.info("---------- Server Configuration----------")
    Logger.info(s"Xuc Host    : ${config.hostAndPort}")
    Logger.info(s"Use SSL     : ${config.useSsl}")
    Logger.info(s"Use SSO     : ${config.useSso}")
    Logger.info(s"CAS URL     : ${config.casServerUrl.getOrElse("")}")
    Logger.info("---------- CC Configuration----------")
    Logger.info(s"Logoff on exit          : ${config.logoffOnExit}")
    Logger.info(s"Show queue controls     : ${config.showQueueControls}")
    Logger.info(s"Show recording controls : ${config.showRecordingControls}")
    Logger.info(s"Enforce login rights    : ${config.enforceManagerSecurity}")
  }
}