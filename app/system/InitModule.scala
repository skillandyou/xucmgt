package system

import com.google.inject.AbstractModule

class InitModule extends AbstractModule {
  def configure(): Unit = {
    bind(classOf[ApplicationStart]).asEagerSingleton()
  }
}
