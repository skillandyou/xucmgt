package controllers

import configuration.UcConfig
import javax.inject.Inject
import models.ConfigurationEntry
import play.api.libs.json.Json
import play.api.mvc.InjectedController

class ClientConfiguration @Inject()(config: UcConfig) extends InjectedController {

  def getVersion() = Action {
    Ok(Json.obj("value" -> config.appVersion))
  }

  def getConfig(paramName:String) = Action {
    config.getClientConf(paramName) match {
      case Some(v) => Ok(Json.toJson(ConfigurationEntry(paramName, v)))
      case None => NotFound
    }
  }
}