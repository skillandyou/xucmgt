package controllers

import configuration.CcConfig
import controllers.helpers.PrettyController
import javax.inject.Inject
import models.ConfigurationEntry
import play.api.Logger
import play.api.libs.json.Json

class CcAgent @Inject()(config: CcConfig, prettyCtrl: PrettyController) extends MainController {
  override val log = Logger(getClass.getName)
  override val isAgent = true
  val title = "XiVO CC Agent"

  override def connect = Action { implicit request =>
    log.debug(s"New connection to CC Agent from ${request.remoteAddress}")
    Ok(prettyCtrl.prettify(views.html.ccagent.index(title, config)(request.lang)))
  }

  def getVersion() = Action {
    Ok(Json.obj("value" -> config.appVersion))
  }

  def getConfig(paramName:String) = Action {
    config.getAgentConf(paramName) match {
      case Some(v) => Ok(Json.toJson(ConfigurationEntry(paramName, v)))
      case None => NotFound
    }

  }

}
